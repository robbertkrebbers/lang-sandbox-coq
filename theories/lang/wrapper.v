From sandbox.lang Require Import lang notation type proofmode tactics substitution macro_helper.
(** Import and Export
    import_fn/export_fn: Code of the function
    import_ctx/export_ctx: Typing context necessary to type import_fn/export_fn (T in the paper)
    import_type/export_type: Proof that import_fn/export_fn is well-typed
 *)
Class Import `{sandboxG Σ} (ty : type) := {
    import_fn : val;
    import_ctx : iProp Σ;
    import_type : import_ctx -∗ import_fn ◁ fn High [# any] ty
}.
Arguments import_fn {_ _} _ {_}.
Arguments import_type {_ _} _ {_}.
Arguments import_ctx {_ _} _ {_}.

Class Export `{sandboxG Σ} (ty : type) := {
    export_fn : val;
    export_ctx : iProp Σ;
    export_type : export_ctx -∗ export_fn ◁ fn High [# ty] any
}.

Arguments export_fn {_ _} _ {_}.
Arguments export_ctx {_ _} _ {_}.
Arguments export_type {_ _} _ {_}.

(* Lift Import and Export to lists.  We cannot use `Forall` because that one is restricted to Prop. *)
Inductive ImportLst `{sandboxG Σ} : list type → Type :=
| importl_nil : ImportLst []
| importl_cons ty tyl `{!Import ty, !ImportLst tyl} : ImportLst (ty::tyl).
Existing Class ImportLst.
Existing Instances importl_nil importl_cons.

Inductive ExportLst `{sandboxG Σ} : list type → Type :=
| exportl_nil : ExportLst []
| exportl_cons ty tyl `{!Export ty, !ExportLst tyl} : ExportLst (ty::tyl).
Existing Class ExportLst.
Existing Instances exportl_nil exportl_cons.

(* Helper for working with ImportLst and ExportLst *)
Section base.
  Context `{sandboxG Σ}.

  Definition importlst_nil_inv (P : ImportLst [] → Type)
             (Hcons : (P importl_nil)) exps : P exps.
  Proof.
    revert P Hcons.
    refine match exps with
             importl_nil => λ P Hcons, Hcons
           | @importl_cons _ _ _ _ exp exps => tt end.
  Defined.

  (* see also vec_S_inv *)
  Definition importlst_cons_inv ty tys (P : ImportLst (ty :: tys) → Type)
             (Hcons : ∀ exp exps, (P (@importl_cons _ _ _ _ exp exps))) exps : P exps.
  Proof.
    revert P Hcons.
    refine match exps with
             importl_nil => tt
           | @importl_cons _ _ _ _ exp exps =>
             λ P Hcons, Hcons exp exps end.
  Defined.

  Definition exportlst_nil_inv (P : ExportLst [] → Type)
             (Hcons : (P exportl_nil)) exps : P exps.
  Proof.
    revert P Hcons.
    refine match exps with
             exportl_nil => λ P Hcons, Hcons
           | @exportl_cons _ _ _ _ exp exps => tt end.
  Defined.

  (* see also vec_S_inv *)
  Definition exportlst_cons_inv ty tys (P : ExportLst (ty :: tys) → Type)
             (Hcons : ∀ exp exps, (P (@exportl_cons _ _ _ _ exp exps))) exps : P exps.
  Proof.
    revert P Hcons.
    refine match exps with
             exportl_nil => tt
           | @exportl_cons _ _ _ _ exp exps =>
             λ P Hcons, Hcons exp exps end.
  Defined.
End base.

Local Hint Resolve is_closed_of_val.
Ltac inv_list :=
  let ps := fresh "ps" in
  move => ps;
    let T := type of ps in
    match eval hnf in T with
    | ExportLst ?n =>
      match eval hnf in n with
      | [] => revert dependent ps; match goal with |- ∀ ps, @?P ps => apply (exportlst_nil_inv P) end
      | ?ty :: ?tys =>
        revert dependent ps; match goal with |- ∀ ps, @?P ps => apply (exportlst_cons_inv _ _ P) => ?? end
      end
    | ImportLst ?n =>
      match eval hnf in n with
      | [] => revert dependent ps; match goal with |- ∀ ps, @?P ps => apply (importlst_nil_inv P) end
      | ?ty :: ?tys =>
        revert dependent ps; match goal with |- ∀ ps, @?P ps => apply (importlst_cons_inv _ _ P) => ?? end
    end
  end.

Section helper.
  Context `{sandboxG Σ}.

  (* importable type *)
  Record itype : Type := {
    it_type :> type;
    it_import : Import it_type;
  }.
  Global Existing Instance it_import.

  Fixpoint importlst_to_list (tys : list type) {imps: ImportLst tys} : list itype :=
    match imps with
    | importl_nil => []
    | @importl_cons _ _ _ _ Import0 ImportLst0 => {| it_import := Import0 |} :: @importlst_to_list _ ImportLst0
    end.

  Lemma importlst_to_list_length tys {imps: ImportLst tys} :
    length (importlst_to_list tys) = length tys.
  Proof.
    elim: tys imps => //=.
    - by inv_list.
    - move => ty tys IH. inv_list => /=. by f_equal.
  Qed.

  Lemma importlst_to_list_lookup tys {imps: ImportLst tys} (ty : itype) n:
    importlst_to_list tys !! n = Some ty →
    tys !! n = Some (ty : type).
  Proof.
    elim: tys imps n; first by inv_list.
    move => ? ? IH. inv_list => /= [[|n]] /=; by eauto => -[<-].
  Qed.

  (* exportable type *)
  Record etype : Type := {
    et_type :> type;
    et_export : Export et_type;
  }.
  Global Existing Instance et_export.

  Fixpoint exportlst_to_list (tys : list type) {exps: ExportLst tys} : list etype :=
    match exps with
    | exportl_nil => []
    | @exportl_cons _ _ _ _ Export0 ExportLst0 => {| et_export := Export0 |} :: @exportlst_to_list _ ExportLst0
    end.

  Lemma exportlst_to_list_length tys {exps: ExportLst tys} :
    length (exportlst_to_list tys) = length tys.
  Proof.
    elim: tys exps => //=.
    - by inv_list.
    - move => ty tys IH. inv_list => /=. by f_equal.
   Qed.

  Lemma exportlst_to_list_lookup tys {exps: ExportLst tys} (ty : etype) n:
    exportlst_to_list tys !! n = Some ty →
    tys !! n = Some (ty : type).
  Proof.
    elim: tys exps n; first by inv_list.
    move => ? ? IH. inv_list => /= [[|n]] /=; by eauto => -[<-].
  Qed.

End helper.
Local Hint Resolve gen_binders_length.
Local Hint Resolve gen_binders_length_vec.
Local Hint Resolve gen_binders_NoDup.

(** Now we can start with actually defining wrappers. *)

Section any.
  Context `{sandboxG Σ}.

  Global Instance import_any : Import any :=
    {| import_ctx := True%I; import_fn := (λ: ["x"], "x") |}.
  Proof.
    iIntros "_". iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[Hx _]". simpl_subst.
    by iApply type_val.
  Defined.
  Global Opaque import_any.

  Global Instance export_any : Export any :=
    {| export_ctx := True%I; export_fn := (λ: ["x"], "x") |}.
  Proof.
    iIntros "_". iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    by iApply type_val.
  Defined.
  Global Opaque export_any.
End any.

Section int.
  Context `{sandboxG Σ}.

  Global Instance import_int : Import int :=
    {| import_ctx := True%I; import_fn := (λ: ["x"], "x" + #0) |}.
  Proof.
    iIntros "_". iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[Hx _]". simpl_subst.
    by iApply (type_plus with "[$]").
  Defined.
  Global Opaque import_int.

  Global Instance export_int : Export int :=
    {| export_ctx := True%I; export_fn := (λ: ["x"], "x") |}.
  Proof.
    iIntros "_". iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    iApply type_val. by iApply ty_surface.
  Defined.
  Global Opaque export_int.
End int.

Section const_int.
  Context `{sandboxG Σ}.


  Global Instance import_const_int n : Import (const_int n) :=
    {| import_ctx := True%I; import_fn := (λ: ["x"], let: "v" := import_fn int ["x"] in
                               if: "v" = #n then "v" else stuck_expr
                               ) |}.
  Proof.
    iDestruct (ty_surface $! (import_type int)) as %?.
    iIntros "_". iApply type_fn => //. solve_surface_expr. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    iApply (type_let).
    { iApply type_fn_call => //.  by iApply import_type. by iSplit. }
    iIntros (v) "#Hv _". simpl_subst.
    iDestruct (int_to_const_int with "Hv") as (m) "Hm".
    iApply type_case. by iApply type_eq.
    iIntros (i er ? Heq) "H1 H2 _".
    move: i Heq => [|[|?]] /=; rewrite ?lookup_nil // => [[<-]].
    - by iApply type_stuck.
    - iApply type_val.
      iDestruct (const_int_inv with "H1") as %->.
      iDestruct (const_int_inv with "H2") as %Heq.
      move: Heq. case_bool_decide as Hmn => // _. by rewrite Hmn.
  Defined.
  Global Opaque import_const_int.

  Global Instance export_const_int n : Export (const_int n) :=
    {| export_ctx := True%I; export_fn := (λ: ["x"], export_fn int ["x"]) |}.
  Proof.
    iDestruct (ty_surface $! (export_type int)) as %?.
    iIntros "_". iApply type_fn => //. solve_surface_expr. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    iApply type_fn_call => //. by iApply export_type.
    iSplit => //.
    by iDestruct (const_int_inv with "Hx") as %->.
  Defined.
  Global Opaque export_const_int.
End const_int.

Section lowptr.
  Context `{sandboxG Σ}.
  Global Instance import_lowptr : Import lowptr :=
    {| import_ctx := True%I; import_fn := (λ: ["x"],
                     assert_const_loc ["x"];;
                     if: GetPriv "x" then stuck_expr else "x") |}.
  Proof.
    iIntros "_". iApply type_fn => //. solve_surface_expr. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[_ _]". simpl_subst.
    iApply (type_seq). by iApply type_assert_const_loc.
    iDestruct 1 as (l) "#Hl".
    iApply (type_case with "[Hl]"). by iApply type_getpriv.
    iIntros (i er v Heq) "Hv1 Hv2 _".
    move: i Heq => [|[|?]] /=; rewrite ?lookup_nil // => [[<-]]; last by iApply type_stuck.
    iApply type_val.
    iDestruct (const_loc_inv with "Hl") as %->.
    iDestruct (const_int_inv with "Hv1") as %->.
    iDestruct (const_int_inv with "Hv2") as %Hlow.
    iPureIntro. rewrite /low. by move: (l.1.1) Hlow => [].
  Defined.
  Global Opaque import_lowptr.

  Global Instance export_lowptr : Export lowptr :=
    {| export_ctx := True%I; export_fn := (λ: ["x"], "x") |}.
  Proof.
    iIntros "_". iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    iApply type_val. by iApply any_subtype.
  Defined.
  Global Opaque export_lowptr.

  Definition read_import ty {I:Import ty} : val :=
    (λ: ["p"; "n"], let: "r" := !("p" +ₗ "n") in import_fn ty ["r"]).

  Lemma type_read_import ty `{!Import ty} :
    □ import_ctx ty -∗ read_import ty ◁ fn High [# lowptr; int] ty.
  Proof.
    iIntros "#Hctx". iPoseProof (import_type ty with "Hctx") as "Hty".
    iDestruct (ty_surface with "Hty") as %?.
    iApply type_fn => //. solve_surface_expr. do 2 iModIntro.
    iIntros (xl). inv_vec xl => p n /=.
    iIntros "[#Hp [#Hn _]]". simpl_subst.
    iApply type_let. by iApply type_read_low.
    iIntros (v) "Hv _". simpl_subst.
    iApply type_fn_call. done. by iApply import_type. by iFrame.
  Qed.

  Definition write_export ty {E:Export ty} : val :=
    (λ: ["p"; "v"; "n"],
     let: "r" := export_fn ty ["v"] in ("p" +ₗ "n") <- "r").

  Lemma type_write_export ty `{!Export ty} :
    □ export_ctx ty -∗ write_export ty ◁ fn High [# lowptr; ty; int] any.
  Proof.
    iIntros "#Hctx". iPoseProof (export_type ty with "Hctx") as "Hty".
    iDestruct (ty_surface with "Hty") as %?.
    iApply type_fn => //. solve_surface_expr. do 2 iModIntro. iIntros (xl). inv_vec xl=> p v n /=.
    iIntros "[#Hp [Hv [#Hn _]]]". simpl_subst.
    iApply (type_let with "[Hv]"). {
      iApply type_fn_call. done. by iApply export_type. by iFrame.
    }
    iIntros (r) "Hr _". simpl_subst.
    by iApply (type_write_low with "Hr").
  Qed.

End lowptr.

Section product.
  Context `{sandboxG Σ}.

  Global Program Instance import_product tys `{!ImportLst tys} : Import (product tys) :=
    {| import_ctx := (□ [∗ list] ty ∈ (importlst_to_list tys), import_ctx (ty : itype) )%I;
       import_fn := @RecV <> ["v"] (
         let: "v" := import_fn lowptr ["v"] in
         let: "p" := Alloc High #(length tys) in
         (* [gen_seq] generates the code that is generated by the
            overline notation in the paper. *)
         gen_seq (imap (λ n (ty : itype),

             let: "r" := read_import ty ["v"; #n] in
               ("p" +ₗ #n) <- "r")
               (* for all types in tys *)
               (importlst_to_list tys))
          (* This is the code after the overline notation*)
          (Free #(length tys) "v";;"p"))%E
                                   _ |}.
  Next Obligation.
    rewrite /Closed /= => tys imps.
    split_and? => //.
    apply gen_seq_closed; first by apply _.
    move => // e. set_unfold.
    move => [? [? [-> ?]]]. by apply _.
  Qed.
  Next Obligation.
  Local Opaque read_import.
  move => tys imps.
  iIntros "#Hctx".
  iDestruct (ty_surface $! (import_type lowptr)) as %?.
  iAssert (∀ n (p : itype), ⌜importlst_to_list tys !! n = Some p⌝ -∗ ⌜surface_val (read_import p)⌝)%I as %Hsv. {
    iIntros (? ? ?). iApply ty_surface.
    iApply type_read_import. iModIntro.
    by iApply (big_sepL_lookup with "Hctx").
  }
  iApply type_fn => //. {
    solve_surface_expr. apply: gen_seq_surface_expr; solve_surface_expr.
    set_unfold. move => ? [? [? [-> ?]]]. solve_surface_expr. by apply: Hsv.
  } clear Hsv.
  do 2 iModIntro. iIntros (xl). inv_vec xl => v /=. iIntros "[Hv _]". simpl_subst.
  iApply (type_let with "[Hv]"). {
    iApply type_fn_call. done. by iApply import_type. by iFrame. } clear v.
  iIntros (v) "#Hv _". simpl_subst.
  iApply type_let; first by iApply type_new.
  iIntros (p) "Hp _". change (subst "v") with (subst' "v").
  rewrite -(subst_env_empty (subst' _ _ _)) -2!subst_subst_env gen_seq_subst_env 2!subst_subst_env subst_env_empty fmap_imap /compose;simpl_subst.
  iApply (type_gen_seq (λ n,
      p ◁ product (take n tys ++ replicate (length tys - n) any))
         with "[Hp]").
  - by rewrite -minus_n_O /= Nat2Z.id.
  - rewrite big_sepL_imap.
    iApply (big_sepL_impl (λ _ _, True%I)).
    by iClear "Hctx"; iInduction (importlst_to_list tys) as [|] "IH" => //; iSplit.
    iModIntro. iIntros (n [ty ?] Hls) "_ Hp".
    have /= Hty := importlst_to_list_lookup _ _ _ Hls.
    have ? : (n < length tys)%nat by eapply lookup_lt_Some.
    rewrite 2!subst_subst_env subst_env_empty. simpl_subst.
    iApply typed_expr_mono; last first.
    + iApply type_let.
      { iApply type_fn_call => //.
        - iApply type_read_import.
            by iApply (big_sepL_lookup _ _ _ _ Hls with "Hctx").
        - iFrame "Hv". by iSplit. }
      iIntros (r) "Hr _". simpl_subst.
      iApply (type_write with "Hp Hr"); try lia.
      rewrite app_length take_length_le ?replicate_length; lia.
    + rewrite Nat2Z.id insert_app_r_alt take_length_le -?minus_n_n; [|lia..].
      have -> : ((length tys - n) = S (length tys-S n))%nat by lia. simpl.
      rewrite cons_middle (take_S_r _ _ _ Hty) assoc.
      by iIntros "?".
    + by iIntros (?) "?".
  - rewrite imap_length importlst_to_list_length -minus_n_n /= app_nil_r firstn_all.
    iIntros "Hp".
    iApply type_seq. by iApply type_delete_low. iIntros "_".
    by iApply type_val.
  Defined.
  Global Opaque import_product.

  Global Program Instance export_product tys `{!ExportLst tys} : Export (product tys) :=
    {| export_ctx := (□ [∗ list] ty ∈ (exportlst_to_list tys), export_ctx (ty : etype) )%I;
       export_fn := @RecV <> ["p"] (
         let: "v" := Alloc Low #(length tys) in
         (* [gen_seq] generates the code that is generated by the
            overline notation in the paper. *)
         gen_seq (imap (λ n (ty : etype),

             (let: "r" := !("p" +ₗ #n) in
              write_export ty ["v"; "r"; #n]))
               (* for all types in tys *)
               (exportlst_to_list tys))
         (* This is the code after the overline notation *)
         (Free #(length tys) "p";;
         export_fn lowptr ["v"]))%E
                                   _ |}.
  Next Obligation.
    rewrite /Closed /= => tys imps.
    split_and? => //.
    apply gen_seq_closed; first by apply _.
    move => e. set_unfold. move => [? [? [-> ?]]]. by apply _.
  Qed.
  Next Obligation.
  Local Opaque write_export.
  move => tys exps.
  iIntros "#Hctx".
  iDestruct (ty_surface $! (export_type lowptr)) as %?.
  iAssert (∀ n (p : etype), ⌜exportlst_to_list tys !! n = Some p⌝ -∗ ⌜surface_val (write_export p)⌝)%I as %Hsv. {
    iIntros (? ? ?). iApply ty_surface.
    iApply type_write_export. by iApply (big_sepL_lookup with "Hctx").
  }
  iApply type_fn => //. {
    solve_surface_expr. apply: gen_seq_surface_expr; solve_surface_expr.
    set_unfold. move => ? [? [? [-> ?]]]. solve_surface_expr. by apply: Hsv.
  } clear Hsv.
  do 2 iModIntro. iIntros (xl). inv_vec xl => p /=. iIntros "[Hp _]". simpl_subst.
  iApply type_let. { by iApply type_new_low. }
  iIntros (v) "#Hv _". change (subst "p") with (subst' "p").
  rewrite -(subst_env_empty (subst' _ _ _)) -2!subst_subst_env gen_seq_subst_env 2!subst_subst_env subst_env_empty fmap_imap /compose;simpl_subst.
  iApply (type_gen_seq (λ n,
      p ◁ product (replicate n any ++ drop n tys))
         with "[Hp]").
  - by rewrite drop_0.
  - rewrite big_sepL_imap.
    iApply (big_sepL_impl (λ _ _, True%I)).
    by iClear "Hctx"; iInduction (exportlst_to_list tys) as [|] "IH" => //; iSplit.
    iModIntro. iIntros (n [ty ?] Hls) "_ Hp".
    have /= Hty := exportlst_to_list_lookup _ _ _ Hls.
    have ? : (n < length tys)%nat by eapply lookup_lt_Some.
    rewrite 2!subst_subst_env subst_env_empty. simpl_subst.
    iApply (type_let with "[Hp]"). {
      iApply (type_read with "Hp"). lia.
      by rewrite Nat2Z.id lookup_app_r ?replicate_length // -minus_n_n lookup_drop -plus_n_O.
    }
    iIntros (r) "Hr Hp". simpl_subst.
    iApply (typed_expr_mono with "[] [Hp]"); last first.
    + iApply (type_fn_call) => //.
      * iApply type_write_export. by iApply (big_sepL_lookup _ _ _ _ Hls with "Hctx").
      * iFrame "Hv Hr". by iSplit.
    + iIntros "_".
      rewrite Nat2Z.id insert_app_r_alt ?replicate_length // -minus_n_n.
      by rewrite(drop_S _ ty)//=app_comm_cons replicate_cons_app-app_assoc.
    + by iIntros (?) "?".
  - rewrite imap_length exportlst_to_list_length drop_all app_nil_r.
    iIntros "Hp".
    iApply (type_seq with "[Hp]").
    { iApply (type_delete with "Hp"). by rewrite replicate_length. }
    iIntros "_".
    iApply type_fn_call => //. by iApply export_type. by iSplit.
  Defined.
  Global Opaque export_product.
End product.
