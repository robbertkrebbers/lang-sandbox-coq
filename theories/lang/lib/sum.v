From sandbox.lang Require Export notation.
From sandbox.lang Require Import heap proofmode type macro_helper.
From sandbox.lang.lib Require Export product.
Set Default Proof Using "Type".

Section sum.
  Context `{sandboxG Σ}.

  Program Definition sum (tys : list type) : type :=
    {| ty_own v := ∃ (n : nat) ty, ⌜tys !! n = Some ty⌝ ∗
           v ◁ product [const_int n; ty] |}%I.
  Next Obligation.
    iIntros (??). iDestruct 1 as (??) "[_ ?]". by iApply ty_surface.
  Qed.

  Definition option `{!sandboxG Σ} ty := sum [any; ty].

  Definition new_sum : val :=
    λ: ["n"; "v"],
    let: "r" := new_tuple 2 ["n"; "v"] in "r".

  Lemma type_new_sum (n : nat) tys ty :
    tys !! n = Some ty →
    new_sum ◁ fn High [# const_int n; ty] (sum tys).
  Proof.
    move => ?.
    iApply type_fn => //. solve_surface_expr. do 2 iModIntro. iIntros (xl).
    inv_vec xl => ? v. simpl_subst. iIntros "[Hl [Hv _]]".
    iApply (type_let with "[Hl Hv]"). { iApply type_fn_call. done.
       iApply (type_new_tuple _ [# const_int n; ty]). by iFrame. }
    iIntros (?) "Hp _". simpl_subst.
    iApply type_val. iExists _, _. by iFrame.
  Qed.

  Definition delete_sum : val :=
    λ: ["s"], Free #2 "s".

  Lemma type_delete_sum tys:
    delete_sum ◁ fn High [# (sum tys)] any.
  Proof.
    iApply type_fn => //. solve_surface_expr. do 2 iModIntro. iIntros (xl).
    inv_vec xl => s/=. simpl_subst. iIntros "[Hs _]".
    iDestruct "Hs" as (? ?) "[_ Hp]".
    by iApply (type_delete with "Hp").
  Qed.

  Program Definition case_sum n : val :=
    (@RecV <> ([BNamed "v"] ++ (BNamed <$> gen_binders n)) (
    case: !("v" +ₗ #0) of (λ s, let: "v" := !("v" +ₗ #1) in (Var s) [ "v" ]) <$> gen_binders n) _).
  Next Obligation.
    move => ?. rewrite /Closed /=. split_and? => //.
    apply/forallb_True/Forall_fmap/Forall_forall => /= ??.
    split_and? => //. case_bool_decide => //. set_solver.
  Qed.

  Local Hint Resolve gen_binders_NoDup.
  Lemma type_case_sum n (tys : vec _ n) ty:
    case_sum n ◁ fn High (sum tys ::: vmap (λ t, fn_once High [# t] ty) tys ) ty.
  Proof.
    iApply type_fn => //=.
    { by rewrite fmap_length gen_binders_length. }
    { by apply/forallb_True/Forall_fmap/Forall_true. }
    iIntros "!# !#" (xl). inv_vec xl => s xl /=.
    iIntros "[Hs Hxl]". simpl_subst. rewrite -/(subst_v _ _ _).
    iDestruct "Hs" as (j ? Htys) "Hp". change (subst "v") with (subst' "v").
    rewrite -(subst_env_empty (subst' _ _ _)) -subst_subst_env -subst_v_subst_env //.
    move: xl. rewrite fmap_length gen_binders_length => xl.
    have ?: (subst_map (gen_binders n) xl ##ₘ <[BNamed "v":=s]> ∅) by apply/map_disjoint_singleton_r/gen_binders_map_v.
    rewrite 5!subst_env_expr /= (lookup_union_Some_r _ _ _ s) ?lookup_insert //=.
    iApply (type_case with "[Hp]"). by iApply (type_read with "Hp").
    iIntros (i er ? [? [-> [? [-> Hi]]%list_lookup_fmap_inv]]%list_lookup_fmap_inv) "H1 H2 Hp" => /=.
    iDestruct (const_int_inv with "H1") as %->.
    iDestruct ("H2") as %?%Nat2Z.inj. subst j.
    rewrite subst_env_let. iApply (type_let with "[Hp]"). {
      rewrite !subst_env_expr (lookup_union_Some_r _ _ _ s) //=. iApply type_read => //. done. by apply lookup_insert.
    }
    iIntros (v) "Hv _". rewrite -subst_subst_env_l ?lookup_delete // insert_delete 2!subst_env_expr /= subst_env_expr lookup_insert /= lookup_insert_ne. 2: {
      move: Hi. intros Hi%elem_of_list_lookup_2. set_unfold.
      move: Hi => [? [-> ?]]. move => /BNamed_inj Hx.
      eapply pretty_nat_neq. by eauto.
    }
    have [|x Hx] := lookup_lt_is_Some_2 xl i; first by rewrite vec_to_list_length -(gen_binders_length n); eapply lookup_lt_Some.
    rewrite (lookup_union_Some_l _ _ _ x) /=; eauto using subst_map_in.
    iApply (type_call_once with "[Hxl]") => //.
    - rewrite vec_to_list_map. iApply (big_sepL_lookup _ _ i with "Hxl").
      by rewrite lookup_zip_with list_lookup_fmap Htys /= Hx.
    - by iFrame.
  Qed.

  Global Opaque case_sum.

  Definition unwrap : val :=
    λ: ["o"], case_sum 2 [ "o";
                             (λ: ["_"], stuck_expr);
                             (λ: ["v"], "v") ].

  Lemma type_unwrap ty :
    unwrap ◁ fn High [# option ty ] ty.
  Proof.
    iApply type_fn => //=. do 2 iModIntro.
    iIntros (xl). inv_vec xl => o /=. iIntros "[Ho _]". simpl_subst.
    iApply type_fn_call => //. by iApply (type_case_sum _ [# any; ty]).
    iFrame => /=. do ! iSplitL => //.
    - iApply type_fn_once. f_equal. done. done.
      iIntros "!#" (xl) "_". inv_vec xl => ?. simpl_subst.
      by iApply type_stuck.
    - iApply type_fn_once. f_equal. done. done.
      iIntros "!#" (xl). inv_vec xl => ? /=. iIntros "[Hty _]". simpl_subst.
      by iApply type_val.
  Qed.
  Global Opaque unwrap.
End sum.
