From sandbox.lang Require Export notation.
From sandbox.lang Require Import heap proofmode type.
From sandbox.lang.lib Require Export sum product.
Set Default Proof Using "Type".

Section specs.
  Context `{sandboxG Σ}.

  (* map of integer to ty *)
  Program Definition int_map (ty : type) : type := {|
     ty_own v := ∃ l, v ◁ const_loc l ∗ (⌜l.1.1 ≠ Low⌝ -∗
    ∃ (n : nat), #l ◁ product (const_int n :: (replicate n (option ty))))
  |}%I.
  Next Obligation.
    move => ??. iDestruct 1 as (?) "[Hl _]".
    by iApply ty_surface.
  Qed.

  Definition int_map_put : val :=
    λ: ["m"; "nv"],
    if: GetPriv "m" then (
        let: "n" := !("m" +ₗ #0) in
        let: "np" := "n" + #1 in
        let: "ns" := "np" + #1 in
        let: "m'" := Alloc High "ns" in
        memcpy ["m'"; "np"; "m"];;
        Free "np" "m";;
        ("m'" +ₗ #0) <- "np";;
        let: "o" := new_sum [ #1; "nv"] in
        ("m'" +ₗ "np") <- "o";;
        new_tuple 2 [ "m'"; "np" ]
    ) else (
        let: "t" := new_sum [ #1; "nv" ] in
        let: "nm" := new_tuple 2 [ #1; "t"] in
        new_tuple 2 ["nm"; #1 ]
    ).

  Lemma type_int_map_put ty :
    int_map_put ◁ fn High [# int_map ty; ty] (product [# int_map ty; int]).
  Proof.
    iApply type_fn => //.
    do 2 iModIntro. iIntros (xl). inv_vec xl => m nv/=. simpl_subst. iIntros "[Hm [Hnv _]]".
    iDestruct "Hm" as (l) "[#Hl Hp]".
    iApply type_case. by iApply type_getpriv.
    iIntros (i er ? Heq) "H1 H2 _".
    move: i Heq => [|[|?]] /=; rewrite ?lookup_nil // => [[<-]].
    - iApply (type_let with "[Hnv]"). {
        iApply (type_fn_call) => //.
        iApply (type_new_sum 1 [any; ty] ty) => //. iFrame. by iSplit.
      }
      iIntros (t) "Ht _". simpl_subst.
      iApply (type_let with "[Ht]"). {
        iApply (type_fn_call) => //.
        iApply (type_new_tuple _ [# const_int 1; sum [any; ty]]) => //.
        iFrame. by iSplit.
      }
      iIntros (nm) "Hnm _". simpl_subst.
      iApply (type_fn_call) => //.
      iApply (type_new_tuple _ [# int_map ty; int]) => //.
      iSplitL => //=.
      iDestruct (product_inv with "Hnm") as %[? ->].
      iExists _. iSplitR => //. iIntros "_". by iExists 1%nat.
    - iDestruct (const_int_inv with "H1") as %->.
      iDestruct ("H2") as %Hlow.
      iDestruct ("Hp" with "[]") as (n) "Hp". by destruct (l.1.1). clear Hlow.
      iDestruct (const_loc_inv with "Hl") as %->.
      iApply (type_let with "[Hp]"). by iApply (type_read with "Hp").
      iIntros (?) "#Hn Hp". simpl_subst.
      iDestruct (const_int_inv with "Hn") as %->.
      iApply type_let. by iApply type_const_plus.
      iIntros (?) "Hnp _". simpl_subst.
      iDestruct (const_int_inv with "Hnp") as %->.
      iApply type_let. by iApply type_const_plus.
      iIntros (?) "Hns _". simpl_subst.
      iDestruct (const_int_inv with "Hns") as %->. rewrite !Z.add_1_r -!Nat2Z.inj_succ.
      iApply type_let. by iApply type_new.
      iIntros (m') "Hm' _". rewrite (lock S). simpl_subst. unlock.
      iApply (type_seq with "[Hp Hm']"). by iApply (type_memcpy (S n) with "[] [$Hp] [$]") => /=; rewrite ?replicate_length ?Z2Nat.id; try lia; eauto.
      iIntros "[Hm Hm']".
      iApply (type_seq with "[Hm]"). {
        iApply (type_delete with "Hm").
        rewrite app_length replicate_length drop_length cons_length Nat2Z.id replicate_length. lia.
      }
      iIntros "_".
      rewrite firstn_all2 ?cons_length ?replicate_length ?Nat2Z.id // drop_replicate.
      have -> : ((S (S n) - S n) = 1)%nat by lia. rewrite replicate_S (lock S) /=. unlock.
      iApply (type_seq with "[Hm']"). by iApply (type_write (const_int (S n)) with "Hm'").
      iIntros "Hm'". rewrite (lock S) /=. unlock.
      iApply (type_let with "[Hnv]"). {
        iApply (type_fn_call) => //.
        iApply (type_new_sum 1 [any; ty] ty) => //. iSplitR => //. by iFrame.
      }
      iIntros (o) "Ho _". rewrite (lock S). simpl_subst. unlock.
      iApply (type_seq with "[Hm' Ho]"). by iApply (type_write with "Hm' Ho") => //; rewrite Z2Nat.inj_lt ?Nat2Z.id /= ?app_length ?replicate_length /=; lia.
      iIntros "Hm'".
      iApply (type_fn_call) => //.
      iApply (type_new_tuple _ [# int_map ty; int]) => //.
      iSplitL; last by simpl.
      iDestruct (product_inv with "Hm'") as %[? ->].
      iExists _. iSplitR => //. iIntros "_". iExists (S n)%nat.
      by rewrite Nat2Z.id /= replicate_cons_app insert_app_r_alt replicate_length // -minus_n_n app_comm_cons.
  Qed.

  Definition int_map_take : val :=
    λ: ["m"; "p"],
    let: "none" := new_sum [ #0; #☠] in
    if: GetPriv "m" then (
        let: "n" := !("m" +ₗ #0) in
        ("m" +ₗ #0) <- "n";;
        if: #1 ≤ "p" then (
            if: "p" ≤ "n" then (
                let: "r" := !("m" +ₗ "p") in
                ("m" +ₗ "p") <- "none";;
                new_tuple 2 ["m"; "r"]
            ) else (
                new_tuple 2 ["m"; "none"]
            )
        ) else (
            new_tuple 2 ["m"; "none"]
        )
    ) else (
        new_tuple 2 ["m"; "none"]
    ).

  Lemma type_int_map_take ty :
    int_map_take ◁ fn High [# int_map ty; int] (product [# int_map ty; option ty]).
  Proof.
    iApply type_fn => //. solve_surface_expr.
    do 2 iModIntro. iIntros (xl). inv_vec xl => m ?/=. simpl_subst. iIntros "[Hm [Hint _]]".
    iDestruct "Hm" as (l) "[#Hl Hpr]".
    iDestruct (int_to_const_int with "Hint") as (p) "#Hp".
    iApply (type_let). {
      iApply type_fn_call => //.
      iApply (type_new_sum 0 [any; ty] any) => //.
        by do ! iSplitL.
    }
    iIntros (none) "Hnone _". simpl_subst.
    iApply type_case. by iApply type_getpriv.
    iIntros (i er ? Heq) "H1 H2 _".
    move: i Heq => [|[|?]] /=; rewrite ?lookup_nil // => [[<-]]; last first.

    iDestruct (const_int_inv with "H1") as %->. iClear "H1".
    iDestruct ("H2") as %Hlow.
    iDestruct ("Hpr" with "[]") as (n) "Hpr". by destruct (l.1.1). clear Hlow.
    iDestruct (const_loc_inv with "Hl") as %->. simpl.
    iApply (type_let with "[Hpr]"). by iApply (type_read with "Hpr").
    iIntros (?) "Hn Hpr". iDestruct (const_int_inv with "Hn") as %->. simpl_subst.
    iApply (type_seq with "[Hpr Hn]"). by iApply (type_write with "Hpr Hn").
    iIntros "Hpr" => /=.
    iApply type_case. by iApply type_leq.
    iIntros (i ? ? Heq) "H1 H2 _".
    move: i Heq => [|[|?]] /=; rewrite ?lookup_nil // => [[<-]]; last first.

    iDestruct (const_int_inv with "H1") as %->.
    iDestruct (const_int_inv with "H2") as %Hle%Z_of_bool_1. iClear "H1 H2".

    iApply type_case. by iApply type_leq.
    iIntros (i ? ? Heq) "H1 H2 _".
    move: i Heq => [|[|?]] /=; rewrite ?lookup_nil // => [[<-]]; last first.

    iDestruct (const_int_inv with "H1") as %->.
    iDestruct (const_int_inv with "H2") as %Hge%Z_of_bool_1. iClear "H1 H2".
    iDestruct (const_int_inv with "Hp") as %->.
    have [m ?]: (∃ (m : nat), p = Z.of_nat (S m)). {
      exists (Z.to_nat (Z.pred p)).
      rewrite -Z2Nat.inj_succ; last lia.
      rewrite -Zsucc_pred Z2Nat.id //. lia.
    } subst p.
    iApply (type_let with "[Hpr]"). {
      iApply (type_read with "Hpr"). lia.
      rewrite Nat2Z.id /=. apply lookup_replicate. split => //. lia.
    }
    iIntros (r) "Hr Hpr". rewrite (lock S). simpl_subst. unlock.
    iApply (type_seq with "[Hnone Hpr]"). {
      iApply (type_write with "Hpr Hnone") => //.
      rewrite insert_length /= replicate_length. lia.
    }
    iIntros "Hpre".
    rewrite list_insert_insert Nat2Z.id /= insert_replicate.

    all: iApply (type_fn_call) => //; first by [iApply (type_new_tuple _ [# int_map ty; option ty]) => //];
      iFrame; iSplitL => //; iExists l; iFrame "Hl"; iIntros (?);
                                  try iSpecialize ("Hpr" with "[//]"); by eauto.
  Qed.

End specs.
