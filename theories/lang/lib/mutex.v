From sandbox.lang Require Export notation.
From sandbox.lang Require Import heap proofmode type.
From sandbox.lang.lib Require Export product.
Set Default Proof Using "Type".

Definition mutexN := nroot .@ "mutex".

(* mutex which stores a value of type ty *)
Program Definition mutex `{sandboxG Σ} (ty : type) : type := {|
     ty_own v := match v return _ with
                 | LitV (LitLoc l) => inv mutexN (∃ b : bool,
                      l ↦ #b ∗ if b then True else
                               ∃ v, (l +ₗ 1) ↦ v ∗ v ◁ ty)
                 | _ => False
                 end%I
|}.
Next Obligation. move => ??? [[]|] * //; by iIntros "?". Qed.

Global Instance mutex_copy `{sandboxG Σ} ty : Copy (mutex ty).
Proof. do 2 case; try apply _. Qed.

Section specs.
  Context `{sandboxG Σ}.

  Program Definition mutexguard (ty : type) : type := {|
     ty_own v := match v return _ with
                 | LitV (LitLoc l) => #l ◁ mutex ty ∗ ∃ v, (l +ₗ 1) ↦ v
                 | _ => False
                 end%I
  |}.
  Next Obligation. move => ? [[]|] * //; by iIntros "?". Qed.

  Definition mutex_inv v ty : v ◁ mutex ty -∗ ⌜∃ (l : loc), v = #l⌝.
  Proof. iIntros "?". case v => // [[| |?]] //. eauto. Qed.
  Definition mutexguard_inv v ty :
    v ◁ mutexguard ty -∗ ⌜∃ (l : loc), v = #l⌝.
  Proof. iIntros "?". case v => // [[| |?]] //. eauto. Qed.

  Definition acquire : val :=
    rec: "acquire" ["l"] := if: CAS "l" #false #true then (
                let: "v" := !("l" +ₗ #1) in
                new_tuple 2 ["l"; "v"]
                ) else "acquire" ["l"].
  Definition release : val := λ: ["l"; "v"],
                             ("l" +ₗ #1) <- "v";;
                             "l" <- #false.

  Lemma type_acquire ty :
    acquire ◁ fn High [# mutex ty] (product [mutexguard ty; ty]).
  Proof.
    iApply type_rec => //. solve_surface_expr. do 2 iModIntro. iIntros (f xl).
    inv_vec xl => ? /=. simpl_subst. iIntros "#Hf [#Hl _]".
    iDestruct (mutex_inv with "Hl") as %[l ->].
    iApply (type_case ({| ty_own v := ∃ n, v ◁ const_int n ∗ (⌜n = 1⌝ -∗ ∃ v, (l +ₗ 1) ↦ v ∗ v ◁ ty); ty_surface := _ |})%I True%I ). {
      iIntros "#?" (?) "HΦ" => /=. rewrite {1 2}/ty_own /=.
      iInv "Hl" as (b) "[Hlp Hb]".
      destruct b.
      - iApply (wp_cas_int_fail_high with "[$]") => //.
        iNext. iIntros "Hlp !#". iSplitR "HΦ"; eauto with iFrame.
        iApply "HΦ" => //. iExists 0. iSplit => //. by iIntros (?).
      - iApply (wp_cas_int_suc_high with "[$]").
        iNext. iIntros "Hlp !#". iSplitR "HΦ Hb"; eauto with iFrame.
        iApply ("HΦ" with "[Hb]") => //. iExists 1. eauto with iFrame.
    }
    iIntros (i er v Heq) "H1 H2 _".
    move: i Heq => [|[|?]] /=; rewrite ?lookup_nil // => [[<-]].
    { iApply (type_fn_call with "Hf") => //. by iSplitL. }
    iDestruct (const_int_inv with "H2") as %->. iClear "Hf H2".
    iDestruct "H1" as (n') "[-> Hp]".
    iDestruct ("Hp" with "[//]") as (v) "[Hp Hv]".
    iApply (type_let ty with "[Hp Hv]"). {
      iIntros "#?" (?) "HΦ". wp_op. wp_read_high.
      by iApply ("HΦ" with "Hv Hp").
    }
    iIntros (v') "Hv' Hv". simpl_subst.
    iApply type_fn_call. done. by iApply (type_new_tuple _ [#_ ; _]).
    iFrame. iSplitL => //. iFrame "Hl". by eauto.
    Unshelve.
    iIntros (?). iDestruct 1 as (?) "[? _]". by iApply ty_surface.
  Qed.

  Lemma type_release ty :
    release ◁ fn High [# mutexguard ty; ty] any.
  Proof.
    iApply type_fn => //. do 2 iModIntro. iIntros (xl).
    inv_vec xl => ? v /=. simpl_subst. iIntros "[Hl [Hv _]]".
    iDestruct (mutexguard_inv with "Hl") as %[l ->].
    iDestruct "Hl" as "[#Hl Hp]". iDestruct "Hp" as (?) "Hp".
    iIntros "#?" (?) "HΦ". wp_op.
    iDestruct (ty_surface with "Hv") as %?.
    wp_write_high.
    iInv "Hl" as (b) "[Hlp Hb]". wp_write_high.
    iModIntro. iSplitR "HΦ"; last by iApply "HΦ".
    by eauto with iFrame.
  Qed.

End specs.
