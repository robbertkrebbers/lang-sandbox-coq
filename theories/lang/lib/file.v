From stdpp Require Export sets gmap option numbers.
From iris.algebra Require Import csum excl auth cmra_big_op.
From iris.algebra Require Import big_op gset frac agree.
From sandbox.lang Require Export notation.
From sandbox.lang Require Import heap proofmode type.
From sandbox.lang.lib Require Import product.
(* Set Default Proof Using "Type". *)

(** This is the fileSP system call policy, which enforces that only
open file handles can be read or closed. *)
Definition fileSP : syscall_policy := {|
  (** The state of this syscall policy is the set of open file handles. *)
  sp_state := gset Z;
  (** In the beginning, all file handles are closed. *)
  sp_initial_state := ∅;
  sp_transitions :=
    (** open adds the file handle to the set of open file handles. *)
    <["open" := λ v r s, t ← lit_to_Z r; Some ({[ t ]} ∪ s) ]> $
    (** read checks that the file is currently open and leaves the
    file handle in the set of open file handles. *)
    <["read" := λ v' r s, v ← lit_to_Z v';
                  if bool_decide (v ∈ s) then Some s else None ]> $
    (** close checks that the file is currently open and removes the
    file handle from the set of open file handles. *)
    <["close" := λ v' r s, v ← lit_to_Z v';
          if bool_decide (v ∈ s) then Some (s ∖ {[ v ]} ) else None ]>
    ∅
|}.

(** Registering the necessary ghost state. *)
Class fileG Σ := FileG {
   file_inG :> inG Σ (authR (gset_disjUR Z));
}.
Definition fileΣ : gFunctors :=
  #[GFunctor (constRF (authR (gset_disjUR Z)))].
Instance subG_fileG {Σ} : subG fileΣ Σ → fileG Σ.
Proof. solve_inG. Qed.

(** These are the axioms which the kernel is assumed to fulfill when
working with files (A_file from the paper). *)
Definition file_axioms (π : list observation) : Prop :=
  (** open only returns integers. Remember that π consists of
  observations, which are tuples of the form (system call name (here
  "open"), argument to the system call (here [f]), return value of the
  system call (here [r])). *)
  (∀ i f r, π !! i = Some ("open", f, r) → is_Some (lit_to_Z r))
    (** Two distinct calls to open (distinct means different indicies
    in the trace) return different file handles. *)
    ∧ (∀ i1 i2 f1 f2 r1 r2, π !! i1 = Some ("open", f1, r1) → π !! i2 = Some ("open", f2, r2) →
       i1 ≠ i2 → r1 ≠ r2).

(** fileSPG is the file system call policy extended with the necessary
invariant for working with it in the logic. *)
Program Definition fileSPG `{sandboxSPPreG Σ} `{fileG Σ} : syscall_policyG Σ := {|
  (** fileSPG extends fileSP *)
  spg_SP := fileSP;
  (** fileSPG needs a gname for the file token ghost state. *)
  spg_name := gname;
  (** invariant of fileSPG *)
  spg_prop := λ γ sps,
    (** Link the state of the system call policy to the ghost state
    available in the logic via the authorative construction. *)
    (own γ (● GSet sps) ∗
         (** There exists a set of indices *)
         ∃ idxs,
           (** which all have occured in the trace (this is necessary
           to show that a new observation will have a greater index
           than all indices in idxs). *)
           ([∗ list] i∈idxs, ∃ o, in_trace i o) ∗
           (** and all currently open file handles have been opened at
           an index in idxs. Together with file_axioms this allows to
           see that a file handle returned by open is not in sps. *)
           ([∗ set] t∈sps, ∃ i v, ⌜i∈idxs⌝ ∗ in_trace i ("open", v, LitInt t)))%I;
  (** fileSPG uses the file_axioms *)
  spg_axioms := file_axioms;
 |}.
Next Obligation.
  move => ???/=.
  iMod (own_alloc (● (GSet ∅))) as (γ) "Hs" => //. by apply auth_auth_valid.
  iModIntro. iExists γ. iFrame. iExists []. by iSplit.
Qed.

Definition do_open : val :=
  λ: ["v"], Syscall "open" "v".

Definition do_read : val :=
  λ: ["f"], let: "r" := Syscall "read" "f" in new_tuple 2 [ "f" ; "r" ].

Definition do_close : val :=
  λ: ["f"], Syscall "close" "f";; #☠.

Section type.
  Context `{sandboxG Σ} `{fileG Σ} `{inSP Σ fileSPG}.
  Definition file_token (n : Z) := own (insp_name fileSPG heap_spg_name) (◯  GSet {[ n ]}).

  Lemma file_token_in n st:
    file_token n -∗ own (insp_name fileSPG heap_spg_name) (●  GSet st)
      -∗ ⌜n ∈ st⌝.
  Proof.
    iIntros "Hf Ho". iDestruct (own_valid_2 with "Ho Hf") as %[?%gset_disj_included _]%auth_both_valid.
    iPureIntro. set_solver.
  Qed.

  Program Definition file : type :=
    {| ty_own v := match v return _ with
                   | LitV (LitInt n) => file_token n
                   | _ => False
                   end%I |}.
  Next Obligation. move => [|] * //; by iIntros "?". Qed.

End type.

Section specs.
  Context `{sandboxG Σ} `{fileG Σ} `{inSP Σ fileSPG}.

  Lemma file_inv v : v ◁ file -∗ ⌜∃ (z : Z), v = #z⌝.
  Proof. iIntros "?". case v => // [] // [] //. eauto. Qed.

  Lemma type_file_open v:
    v ◁ int -∗ typed_expr High file True (Syscall "open" v).
  Proof.
    iIntros "Hv".
    iDestruct (int_to_const_lit with "Hv") as (n) "Hn".
    iApply (type_syscall fileSPG with "Hn").
    iIntros (r st i π) "[Hst Hls]". iDestruct "Hls" as (idxs) "[#Hidxs #Hls]".
    iExists idxs. iFrame "Hidxs".
    iIntros (Hgt%max_list_Z_gt_not_in) "#Hin Hπ". iIntros ([Hint Hneq]).
    iDestruct (in_full_trace with "Hπ Hin") as %Hin.
    have [?] := Hint _ _ _ Hin.
    move: r Hin => [|?|r] // Hin _.

    have [?|?] := decide (r ∈ st). {
      iDestruct (big_sepS_elem_of _ _ r with "Hls") as (i2 v2) "[% Hin2]" => //.
      iDestruct (in_full_trace with "Hπ Hin2") as %Hin2.
      exfalso. eapply (Hneq _ _ _ _ _ _ Hin Hin2) => // ?. by subst.
    }

    rewrite /run_sp_obs /=. simpl_map => /=.
    iExists _. iSplitR => //.
    iMod (own_update with "Hst") as "[$ ?]". { eapply auth_update_alloc.
      apply (gset_disj_alloc_empty_local_update). set_solver. }
    iModIntro. iFrame.
    iExists (i :: idxs) => /=. iFrame "Hidxs". iSplit; first by iExists _.
    rewrite big_sepS_insert //.
    iSplit.
    - iExists _, _. iFrame "Hin". iPureIntro. set_solver.
    - iApply (big_sepS_impl with "Hls"). iIntros "!#" (t) "_".
      iDestruct 1 as (? ?) "[% ?]". iExists _, _. iFrame.
      iPureIntro. set_solver.
  Qed.

  Lemma type_do_open:
    do_open ◁ fn High [# int] file.
  Proof.
    iApply type_fn => //.
    do 2 iModIntro. iIntros (xl). inv_vec xl => v /=.
    iIntros "[Hv _]". simpl_subst.
    by iApply type_file_open.
  Qed.

  Lemma type_file_read v:
    v ◁ file -∗ typed_expr High any (v ◁ file) (Syscall "read" v).
  Proof.
    iIntros "Hf".
    iDestruct (file_inv with "Hf") as %[n ->].
    iApply (type_syscall fileSPG any (#n ◁ file) with "[] [Hf]") => //.
    iIntros (r st i π) "[Hst Hls]" => /=. iExists [].
    iSplitR => //. iIntros (_) "_ ? _".
    iModIntro. iDestruct (file_token_in with "Hf Hst") as %?.
    rewrite /run_sp_obs /=. simpl_map => /=.
    case_bool_decide => //. iExists _. iSplitR => //.
      by iFrame.
  Qed.

  Lemma type_do_read:
    do_read ◁ fn High [# file] (product [file; any]).
  Proof.
    iApply type_fn => //. do 2 iModIntro. iIntros (xl). inv_vec xl => f /=.
    iIntros "[Hf _]". simpl_subst.
    iApply (type_let with "[Hf]"). by iApply type_file_read.
    iIntros (r) "Hr Hn". simpl_subst.
    iApply (type_fn_call $! (type_new_tuple 2 [# _ ; _])) => //.
    by iFrame.
  Qed.

  Lemma type_file_close v:
    v ◁ file -∗ typed_expr High any True (Syscall "close" v).
  Proof.
    iIntros "Hf".
    iDestruct (file_inv with "Hf") as %[n ->].
    iApply (type_syscall fileSPG any True%I); eauto.
    iIntros (r st i π) "[Hst Hls]" => /=.
    iExists []. iSplitR => //.
    iIntros (_) "_ Hπ _".
    iDestruct (file_token_in with "Hf Hst") as %?.
    rewrite /run_sp_obs /=. simpl_map => /=. case_bool_decide => //.
    iExists _. iFrame "Hπ". iSplitR => //. iSplitL => //. iSplitR "Hls".
    - iRevert "Hf Hst". rewrite /ty_own /= /file_token.
      iApply bi.wand_intro_l. rewrite -own_op.
      apply own_update. eapply auth_update_dealloc.
      by apply gset_disj_dealloc_local_update.
    - iDestruct "Hls" as (maxi) "[? #Hls]".
      iModIntro. iExists _. iFrame.
      by rewrite big_sepS_delete //; iDestruct "Hls" as "[_ $]".
  Qed.

  Lemma type_do_close:
    do_close ◁ fn High [# file] any.
  Proof.
    iApply type_fn => //.
    do 2 iModIntro. iIntros (xl). inv_vec xl => f /=.
    iIntros "[Hf _]". simpl_subst.
    iApply (type_seq with "[Hf]").  by iApply type_file_close.
    iIntros "_". by iApply type_val.
  Qed.

End specs.
