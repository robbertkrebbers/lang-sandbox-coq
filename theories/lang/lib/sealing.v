From sandbox.lang Require Export notation.
From sandbox.lang Require Import heap proofmode type wrapper.
From sandbox.lang.lib Require Import sum int_map mutex.
Set Default Proof Using "Type".

Section globals.
  Context `{sandboxG Σ}.
  Definition cmap_global_init := [LitInt 0; LitLoc (Low, 1%positive, 0)].

  Lemma cmap_global_init_l l ty:
    ((l ↦∗ (LitV <$> cmap_global_init)) ={⊤}=∗ #l ◁ mutex (int_map ty))%I.
  Proof.
    iIntros "Hl". rewrite /ty_own /= !heap_mapsto_vec_cons.
    iDestruct "Hl" as "[Hl [Hl1 _]]".
    iApply (inv_alloc mutexN). iNext. iExists false. iFrame.
    iExists _. iFrame. rewrite /ty_own /=. iExists _. iSplitL => //.
      by iIntros (?).
  Qed.
End globals.

Section specs.
  Context `{sandboxG Σ}.

  Program Definition sealed (n : namespace) ty := {|
    ty_own v := v ◁ ty
  |}.
  Next Obligation. move => ???. by iApply ty_surface. Qed.

  Global Instance import_sealed ty n
    : Import (sealed n ty) :=
    {| import_ctx := global_to_val n ◁ mutex (int_map ty);
       import_fn := (λ: ["x"],
                     let: "i" := import_fn int [ "x" ] in
                     let: "ac" := acquire [global_to_val n] in
                     let: "m" := !("ac" +ₗ #0) in
                     let: "map" := !("ac" +ₗ #1) in
                     Free #2 "ac";;
                     let: "taken" := int_map_take ["map"; "i"] in
                     let: "map" := !("taken" +ₗ #0) in
                     let: "o" := !("taken" +ₗ #1) in
                     Free #2 "taken";;
                     release [ "m"; "map" ];;
                     unwrap ["o"]
                     ) |}.
  Proof.
    Local Opaque release delete int_map_take acquire.
    iIntros "#Hn".
    iDestruct (ty_surface $! (import_type int)) as %?.
    iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    iApply type_let. { iApply (type_fn_call) => //.
      by iApply import_type. by iSplitL. }
    iIntros (i) "#Hi _". simpl_subst.
    iApply type_let. { iApply type_fn_call => //.
      iApply (type_acquire (int_map ty)). by iSplit.
    }
    iIntros (ac) "Hac _". simpl_subst.
    iApply (type_let with "[Hac]"). by iApply type_read => //.
    iIntros (m) "Hm Hac". simpl_subst.
    iApply (type_let with "[Hac]"). by iApply type_read => //.
    iIntros (map) "Hmap Hac". simpl_subst.
    iApply (type_seq with "[Hac]"). by iApply type_delete => //.
    iIntros "_".
    iApply (type_let with "[Hmap]"). { iApply type_fn_call => //.
      iApply (type_int_map_take). iFrame. by iSplitL. }
    iIntros (t) "Ht _". simpl_subst.
    iApply (type_let with "[Ht]"). by iApply type_read => //.
    iIntros (map') "Hmap Ht". simpl_subst.
    iApply (type_let with "[Ht]"). by iApply type_read => //.
    iIntros (o) "Ho Ht". simpl_subst.
    iApply (type_seq with "[Ht]"). by iApply type_delete => //.
    iIntros "_".
    iApply (type_seq with "[Hm Hmap]"). { iApply type_fn_call => //.
      iApply (type_release (int_map ty)). by iFrame.  }
    iIntros "_".
    iApply type_fn_call => //. by iApply (type_unwrap ty).
    by iFrame.
  Defined.
  Global Opaque import_sealed.

  Global Instance export_sealed n ty : Export (sealed n ty) :=
    {| export_ctx := global_to_val n ◁ mutex (int_map ty);
       export_fn := (λ: ["x"],
                     let: "ac" := acquire [ global_to_val n ] in
                     let: "m" := !("ac" +ₗ #0) in
                     let: "map" := !("ac" +ₗ #1) in
                     Free #2 "ac";;
                     let: "put" := int_map_put ["map"; "x"] in
                     let: "map" := !("put" +ₗ #0) in
                     let: "p" := !("put" +ₗ #1) in
                     Free #2 "put";;
                     release [ "m"; "map" ];;
                     export_fn int [ "p" ]) |}.
  Proof.
    Local Opaque release delete int_map_put acquire.
    iIntros "#Hn".
    iDestruct (ty_surface $! (export_type int)) as %?.
    iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[Hx _]". simpl_subst.
    iApply type_let. { iApply type_fn_call => //.
      iApply (type_acquire (int_map ty)). by iSplit.
    }
    iIntros (ac) "Hac _". simpl_subst.
    iApply (type_let with "[Hac]"). by iApply type_read => //.
    iIntros (m) "Hm Hac". simpl_subst.
    iApply (type_let with "[Hac]"). by iApply type_read => //.
    iIntros (map) "Hmap Hac". simpl_subst.
    iApply (type_seq with "[Hac]"). by iApply type_delete => //.
    iIntros "_".
    iApply (type_let with "[Hmap Hx]"). { iApply type_fn_call => //.
      iApply (type_int_map_put ty). by iFrame. }
    iIntros (t) "Ht _". simpl_subst.
    iApply (type_let with "[Ht]"). by iApply type_read => //.
    iIntros (map') "Hmap Ht". simpl_subst.
    iApply (type_let with "[Ht]"). by iApply type_read => //.
    iIntros (o) "Ho Ht". simpl_subst.
    iApply (type_seq with "[Ht]"). by iApply type_delete => //.
    iIntros "_".
    iApply (type_seq with "[Hm Hmap]"). { iApply type_fn_call => //.
      iApply (type_release (int_map ty)). by iFrame.  }
    iIntros "_".
    iApply type_fn_call => //. by iApply export_type.
    by iFrame.
  Defined.
  Global Opaque export_sealed.

End specs.
