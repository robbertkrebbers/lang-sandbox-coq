From sandbox.lang Require Import type.
From iris.program_logic Require Import ectx_lifting.

Section adversary.
  Context `{sandboxG Σ}.

  Definition adv_env (γ : env) : Prop :=
    map_Forall (λ _ v, surface_val v) γ.

  Local Hint Extern 0 (sub_redexes_are_values _) => solve_sub_redexes_are_values.

  Lemma surface_expr_env e γ:
    surface_expr e → adv_env γ → surface_expr (γ e).
  Proof.
    elim: e γ => [s γ ? Hγ|l|f xl e IH|*|e ? ? /Forall_forall|*|*|*|*|*|e ?? /Forall_forall|*|*|?? /Forall_forall|*|*|*|*|*] *;
                              simpl in *;
                              rewrite subst_env_expr //=; destruct_and?; split_and?; try match goal with
            | Hf : Is_true (forallb _ _) |- _ => move/forallb_True/Forall_forall in Hf
                                      end;
            try apply/forallb_True/Forall_fmap/Forall_forall; eauto.
    - have [[? Hin]|/eq_None_not_Some ->] := decide (is_Some (γ !! BNamed s)); rewrite ?Hin //=.
      by apply: Hγ.
    - apply: IH => //. by apply map_Forall_delete, map_Forall_foldr_delete.
  Qed.

  Local Hint Extern 0 (sub_redexes_are_values _) => solve_sub_redexes_are_values.

  (* We first need to prove a slightly more general form of Theorem 2
  to be able to use induction. Thus, we first prove this version which
  also generalizes over a substitution context. *)
  Lemma type_adv_expr e γ:
    ⌜surface_expr e⌝ -∗ ⌜adv_env γ⌝ -∗ typed_expr Low any True (γ e).
  Proof.
    iRevert (e γ). iLöb as "IHL". iIntros (e). iStopProof.
    elim: e => [s|l|f xl e IH|? e1 IH1 e2 IH2|e1 IH1 el IH|e1 IH1|e1 IH1 e2 IH2|e1 IH1 e2 IH2 e3 IH3|h e1 IH1|e1 IH1 e2 IH2|e IHe el /Forall_lookup IH|e1 IH1|s e1 IH1|s el IH|*|e1 IH1 e2 IH2 e3 IH3|e1 IH1|e1 IH1|e1 IH1]/=; iIntros "#IHL" (γ Hs Hγ);
     destruct_and? ; rewrite subst_env_expr;
       try iPoseProof (IH1 with "IHL") as "He1"; try iPoseProof (IH2 with "IHL") as "He2";
         try iPoseProof (IH3 with "IHL") as "He3";
     try (iIntros "#?" (?) "HΦ"; wp_apply "He1" => //; iIntros (v1) "Hv1 _";
         try (wp_apply "He2" => //; iIntros (v2) "Hv2 _";
         try (wp_apply "He3" => //; iIntros (v3) "Hv3 _"))).
    - have [[? Hin]|/eq_None_not_Some ->] := decide (is_Some (γ !! BNamed s)); rewrite ?Hin => /=.
      + by iApply type_val; eauto.
      + iIntros "#?" (?) "?". solve_stuck.
    - by iApply type_val.
    - set e' := (_ e). iIntros "#?" (?) "HΦ".
      have [?|?] := decide (Closed (f :b: xl +b+ []) e'); last by solve_stuck.
      have ?: (IntoVal (rec: f xl := e') (rec: f xl := e')) by [].
      iApply type_val => //. iPureIntro. rewrite /surface_val /=.
      apply: surface_expr_env => //. by apply map_Forall_delete, map_Forall_foldr_delete.
    - iApply sandbox_atomic_head_step_maybestuck; [eauto..|].
      iNext. iIntros (??????). inv_head_step. rewrite right_id.
      iIntros "$ !#". by iApply "HΦ".
    - iApply (wp_app (replicate (length el) (λ v, v ◁  any)%I)); first by rewrite replicate_length fmap_length.
      + iInduction el as [|] "IH" => //=.
        move: IH => /Forall_cons[IH2 IH]. iPoseProof (IH2 with "IHL") as "He2". simpl in *. destruct_and?.
        iSplitL; by [ iApply IH2 | iApply "IH"].
      + rewrite fmap_length. iIntros (vl Hlen) "Hvl".
        move: v1 => [*|f xl e' ?]; first by solve_stuck.
        iApply app_argument_length_stuck. iIntros (Hxl).
        iDestruct "Hv1" as %?.
        iApply (type_fn_call _ _ (vreplicate (length vl) any) with "[] [Hvl]") => //;[ by rewrite fmap_length| | ].
        2: {
          rewrite -Hlen vec_to_list_replicate. clear. iClear "#".
          iInduction vl as [] "IH" => //=. iDestruct "Hvl" as "[??]". iFrame. by iApply "IH".
        }
        iApply (type_rec) => //. iIntros "!# !#" (f' xl') "Hf Hv".
        rewrite -(subst_env_empty (subst_v _ _ _)) -subst_v_subst_env' right_id.
        iApply "IHL" => //.
        iDestruct (ty_surface with "Hf") as %?. csimpl.
        iAssert (⌜∀ x, x ∈ vec_to_list xl' → surface_val x⌝)%I as %Hx. {
          iClear "IHL He1 Hf". rewrite vec_to_list_replicate -Hxl. clear.
          iIntros (x Hx).
          iInduction xl as [] "IH"; simpl in *; inv_vec xl' => /=. by set_solver.
          set_unfold => ?? [->|?]; iDestruct "Hv" as "[? ?]"=> //. by iApply "IH".
        }
        iPureIntro => s x. intros Hin%elem_of_list_to_map_2. set_unfold.
        move: Hin => -[[_ ->]|?] //. eauto using elem_of_zip_r.
    - move: v1 => [[|l|?]|*]; try by solve_stuck.
      by wp_read_low vr => ?; wp_value_head ; iApply "HΦ".
    - move: v1 => [[|l|?]|*]; [solve_stuck| | solve_stuck..].
      iDestruct "Hv2" as %?.
      have [?|Hh] := low_or_high l.
      + wp_write_low. by iApply "HΦ".
      + have ?: (l.1.1 ⊑ Low → False) by rewrite Hh.
        solve_stuck.
    - iApply sandbox_atomic_head_step_maybestuck; [eauto..|].
      iNext. iIntros (??????). inv_head_step; rewrite right_id.
      + iIntros "$ !#". by iApply "HΦ".
      + iIntros "Hctx". iSplitL "Hctx".
        * iMod (heap_write_low with "[$]") as "$" => //.
            by apply priv_leb_low.
        * iModIntro. by iApply "HΦ".
    - move: h => []; first by solve_stuck.
      move: v1 => [[|?|n]|*]; try by solve_stuck.
      have [?|?]:= decide (0 < n); last by solve_stuck.
      wp_alloc_low v => ?. wp_value_head.
        by iApply "HΦ".
    - move: v1 => [[|?|n]|*]; try by solve_stuck.
      move: v2 => [[|l|?]|*]; try by solve_stuck.
      have [?|Hh] := low_or_high l.
      + wp_free_low. by iApply "HΦ".
      + have ?: (l.1.1 ⊑ Low → False) by rewrite Hh.
        solve_stuck.
    - iPoseProof (IHe with "IHL") as "He".
      iApply type_case. by iApply "He".
      iIntros (i er v [? [-> ?]]%list_lookup_fmap_inv) "_ _ _".
      iApply IH => //. iPureIntro.
      eapply (Forall_lookup (λ x, surface_expr x)). by eapply forallb_True. done.
    - iIntros "#?" (?) "HΦ".
      iApply (wp_fork with "[He1]"); iNext.
      + by iApply "He1".
      + by iApply "HΦ".
    - solve_stuck.
    - iIntros "#?" (?) "HΦ".
      have ->: (γ <$> el = (of_val <$> []) ++ (γ <$> el)) by [].
      iAssert ([∗ list] v0 ∈ [], ⌜surface_val v0⌝)%I as "Hvl". by [].
      move: ([]) => vl.
      iInduction el as [|e2 el] "IH" forall (vl); csimpl.
      + rewrite app_nil_r.
        iAssert (interface_ctx_rec interface_ctx) as "HI". by rewrite {1}/interface_ctx fixpoint_unfold.
        iApply sandbox_head_step_maybestuck => //.
        iIntros (σ1 ?) "Hctx".
        iMod (fupd_intro_mask' _ (∅)) as "Hm"; first by solve_ndisj.
        iIntros "!# !# " (e2 σ2 ? ?). inv_head_step.
        iDestruct "HI" as (I) "[HI Ht]".
        iDestruct (high_interface_state with "Hctx HI") as %?. subst I.
        iDestruct (big_sepM_lookup with "Ht") as "#Hcall" => //.
        iMod "Hm". iModIntro. iFrame.
        wp_apply ("Hcall" with "[//] [Hvl]"); first by iFrame.
        iIntros (? ?). wp_pure (GatedRet _). by iApply "HΦ".
      + move: IH => /Forall_cons[IH2 IH]. iPoseProof (IH2 with "IHL") as "He2". simpl in *. destruct_and?.
        (* manual binding, find a nicer way for this *)
        have -> :((GatedCall s ((of_val <$> vl) ++ γ e2 :: (γ <$> el)) at Low)%E = fill [(GatedCallCtx s vl (γ <$> el))] ((γ e2) at Low)%E) by [].
        iApply wp_bind. iApply "He2" => //=. iIntros (v2) "Hv2 _".
        rewrite cons_middle. change ([v2])%E with (of_val <$> [v2]).
        rewrite app_assoc -fmap_app.
        iApply ("IH" with "[] [] HΦ") => //.
        rewrite big_sepL_app. by iFrame.
    - iApply sandbox_atomic_head_step_maybestuck; [eauto.. |].
      iNext. iIntros (??????). inv_head_step. rewrite right_id.
      iIntros "$ !#". by iApply "HΦ".
    - move: v1 => [|*]; last by solve_stuck.
      move => [|?|?]; [solve_stuck| |solve_stuck].
      wp_pure (GetPriv _). by iApply "HΦ".
    - move: v1 => [|*]; last by solve_stuck.
      move => [|?|?]; [solve_stuck| |solve_stuck].
      wp_pure (GetBlock _). by iApply "HΦ".
    - move: v1 => [|*]; last by solve_stuck.
      move => [|?|?]; [solve_stuck| |solve_stuck].
      wp_pure (GetOffset _). by iApply "HΦ".
  Qed.

  (** This is Theorem 2 (Soundness of the sandbox). It trivially follows from type_adv_expr. *)
  Lemma type_adv_expr' e:
    surface_expr e → typed_expr Low any True%I e.
  Proof.
    iIntros (?).
    iPoseProof (type_adv_expr e ∅ with "[//] [//]") as "Hf".
    by rewrite subst_env_empty.
  Qed.


  Lemma type_gated_call s vl:
    ([∗ list] v ∈ vl, v ◁ any) -∗
    typed_expr High any True (GatedCall s (of_val <$> vl)).
  Proof.
    rewrite big_sepL_forall.
    iIntros (Hvl). iIntros "#?" (?) "HΦ".
    iApply sandbox_head_step_maybestuck => //. iIntros (σ1 ?) "Hctx".
    iMod (fupd_intro_mask' _ (∅)) as "Hm"; first by solve_ndisj.
    iIntros "!# !# " (e2 σ2 ? ?). inv_head_step.
    iDestruct (interface_lookup_surface with "[$] [//]") as %?.
    iMod "Hm". iModIntro. iFrame.
    wp_bind (App _ _).
    iApply type_adv_expr' => //=.
    - split_and? => //.
      rewrite forallb_True Forall_forall => ?. set_unfold. move => [? [-> /elem_of_list_lookup[??]]].
      by eapply Hvl.
    - iIntros (?) "#? _". wp_pure (GatedRet _). by iApply "HΦ".
  Qed.

End adversary.
