From stdpp Require Import list option fin_sets vector fin_maps pretty binders sets.
From Coq Require Import ssreflect.

(* TODO: ask whether interesting *)
Lemma pretty_N_neq (n : N): pretty n ≠ "v".
Proof.
  rewrite pretty_N_unfold.
  have : ("" ≠ "v") by []. move: {1 3}("") => s.
  elim/(well_founded_ind N.lt_wf_0): n s.
  case => n /=; first by move => ?; rewrite pretty_N_go_0.
  move => IH s Hs. rewrite pretty_N_go_step. by lia.
  apply IH; first by apply: N.div_lt; lia.
  rewrite -(N2Nat.id (_ `mod` _)).
  move Heq: (N.to_nat $ N.pos n `mod` 10)%N => m.
  have {Heq} : (N.of_nat m < 10)%N by rewrite -Heq N2Nat.id; apply N.mod_lt.
  do 10 destruct m as [|m] => //. lia.
Qed.

(* TODO: ask whether interesting *)
Lemma pretty_nat_neq (n : nat): pretty n ≠ "v".
Proof. apply: pretty_N_neq. Qed.

(* TODO: ask whether interesting *)
Lemma Forall3_cons_inv {A B C} {x : A} {y : B} {z : C} {xs ys zs P} :
  Forall3 P (x::xs) (y::ys) (z::zs) → P x y z ∧ Forall3 P xs ys zs.
Proof. by inversion 1. Qed.

(* TODO: new MR with these, see https://gitlab.mpi-sws.org/iris/stdpp/merge_requests/78#note_38018 *)
Global Instance set_unfold_imap A B f (l : list A) (x : B):
  SetUnfoldElemOf x (imap f l) (∃ i y, x = f i y ∧ l !! i = Some y).
Proof.
  constructor.
  elim: l f => /=. set_solver. set_unfold. move => ? ? IH f.
  rewrite IH {IH}. split. case.
  - move => ->. set_solver.
  - move => [n [v [-> ?]]]. exists (S n), v => /=. set_solver.
  - move => [[|n] /= [v [-> ?]]]; simplify_eq; [by left | right].
    naive_solver.
Qed.

(* too specific to move *)
Lemma imap_fst_NoDup {A B C} l (f : nat → A → B) (g : nat → C):
  Inj eq eq g →
  NoDup (imap (λ i o, (g i, f i o)) l).*1.
Proof.
  move => ?. rewrite fmap_imap (imap_ext _ (λ i o, g i)%nat) // imap_seq_0.
    by apply NoDup_fmap, NoDup_ListNoDup, seq_NoDup.
Qed.

(* TODO: ask whether interesting *)
Lemma option_union_Some_l {A} (x : A) y:
  Some x ∪ y = Some x.
Proof. by destruct y. Qed.

(* TODO: ask whether interesting *)
Global Instance BNamed_inj : Inj eq eq BNamed.
Proof. by move => ?? []. Qed.

(* TODO: ask whether interesting *)
Definition max_list_Z : list nat → Z :=
  fix go l :=
    match l with
    | [] => -1
    | x :: l => x `max` go l
    end%Z.

(* TODO: ask whether interesting *)
Lemma max_list_Z_gt_not_in (n : nat) ns :
  ((n > max_list_Z ns) → n ∉ ns)%Z.
Proof.
  elim: ns; first set_solver.
  move => n2 ns /= IH ?. set_unfold. move => [?|?]; first by subst; lia.
  apply: IH => //. lia.
Qed.


(* TODO *)
(* TODO: ask whether interesting *)
Ltac cinv_vec := let H := fresh "H" in move => H; cbn in H; inv_vec H.

From iris.bi Require Import big_op derived_laws_sbi.
Import interface.bi derived_laws_bi.bi derived_laws_sbi.bi.
Section sep_list.
  Context {PROP : bi}.
  Implicit Types Ps Qs : list PROP.
  Implicit Types A : Type.
  Context {A : Type}.
  Implicit Types l : list A.
  Implicit Types Φ Ψ : nat → A → PROP.

  Lemma big_sepL_insert Φ l i x y:
    l !! i = Some y →
    ([∗ list] k↦y ∈ l, Φ k y) ∗ Φ i x ⊣⊢
    ([∗ list] k↦y ∈ <[i := x]> l, Φ k y) ∗ Φ i y.
  Proof.
    move => Hlen. have ? := lookup_lt_Some _ _ _ Hlen.
    rewrite -(take_drop_middle _ _ _ Hlen) insert_app_r_alt take_length_le; try lia.
    rewrite -minus_diag_reverse /= !big_sepL_app !big_sepL_cons take_length_le -? plus_n_O; last lia.
    by rewrite 2!(sep_comm (Φ i _) _) -!(sep_assoc _ _ (Φ i _)) (sep_comm (Φ _ _)).
  Qed.

  Lemma big_sepL_insert' `{!BiAffine PROP} Φ l i x :
    i < length l →
    ([∗ list] k↦y ∈ l, Φ k y) -∗ Φ i x -∗
    ([∗ list] k↦y ∈ <[i := x]> l, Φ k y).
  Proof.
    move => /lookup_lt_is_Some[??].
    apply wand_intro_r.
    rewrite big_sepL_insert //.
    setoid_rewrite <-sep_emp at 5.
    apply sep_mono => //. by apply affine.
  Qed.

  Lemma big_sepL_imap B (f : nat → A → B) (Φ : nat → B → PROP) l:
    ([∗ list] k↦y ∈ (imap f l), Φ k y) ⊣⊢ ([∗ list] k↦y ∈ l, Φ k (f k y)).
  Proof. elim: l f Φ => //=?? IH ??. by rewrite IH. Qed.

  (* TODO prove this and use it in wrapper.v *)
  (* Lemma big_sepL_forall1 Φ l : *)
    (* □ (∀ k x, ⌜l !! k = Some x⌝ → Φ k x) -∗ *)
    (* [∗ list] k↦x ∈ l, Φ k x. *)
  (* Proof. *)

End sep_list.
Fixpoint big_sepL3 {PROP : bi} {A B C}
    (Φ : nat → A → B → C → PROP) (l1 : list A) (l2 : list B) (l3 : list C) : PROP :=
  match l1, l2, l3 with
  | [], [], [] => emp
  | x1 :: l1, x2 :: l2, x3 :: l3 => Φ 0%nat x1 x2 x3 ∗ big_sepL3 (λ n, Φ (S n)) l1 l2 l3
  | _, _, _ => False
  end%I.
Instance: Params (@big_sepL3) 4 := {}.
Arguments big_sepL3 {PROP A B C} _ !_ !_ !_ /.
Typeclasses Opaque big_sepL3.
(* Notation "'[∗' 'list]' k ↦ x1 ;; x2 ;; x3 ∈ l1 ;; l2 ;; l3 , P" := *)
  (* (big_sepL3 (λ k x1 x2 x3, P) l1 l2 l3) (at level 100) : bi_scope. *)
(* Notation "'[∗' 'list]' x1 | x2 | x3 ∈ l1 | l2 | l3 , P" := *)
  (* (big_sepL3 (λ _ x1 x2 x3, P) l1 l2 l3) (at level 1). *)

Lemma big_sepL3_cons_inv_l {PROP : bi} {A B C} (Φ : nat → A → B → C → PROP) x1 l1 l2 l3 :
  big_sepL3 (λ k y1 y2 y3, Φ k y1 y2 y3) (x1 :: l1) l2 l3 -∗
    ∃ x2 l2' x3 l3', ⌜ l2 = x2 :: l2' ⌝ ∧ ⌜ l3 = x3 :: l3' ⌝ ∧
            Φ 0%nat x1 x2 x3 ∗ big_sepL3 (λ k y1 y2 y3, Φ (S k) y1 y2 y3) l1 l2' l3'.
Proof.
  move: l2 l3 => [|x2 l2] [|x3 l3]; try apply False_elim => /=.
  by rewrite -(exist_intro x2) -(exist_intro l2) -(exist_intro x3) -(exist_intro l3) pure_True // pure_True // 2!left_id.
Qed.

From iris.program_logic Require Import ectx_language weakestpre lifting.
From iris.proofmode Require Import tactics.
From iris.program_logic Require Import ownp.
Section wp.
Context {Λ : ectxLanguage} `{irisG Λ Σ} {Hinh : Inhabited (state Λ)}.
Implicit Types s : stuckness.
Implicit Types P : iProp Σ.
Implicit Types Φ : val Λ → iProp Σ.
Implicit Types v : val Λ.
Implicit Types e : expr Λ.

Lemma sub_redexes_are_values_prim_step e1 σ1 κ e2 σ2 efs :
  prim_step e1 σ1 κ e2 σ2 efs →
  sub_redexes_are_values e1 →
  head_step e1 σ1 κ e2 σ2 efs.
Proof.
  move => [K e1' e2' -> -> Hstep] ?.
  have -> : (K = empty_ectx) by eauto 10 using val_head_stuck.
  by rewrite !fill_empty.
Qed.

Hint Resolve head_prim_reducible head_reducible_prim_step sub_redexes_are_values_prim_step.
Hint Resolve (reducible_not_val _ inhabitant).
Hint Resolve head_stuck_stuck.
Lemma wp_lift_head_step_fupd_maybestuck {s E Φ} e1:
  to_val e1 = None →
  (∀ σ1 κ κs n, state_interp σ1 (κ ++ κs) n ={E,∅}=∗
    ⌜if s is NotStuck then head_reducible e1 σ1 else head_reducible e1 σ1 ∨ sub_redexes_are_values e1⌝ ∗
    ∀ e2 σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ ={∅,∅,E}▷=∗
      state_interp σ2 κs (length efs + n) ∗ WP e2 @ s; E {{ Φ }} ∗ [∗ list] ef ∈ efs, WP ef @ s; ⊤ {{ v, fork_post v }})
  ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_step_fupd=>//. iIntros (σ1 κ κs?) "Hσ".
  iMod ("H" with "Hσ") as "[H1 H]"; iModIntro. iDestruct "H1" as %Hs.
  iSplit; first by destruct s; eauto. iIntros (e2 σ2 efs Hstep).
  iApply "H"; destruct s; eauto.
  destruct Hs; eauto.
Qed.

Lemma wp_lift_head_step_maybestuck {s E Φ} e1 :
  to_val e1 = None →
  (∀ σ1 κ κs n, state_interp σ1 (κ ++ κs) n ={E,∅}=∗
    ⌜if s is NotStuck then head_reducible e1 σ1 else head_reducible e1 σ1 ∨ sub_redexes_are_values e1⌝ ∗
    ▷ ∀ e2 σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ ={∅,E}=∗
      state_interp σ2 κs (length efs + n) ∗
      WP e2 @ s; E {{ Φ }} ∗
      [∗ list] ef ∈ efs, WP ef @ s; ⊤ {{ fork_post }})
  ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_head_step_fupd_maybestuck; [done|]. iIntros (????) "?".
  iMod ("H" with "[$]") as "[$ H]". iIntros "!>" (e2 σ2 efs ?) "!> !>". by iApply "H".
Qed.


Lemma wp_lift_atomic_head_step_maybestuck {s E Φ} e1 :
  to_val e1 = None →
  (∀ σ1 κ κs n, state_interp σ1 (κ ++ κs) n ={E}=∗
    ⌜if s is NotStuck then head_reducible e1 σ1 else head_reducible e1 σ1 ∨ sub_redexes_are_values e1⌝ ∗
    ▷ ∀ e2 σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ ={E}=∗
      state_interp σ2 κs (length efs + n) ∗
      from_option Φ False (to_val e2) ∗ [∗ list] ef ∈ efs, WP ef @ s; ⊤ {{ v, fork_post v }})
  ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_atomic_step; eauto.
  iIntros (σ1 κ κs n) "Hσ1".
  iMod ("H" with "Hσ1") as "[Hs H]"; iModIntro.
  iDestruct "Hs" as %Hs.
  iSplit; first by destruct s; auto. iNext. iIntros (e2 σ2 efs Hstep).
  iApply "H"; destruct s; eauto.
  destruct Hs; eauto.
Qed.

Lemma wp_lift_atomic_head_step_no_fork_maybestuck {s E Φ} e1 :
  to_val e1 = None →
  (∀ σ1 κ κs n, state_interp σ1 (κ ++ κs) n ={E}=∗
    ⌜if s is NotStuck then head_reducible e1 σ1 else head_reducible e1 σ1 ∨ sub_redexes_are_values e1⌝ ∗
    ▷ ∀ e2 σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ ={E}=∗
      ⌜efs = []⌝ ∗ state_interp σ2 κs n ∗ from_option Φ False (to_val e2))
  ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_atomic_head_step_maybestuck; eauto.
  iIntros (σ1 κ κs n) "Hσ1". iMod ("H" $! σ1 with "Hσ1") as "[$ H]"; iModIntro.
  iNext; iIntros (v2 σ2 efs Hstep).
  iMod ("H" $! v2 σ2 efs with "[# //]") as "(% & ? & $)". subst; auto.
Qed.

End wp.
