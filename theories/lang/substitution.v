From stdpp Require Export gmap.
From sandbox.lang Require Export lang notation.

Local Hint Resolve to_of_val.

Section subst.
  Definition env := gmap binder val.
  Implicit Types γ : env.

  Definition subst_env_acc (r : expr) (x : (binder * val)) : expr :=
    subst' x.1 (of_val x.2) r.

  Arguments subst_env_acc _ _ /.

  Definition subst_env' (l : list (binder * val)) (e :expr) : expr :=
    foldl subst_env_acc e l.

  Definition subst_env γ : expr → expr :=
    subst_env' (map_to_list γ).

  Coercion subst_env : env >-> Funclass.

  Lemma subst_env'_proper L1 L2:
    NoDup L1.*1 → L1 ≡ₚ L2 →
    pointwise_relation (expr) (=) (subst_env' L1) (subst_env' L2).
  Proof.
    move => HND Hp e. elim: Hp HND e => //=.
    - move => ????? /NoDup_cons[]. by eauto.
    - move => [??][??]? /NoDup_cons[Hneq _] ?/=.
      f_equal. set_unfold.
      apply subst'_comm; try by apply: is_closed_of_val.
      contradict Hneq. by eauto.
    - move => ??? HPerm IH1 ? IH2 ? ?.
      by rewrite IH1 // IH2 // -HPerm.
  Qed.

  (* This explicit recursive definition is needed for simplification
  to work automatically *)
  Fixpoint subst_map (xl : list string) (vl : list val) : env
    := match xl, vl with
       | [], _ => ∅
       | _, [] => ∅
       | x::xl, v::vl => <[BNamed x := v]> (subst_map xl vl : env)
       end.

  Lemma subst_map_eq xl vl : subst_map xl vl = list_to_map (zip (BNamed <$> xl) vl).
  Proof. elim: xl vl => //?? IH [] //= ??. by f_equal. Qed.

  Lemma subst_map_in xl vl k s v:
    NoDup xl →
    xl !! k = Some s →
    vl !! k = Some v →
    subst_map xl vl !! BNamed s = Some v.
  Proof.
    elim: xl vl k => //= ? ? IH[]//?? k /NoDup_cons[Hn?]. move: k=> []/=.
    - move => [<-] [<-]. by simpl_map.
    - move => ? Hs?. rewrite lookup_insert_ne; eauto.
      move => /BNamed_inj?. subst. by eapply Hn, elem_of_list_lookup_2.
  Qed.

  Lemma subst_map_not_in xl vl s:
    s ∉ xl → subst_map xl vl !! BNamed s = None.
  Proof.
    elim: xl vl => //= ? ? IH []//???.
    apply/lookup_insert_None. set_solver.
  Qed.

  Lemma subst_env_empty e : (∅ : env) e = e.
  Proof. done. Qed.

  Lemma subst_env_closed e γ : Closed [] e → γ e = e.
  Proof.
    move => ?. rewrite /subst_env /=.
    elim: (map_to_list _) => //= [[[]]]?? //=?.
    by rewrite is_closed_nil_subst.
  Qed.

  Lemma subst_env_is_closed γ X e : Closed ((map_to_list γ).*1 +b+ X) e → Closed X (γ e).
  Proof.
    rewrite /Closed /subst_env.
    elim: (map_to_list γ) e => //= [[]] /= ??? IH ??.
    apply IH, subst'_is_closed => //. by apply is_closed_of_val.
  Qed.

  Lemma subst_subst_env' L b e v:
    subst_env' ((b,v) :: L) e = subst_env' L (subst' b v e).
  Proof. done. Qed.

  Lemma subst_subst_env e b v γ:
    (<[b:=v]> γ) e = γ (subst' b (of_val v) e).
  Proof.
    rewrite /subst_env -subst_subst_env' -insert_delete.
    rewrite subst_env'_proper; [|apply NoDup_fst_map_to_list|apply map_to_list_insert, lookup_delete].

    have [[? Hin]|/eq_None_not_Some ?] := (decide (is_Some (γ !! b))); last by rewrite delete_notin.
    rewrite -{2}(insert_id _ _ _ Hin) -insert_delete 2!subst_subst_env'.
    etrans; [| symmetry; apply subst_env'_proper; [by apply NoDup_fst_map_to_list| by apply map_to_list_insert, lookup_delete] ].

    rewrite subst_subst_env'. f_equal.
    rewrite subst'_compose. f_equal. symmetry.
    by apply is_closed_nil_subst', is_closed_of_val.
  Qed.

  Lemma subst_env_subst_env e γ1 γ2:
    (γ1 ∪ γ2) e = γ2 (γ1 e).
  Proof.
    rewrite /subst_env -(list_to_map_to_list γ1) -foldr_insert_union.
    rewrite (subst_env'_proper (_ (list_to_map _))); [| apply NoDup_fst_map_to_list|apply map_to_list_to_map, NoDup_fst_map_to_list].
    elim: (map_to_list (γ1)) e => //= ? ? IH e.
    by rewrite -IH -/(subst_env _) subst_subst_env.
  Qed.

  Lemma subst_subst_env_l γ b e v:
    γ !! b = None → (<[b:=v]> γ) e = subst' b (of_val v) (γ e).
  Proof. move => ?. by rewrite -(subst_env_empty (subst' _ _ _)) -subst_subst_env -subst_env_subst_env -insert_union_r // right_id. Qed.

  Lemma subst_v_subst_env' e γ xl (vs : vec _ _):
    ((list_to_map $ reverse (zip xl vs)) ∪ γ) e = γ (subst_v xl vs e).
  Proof.
    rewrite -foldr_insert_union.
    elim: xl vs γ e => /=; first
      by move => vs; inv_vec vs => /= ??; rewrite /subst_v.
    move => x xl IH vs. inv_vec vs => /= v vs γ e.
    by rewrite /subst_v /= -/(subst_v _ _ e) -subst_subst_env -IH reverse_cons foldr_app.
  Qed.

  Lemma subst_v_subst_env e γ xl (vs : vec _ _):
    NoDup xl →
    (subst_map xl vs ∪ γ) e = γ (subst_v (BNamed <$> xl) vs e).
  Proof.
    move => HND. rewrite subst_map_eq -subst_v_subst_env'.
    do 2 f_equal.
    apply list_to_map_proper; last by symmetry; apply reverse_Permutation.
    rewrite fst_zip // ?vec_to_list_length; last by lia.
    apply NoDup_fmap => //. by apply _.
  Qed.

  Lemma subst_v_subst_env_l' e γ xl (vs : vec _ _):
    (γ ∪ (list_to_map $ reverse (zip xl vs))) e = subst_v xl vs (γ e).
  Proof. by rewrite -(subst_env_empty (subst_v _ _ _)) -subst_v_subst_env' -subst_env_subst_env right_id. Qed.

  Lemma subst_v_subst_env_l e γ xl (vs : vec _ _):
    NoDup xl →
    (γ ∪ subst_map xl vs) e = subst_v (BNamed <$> xl) vs (γ e).
  Proof. move => ?. by rewrite -(subst_env_empty (subst_v _ _ _)) -subst_v_subst_env // -subst_env_subst_env right_id. Qed.

  Lemma subst_env_expr e γ:
    γ e =
    match e with
    | Var x => default (Var x) (of_val <$> γ !! BNamed x)
    | Rec f x e => Rec f x $ (delete f $ foldr delete γ x) e
    | App e1 el => App (γ e1) (γ <$> el)
    | Lit _ => e
    | BinOp op e1 e2 => BinOp op (γ e1) (γ e2)
    | Case e1 el => Case (γ e1) (γ <$> el)
    | Fork e => Fork (γ e)
    | Alloc p e => Alloc p (γ e)
    | Free e1 e2 => Free (γ e1) (γ e2)
    | Read e => Read (γ e)
    | Write e1 e2 => Write (γ e1) (γ e2)
    | CAS e1 e2 e3 => CAS (γ e1) (γ e2) (γ e3)
    | MkLocation e1 e2 e3 => MkLocation (γ e1) (γ e2) (γ e3)
    | Syscall s e => Syscall s (γ e)
    | GatedCall s el => GatedCall s (γ <$> el)
    | GatedRet e => GatedRet (γ e)
    | GetPriv e => GetPriv (γ e)
    | GetBlock e => GetBlock (γ e)
    | GetOffset e => GetOffset (γ e)
    end.
  Proof.
    move: e => [];
     try by rewrite /subst_env; elim: (map_to_list γ) => [|[[]]] => //=;
      (try by move => *; rewrite list_fmap_id);
       move => ? ? ? IH ? ?; rewrite IH; f_equal; rewrite -list_fmap_compose; by apply list_fmap_ext.
    - move => x.
      have [[? Hr]| /eq_None_not_Some Hr]:= decide (is_Some $ γ !! BNamed x); rewrite Hr /=.
      + rewrite -(insert_id _ _ _ Hr) subst_subst_env /=.
        case_bool_decide => //.
        by apply subst_env_closed, is_closed_of_val.
      + rewrite -(list_to_map_to_list γ) in Hr.
        move/not_elem_of_list_to_map in Hr.
        rewrite /subst_env; elim: (map_to_list γ) Hr; csimpl=>// ?? IH ?.
        rewrite (is_closed_subst' [BNamed x]). by set_solver.
          by set_unfold; case_bool_decide; set_solver.
          by set_solver.
    - elim/(map_ind (M:=gmap binder) (A:=val)): γ.
      + move=> f xl e. rewrite subst_env_empty -{1}(subst_env_empty e).
        do 2 f_equal. elim: xl => [|? ? IH] /=. by rewrite delete_empty.
        by rewrite delete_commute -IH delete_empty.
      + move => b v γ Hnin IH f xl e.
        rewrite subst_subst_env.
        have ->: (subst' b v (rec: f xl := e) = (rec: f xl :=
              if bool_decide (b ≠ f ∧ b ∉ xl)
              then subst' b v e
              else e)%E) by move: {Hnin} b => [] //=; case_bool_decide.

        have [?|Hxl]:= (decide (b ∈ xl)); first by
          rewrite foldr_delete_insert //; case_bool_decide; naive_solver.
        rewrite foldr_delete_insert_ne //.
        have [->|Hf]:= (decide (b = f)); first by
          rewrite delete_insert_delete; case_bool_decide; naive_solver.
        rewrite delete_insert_ne // subst_subst_env.
        by case_bool_decide; naive_solver.
  Qed.

  Lemma subst_env_delete_anon e γ :
    (delete BAnon γ) e = γ e.
  Proof.
    move: e γ. fix FIX 1. destruct e; move => *; setoid_rewrite subst_env_expr; f_equal; try by apply FIX.
    - f_equal. by apply lookup_delete_ne.
    - by rewrite -foldr_delete_commute delete_commute.
    - elim: el => //= ???. by f_equal.
    - elim: el => //= ???. by f_equal.
    - elim: el => //= ???. by f_equal.
  Qed.

  Lemma subst_env_let b e1 e2 γ:
    (γ (let: b := e1 in e2)%E) = (let: b := (γ e1) in ((delete b γ) e2))%E.
  Proof. by rewrite 2!subst_env_expr /= subst_env_delete_anon. Qed.
  Lemma subst_env_seq γ e1 e2 :
    (γ (e1 ;; e2)%E) = ((γ e1) ;; (γ e2))%E.
  Proof. by rewrite subst_env_let /= subst_env_delete_anon. Qed.

  Lemma subst_env_var_fmap (vl : list _) (γ : env) (xl : list _):
    length vl = length xl →
    NoDup xl →
    subst_map xl vl ⊆ γ →
    γ <$> (Var <$> xl) = of_val <$> vl.
  Proof.
    elim: xl vl. by case.
    move => /= x xl IH [//|v vl] /= [?] /NoDup_cons[??] Hsub.
    f_equal.
    - rewrite subst_env_expr.
      cut (γ !! BNamed x = Some v); first by move => ->.
      apply/lookup_weaken => //. by apply/lookup_insert.
    - apply IH => //.
      etrans; last by apply Hsub.
      by apply/insert_subseteq/subst_map_not_in.
  Qed.

End subst.
