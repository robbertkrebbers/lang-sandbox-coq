From iris.program_logic Require Export adequacy weakestpre.
From iris.algebra Require Import auth agree gmap.
From sandbox.lang Require Export heap type.
From sandbox.lang Require Import notation interface wrapper.

Class sandboxPreG Σ := SandboxPreG {
  sandbox_preG_irig :> invPreG Σ;
  sandbox_preG_heap :> inG Σ (authR heapUR);
  sandbox_preG_heap_freeable :> inG Σ (authR heap_freeableUR);
  sandbox_preG_heap_trace :> inG Σ (authR traceUR);
  sandbox_preG_heap_interface :> inG Σ (agreeR interfaceO);
}.

Definition sandboxΣ : gFunctors :=
  #[invΣ;
    GFunctor (constRF (authR heapUR));
    GFunctor (constRF (authR heap_freeableUR));
    GFunctor (constRF (authR traceUR));
    GFunctor (constRF (agreeR interfaceO))].
Instance subG_sandboxPreG {Σ} : subG sandboxΣ Σ → sandboxPreG Σ.
Proof. solve_inG. Qed.

Definition is_good (SP : syscall_policy) (σ : state) : Prop := is_Some $ run_sp_trace SP σ.(trace).

Definition block_to_loc (l: namespace): list base_lit → gmap loc val :=
  list_to_map ∘ imap (λ i b, (global_to_loc l +ₗ i, LitV b)).

Lemma block_to_loc_nil l :
  block_to_loc l [] = ∅.
Proof. done. Qed.

Lemma block_to_loc_app l bs x :
  block_to_loc l (bs ++ [x]) =
  <[global_to_loc l +ₗ length bs:=LitV x]> (block_to_loc l bs).
Proof.
  rewrite /block_to_loc /= -list_to_map_cons.
  apply list_to_map_proper; first by apply imap_fst_NoDup, _.
  rewrite imap_app /= -plus_n_O.
    by symmetry; apply Permutation_cons_append.
Qed.

Lemma block_to_loc_comm j1 j2 z1 z2 y:
  j1 ≠ j2 →
  block_to_loc j1 z1 ∪ (block_to_loc j2 z2 ∪ y) =
  block_to_loc j2 z2 ∪ (block_to_loc j1 z1 ∪ y).
Proof.
  move => ?. rewrite !assoc. f_equal. apply map_union_comm.
  apply/map_disjoint_list_to_map_l/Forall_forall => *.
  apply not_elem_of_list_to_map_1. set_solver.
Qed.

Definition initial_state (gm : gmap namespace (list base_lit)) (I: priv → interface_map)
  : state := {|
              heap := map_fold (λ b ls m, block_to_loc b ls ∪ m) ∅ gm;
              trace := [];
              interface := I;
            |}.

(* Lemma globals_to_state_empty: *)
  (* globals_to_state ∅ = empty_state. *)
(* Proof. done. Qed. *)

Lemma initial_state_insert gm n bs I:
  gm !! n = None →
  initial_state (<[n := bs]> gm) I = heap_fupd
     (union (block_to_loc n bs)) (initial_state gm I).
Proof.
  move => ?. rewrite /initial_state /heap_fupd /=. f_equal.
  rewrite map_fold_insert_L // => *. by apply block_to_loc_comm.
Qed.

Lemma initial_state_insert_nil gm n I:
  gm !! n = None →
  initial_state (<[n := []]> gm) I = initial_state gm I.
Proof.
  move => ?. by rewrite initial_state_insert // block_to_loc_nil /heap_fupd left_id.
Qed.

Lemma initial_state_app gm n bs x I:
  gm !! n = None →
  initial_state (<[n := bs ++ [x] ]> gm) I = heap_fupd
    (<[global_to_loc n +ₗ length bs := LitV x]>)
    (initial_state (<[n := bs]> gm) I).
Proof.
  move => ?. rewrite initial_state_insert // initial_state_insert // /heap_fupd /=. f_equal.
    by rewrite insert_union_l block_to_loc_app.
Qed.

Lemma initial_state_lookup m k (n : nat) I:
  heap (initial_state m I) !! (global_to_loc k +ₗ n) =
  m !! k ≫= (λ (bs : list base_lit), LitV <$> (bs !! n)).
Proof.
  rewrite /initial_state /=.
  induction m as [|l bs m Hl IH] using map_ind => //.
  rewrite map_fold_insert_L // => *; last by apply block_to_loc_comm.
  have [?|?] := decide (k = l); subst; simpl_map => /=.
  - have [[? Hin]|/eq_None_not_Some Hin] := decide (is_Some (bs !! n)); rewrite Hin => /=.
    + apply lookup_union_Some_l, elem_of_list_to_map_1; first by apply imap_fst_NoDup, _. set_solver.
    + apply lookup_union_None. rewrite IH Hl. split => //.
      apply not_elem_of_list_to_map_1. set_solver.
  - rewrite lookup_union_r //. apply not_elem_of_list_to_map_1.
    set_solver.
Qed.

Lemma initial_state_lookup_Some_lit m k x I:
  heap (initial_state m I) !! k = Some x → ∃ b, x = LitV b.
Proof.
  induction m as [|b n ?? IH] using map_ind => //=.
  rewrite map_fold_insert_L// => Hf *; last by apply block_to_loc_comm.
  move: Hf. have [[? Hin]|/eq_None_not_Some ?] := decide (is_Some (block_to_loc b n !! k)); last by rewrite lookup_union_r.
  move => /lookup_union_Some_raw. rewrite Hin => [[[<-]|[??]]] //.
  move/elem_of_list_to_map in Hin. set_unfold.
  move: Hin => []; first by apply imap_fst_NoDup, _. naive_solver.
Qed.

Lemma interface_init `{sandboxG Σ} I:
  high_interface_def I -∗
  ([∗ map] e∈I, ∃ n, e ◁ fn High (vreplicate n any) any) ={⊤}=∗ interface_ctx.
Proof.
  iIntros "HI He". rewrite /interface_ctx fixpoint_unfold.
  do 2 iModIntro. iExists _. rewrite high_interface_eq. iFrame.
  iApply (big_sepM_impl with "He").
  iIntros "!#" (s v Hin).
  iDestruct 1 as (n) "#Hfn".
  iIntros "!#" (vs Φ) "Hg Hv HΦ".
  iAssert (v ◁ fn High (vreplicate n any) any)%I as "H'". done.
  iDestruct "H'" as (????->?) "_".
  iApply app_argument_length_stuck. iIntros (Hlen).
  iApply (type_fn_call with "Hfn [Hv] [$]"); subst n.
  - by rewrite fmap_length.
  - rewrite vec_to_list_replicate {}Hlen. iClear "Hfn".
    iInduction vs as [] "IH" => //=.
    iDestruct "Hv" as "[$ ?]".  by iApply "IH".
  - iIntros (?) "? ?". by iApply "HΦ".
Qed.

(** This is Theorem 3 (Robust Safety).
    The numbers here refer to the numbers in Theorem 3.
 *)
Theorem robust_safety Σ `{sandboxPreG Σ} A SP m vu vt main t2 σ2 :
  let _ := SyscallG A in
  (* (5) *)
  let I := λ p, match p with
                | Low => vu
                | High => vt
                end in
  (* SPG is SP extended with an invariant as explained in Section 7
  and defined in syscall_policies.v. We cannot quantify over SPG at
  the top-level since it depends on [sandboxSPPreG]. *)
  (∀ {HPre : sandboxSPPreG Σ}, ∃ SPG,
   (* This theorem works for any SPG whose safety property is SP and
   axioms on the trace are equivalent to A. *)
   SPG.(spg_SP) = SP ∧ (∀ π, SPG.(spg_axioms) π ↔ A π) ∧
   (* With SPG it is possible to create an instance of sandboxG, which
   contains SPG. In the paper iProp is sometimes explicitly
   parametrized by SP and A. Here this is done impliclitly by being
   parametric over sandboxG. *)
   (∀ `{sandboxG Σ},
       (* The created sandboxG contains SPG and the previous sandboxSPPreG *)
       heap_SP = SPG → heap_sandboxPre = HPre →
       (* (2) This corresponds to InitFromHeap. [m] is a map from
       names of global variables to initial values of them and this
       states that for all global variables k with initial values qs,
       k (with the name converted to a location) points to qs. Tinit
       is implicit here since in this Coq development typing contexts
       are directly represented by their semantic interpretation in
       Iris. *)
       ([∗ map] k↦qs∈m, (global_to_loc k) ↦∗ (LitV <$> qs)) ={⊤}=∗
              (* (4) *)
              ([∗ map] e∈vt, ∃ n, e ◁ fn High (vreplicate n any) any) ∗
              (* (3) *)
              main ◁ fn High [# ] any)) →
  (* ensures that the untrusted code is purely surface values  *)
  map_Forall (λ _ v, surface_val v) vu →
  (* (6) *)
  rtc erased_step ([(main [] at High)%E], initial_state m I) (t2, σ2) →
  (* (7) *)
  is_good SP σ2.
Proof.
  move => HSG I Hwp Hu Hsteps.
  eapply (wp_invariance _ _ MaybeStuck), Hsteps => ??.

  set h := to_heap (initial_state m I).
  iMod (own_alloc (Auth (Some (1%Qp, to_agree h)) h)) as (γh) "Hh" => //.
  { apply auth_valid_discrete => /=. split => //. exists h. eauto using to_heap_valid. }
  rewrite (auth_both_op h). iDestruct "Hh" as "[Hh Hm]".
  iMod (own_alloc (● (∅ : heap_freeableUR))) as (γf) "Hf" => //. by apply auth_auth_valid.
  iMod (own_alloc (● to_trace [])) as (γt) "Ht".
  { apply auth_auth_valid. constructor. }
  iMod (own_alloc (to_agree (I High : interfaceO))) as (γI) "#HI" => //.
  set (HpreSP := SandboxSPPreG _ _ _ _ _ _ γh γf γt γI).

  move: (Hwp HpreSP) => {Hwp} [SPG [<- [HAx Hwp]]].
  iMod (spg_init SPG) as (γs) "Hs" => //.
  have {HAx} HAx : (∀ π, @sysaxioms (@heap_axioms _ HpreSP) π ↔ spg_axioms SPG π) by move => ?; rewrite /= -HAx.
  set (HHeap := HeapG _ _ _ γs HAx).
  set (Hsandbox := SandboxG _ _ HHeap).
  move: (Hwp Hsandbox) => {Hwp} Hinit //.

  iMod (Hinit with "[Hm]") as "[#Hfn Hmain]" => //. {
    rewrite {}/h /=. iClear "HI". clear Hsteps Hinit.
      iInduction m as [|k bs ? gm ?] "IH" using map_ind => //.
      iApply big_sepM_insert => //. rewrite {2}/heap_mapsto_vec.
      iInduction bs as [|??] "IH2" using rev_ind => //; csimpl.
      + iSplitR=>//. iApply "IH". by rewrite initial_state_insert_nil.
      + rewrite fmap_app big_sepL_app initial_state_app //= to_heap_insert_high //= fmap_length -plus_n_O right_id.
        rewrite (insert_singleton_op (to_heap _)); last first. {
          apply lookup_to_heap_None. rewrite initial_state_lookup.
          simpl_map => /=. by rewrite lookup_ge_None_2.
        }
        rewrite auth_frag_op own_op {3}heap_mapsto_eq.
        iDestruct "Hm" as "[$ Hm]". by iApply "IH2".
  }

  iMod (interface_init with "HI Hfn") as "#?" => //.
  iModIntro. iExists (λ σ _ _, heap_ctx σ), (λ _, True)%I.

  iSplitR "Hmain"; last iSplitL.
  - (* heap_ctx *)
    iExists _, _. rewrite full_trace_eq high_interface_eq.
    iFrame. iFrame "HI".
    do ! iSplit => //.
    + by iPureIntro; move => ??/initial_state_lookup_Some_lit [? ->].
    + iIntros ([]); eauto; iIntros (?? Hin).
      iDestruct (big_sepM_lookup _ _ _ _ Hin with "Hfn") as (?) "Hfn2".
      by iApply ty_surface.
  - (* WP *)
    by iApply (type_fn_call with "Hmain").
  - (* is_good *)
    iDestruct 1 as (? ?) "(_ & _ & _ & Hπ  & _ & _ & _ & _ & %)". iExists _. iModIntro.
    iPureIntro. by eexists _.
Qed.
