From iris.program_logic Require Export language ectx_language ectxi_language.
From stdpp Require Export strings binders namespaces.
From stdpp Require Import gmap.
From sandbox.lang Require Export base.
Set Default Proof Using "Type".

Open Scope Z_scope.

(** Expressions and vals. *)
Inductive priv : Set := High | Low.
Instance priv_dec_eq : EqDecision priv.
Proof. solve_decision. Defined.
Program Instance priv_countable : Countable priv := {|
  encode b := match b with | High => 1%positive | Low => 2%positive end;
  decode p := Some match p return priv with 1%positive => High | _ => Low end
|}.
Next Obligation. by intros []. Qed.
Definition priv_leb (p1 p2 : priv) : bool :=
  match p1, p2 with
  | High, Low => false
  | _, _ => true
  end.
Instance priv_le : SqSubsetEq priv := priv_leb.
Instance priv_le_po : PreOrder priv_le.
Proof. split; by repeat case. Qed.
Definition priv_to_Z (p : priv) : Z :=
  match p with
  | High => 1
  | Low => 0
  end.
Definition Z_to_priv (n : Z) : priv :=
  match n with
  | 0 => Low
  | _ => High
  end.
Definition other_priv (p : priv) : priv :=
  match p with
  | High => Low
  | Low => High
  end.

Definition block : Set := positive.
Definition loc : Set := priv * block * Z.

Bind Scope loc_scope with loc.
Delimit Scope loc_scope with L.
Open Scope loc_scope.

Inductive base_lit : Set :=
| LitPoison | LitLoc (l : loc) | LitInt (n : Z).
Inductive bin_op : Set :=
| PlusOp | MinusOp | LeOp | EqOp | OffsetOp.

(* same expressions as lambdaRust with the following exceptions:
   - order is missing from Read and Write
   - added Syscall instruction
   - added priv bit to Alloc
   - added privilege switching mechanism
   - added MkLocation, GetPriv, GetBlock, GetOffset instructions
 *)
Section expr.
Local Unset Elimination Schemes.
Inductive expr :=
| Var (x : string)
| Lit (l : base_lit)
| Rec (f : binder) (xl : list binder) (e : expr)
| BinOp (op : bin_op) (e1 e2 : expr)
| App (e : expr) (el : list expr)
| Read (e : expr)
| Write (e1 e2: expr)
| CAS (e0 e1 e2 : expr)
| Alloc (h : priv) (e : expr)
| Free (e1 e2 : expr)
| Case (e : expr) (el : list expr)
| Fork (e : expr)
| Syscall (s : string) (e : expr)
| GatedCall (s : namespace) (el : list expr)
| GatedRet (e : expr) (* Not part of surface syntax, only for operational semantics *)
| MkLocation (e1 : expr) (e2 : expr) (e3 : expr)
| GetPriv (e : expr)
| GetBlock (e : expr)
| GetOffset (e : expr)
.
End expr.

(* we need a stronger elimination scheme which has Forall P el *)
Lemma expr_ind (P : expr → Prop):
  (∀ x : string, P (Var x)) → (∀ l : base_lit, P (Lit l)) →
  (∀ (f1 : binder) (xl : list binder) (e : expr), P e → P (Rec f1 xl e)) →
  (∀ (op : bin_op) (e1 : expr), P e1 → ∀ e2 : expr, P e2 → P (BinOp op e1 e2)) →
  (∀ e : expr, P e → ∀ el : list expr, Forall P el → P (App e el)) →
  (∀ e : expr, P e → P (Read e)) →
  (∀ e1 : expr, P e1 → ∀ e2 : expr, P e2 → P (Write e1 e2)) →
  (∀ e0 : expr, P e0 → ∀ e1 : expr, P e1 → ∀ e2 : expr, P e2 → P (CAS e0 e1 e2)) →
  (∀ (h : priv) (e : expr), P e → P (Alloc h e)) →
  (∀ e1 : expr, P e1 → ∀ e2 : expr, P e2 → P (Free e1 e2)) →
  (∀ e : expr, P e → ∀ el : list expr, Forall P el → P (Case e el)) →
  (∀ e : expr, P e → P (Fork e)) →
  (∀ (s : string) (e : expr), P e → P (Syscall s e)) →
  (∀ (s : namespace) (el : list expr), Forall P el → P (GatedCall s el)) →
  (∀ e : expr, P e → P (GatedRet e)) →
  (∀ e1 : expr, P e1 → ∀ e2 : expr, P e2 → ∀ e3 : expr, P e3 → P (MkLocation e1 e2 e3)) →
  (∀ e : expr, P e → P (GetPriv e)) →
  (∀ e : expr, P e → P (GetBlock e)) →
  (∀ e : expr, P e → P (GetOffset e)) → ∀ e, P e.
Proof.
  move => ???? Happ ????? Hcase ?? Hcall ????? e. generalize dependent P => P.
  move: e. fix FIX 1. move => []; [auto|auto|auto|auto| |auto|auto|auto|auto|auto| |auto|auto| |auto..];
     move => ? el ???? Happ ????? Hcase ?? Hcall *; try apply Happ; try apply Hcase; try apply Hcall; try by [apply: FIX; auto];
     elim: el; auto.
Qed.

Arguments App _%E _%E.
Arguments Case _%E _%E.
Arguments GatedCall _ _%E.

Bind Scope expr_scope with expr.

Fixpoint is_closed (X : list string) (e : expr) : bool :=
  match e with
  | Var x => bool_decide (x ∈ X)
  | Lit _  => true
  | Rec f xl e => is_closed (f :b: xl +b+ X) e
  | BinOp _ e1 e2 | Write e1 e2 | Free e1 e2 =>
    is_closed X e1 && is_closed X e2
  | App e el | Case e el => is_closed X e && forallb (is_closed X) el
  | GatedCall _ el => forallb (is_closed X) el
  | Read e | Alloc _ e | Fork e | Syscall _ e | GatedRet e | GetPriv e | GetBlock e | GetOffset e => is_closed X e
  | CAS e0 e1 e2 | MkLocation e0 e1 e2 => is_closed X e0 && is_closed X e1 && is_closed X e2
  end.

Class Closed (X : list string) (e : expr) := closed : is_closed X e.
Instance closed_proof_irrel env e : ProofIrrel (Closed env e).
Proof. rewrite /Closed. apply _. Qed.
Instance closed_decision env e : Decision (Closed env e).
Proof. rewrite /Closed. apply _. Defined.

Fixpoint surface_expr (e : expr) : bool :=
  match e with
  | GatedRet _ => false
  | Var _ | Lit _  => true
  | Rec _ _ e => surface_expr e
  | BinOp _ e1 e2 | Write e1 e2 | Free e1 e2 =>
    surface_expr e1 && surface_expr e2
  | App e el | Case e el => surface_expr e && forallb surface_expr el
  | GatedCall _ el => forallb surface_expr el
  | Read e | Alloc _ e | Fork e | Syscall _ e  | GetPriv e | GetBlock e | GetOffset e => surface_expr e
  | CAS e0 e1 e2 | MkLocation e0 e1 e2 => surface_expr e0 && surface_expr e1 && surface_expr e2
  end.


Inductive val :=
| LitV (l : base_lit)
| RecV (f : binder) (xl : list binder) (e : expr) `{Closed (f :b: xl +b+ []) e}.

Bind Scope val_scope with val.

Definition of_val (v : val) : expr :=
  match v with
  | RecV f x e => Rec f x e
  | LitV l => Lit l
  end.

Definition to_val (e : expr) : option val :=
  match e with
  | Rec f xl e =>
    if decide (Closed (f :b: xl +b+ []) e) then Some (RecV f xl e) else None
  | Lit l => Some (LitV l)
  | _ => None
  end.

Definition surface_val (v : val) : bool := surface_expr (of_val v).

(** The state: trace and heaps of vals. *)
(** observations are tuples of (name, argument, returned value)*)
Definition observation : Type := (string * base_lit * base_lit).
Definition interface_map : Type := gmap namespace val.
Record state : Type := {
  heap: gmap loc val;
  trace: list observation;
  interface: priv → interface_map;
}.
Definition empty_state (I: priv → interface_map) : state := {| heap:= ∅; trace := []; interface := I |}.

Definition heap_fupd (f : gmap loc val -> gmap loc val) (σ : state) : state :=
  {| heap := f σ.(heap); trace:= σ.(trace); interface := σ.(interface) |}.
(* adds the observation to the end of the trace *)
Definition add_observation (o : observation) (σ : state) : state :=
  {| trace := σ.(trace) ++ [o]; heap := σ.(heap); interface := σ.(interface) |}.

(** Evaluation contexts *)
Inductive ectx_item :=
| BinOpLCtx (op : bin_op) (e2 : expr)
| BinOpRCtx (op : bin_op) (v1 : val)
| AppLCtx (e2 : list expr)
| AppRCtx (v : val) (vl : list val) (el : list expr)
| ReadCtx
| WriteLCtx (e2 : expr)
| WriteRCtx (v1 : val)
| CasLCtx (e1 e2: expr)
| CasMCtx (v0 : val) (e2 : expr)
| CasRCtx (v0 : val) (v1 : val)
| AllocCtx (h: priv)
| FreeLCtx (e2 : expr)
| FreeRCtx (v1 : val)
| CaseCtx (el : list expr)
| SyscallCtx (s : string)
| GatedCallCtx (s : namespace) (vl : list val) (el : list expr)
| GatedRetCtx
| MkLocationLCtx (e1 e2: expr)
| MkLocationMCtx (v0 : val) (e2 : expr)
| MkLocationRCtx (v0 : val) (v1 : val)
| GetPrivCtx | GetBlockCtx | GetOffsetCtx
.

Definition fill_item (Ki : ectx_item) (e : expr) : expr :=
  match Ki with
  | BinOpLCtx op e2 => BinOp op e e2
  | BinOpRCtx op v1 => BinOp op (of_val v1) e
  | AppLCtx e2 => App e e2
  | AppRCtx v vl el => App (of_val v) ((of_val <$> vl) ++ e :: el)
  | ReadCtx => Read e
  | WriteLCtx e2 => Write e e2
  | WriteRCtx v1 => Write (of_val v1) e
  | CasLCtx e1 e2 => CAS e e1 e2
  | CasMCtx v0 e2 => CAS (of_val v0) e e2
  | CasRCtx v0 v1 => CAS (of_val v0) (of_val v1) e
  | AllocCtx h => Alloc h e
  | FreeLCtx e2 => Free e e2
  | FreeRCtx v1 => Free (of_val v1) e
  | CaseCtx el => Case e el
  | SyscallCtx s => Syscall s e
  | GatedCallCtx s vl el => GatedCall s ((of_val <$> vl) ++ e :: el)
  | GatedRetCtx => GatedRet e
  | MkLocationLCtx e1 e2 => MkLocation e e1 e2
  | MkLocationMCtx v0 e2 => MkLocation (of_val v0) e e2
  | MkLocationRCtx v0 v1 => MkLocation (of_val v0) (of_val v1) e
  | GetPrivCtx => GetPriv e
  | GetBlockCtx => GetBlock e
  | GetOffsetCtx => GetOffset e
  end.

(** Substitution *)
Fixpoint subst (x : string) (es : expr) (e : expr)  : expr :=
  match e with
  | Var y => if bool_decide (y = x) then es else Var y
  | Lit l => Lit l
  | Rec f xl e =>
    Rec f xl $ if bool_decide (BNamed x ≠ f ∧ BNamed x ∉ xl) then subst x es e else e
  | BinOp op e1 e2 => BinOp op (subst x es e1) (subst x es e2)
  | App e el => App (subst x es e) (map (subst x es) el)
  | Read e => Read (subst x es e)
  | Write e1 e2 => Write (subst x es e1) (subst x es e2)
  | CAS e0 e1 e2 => CAS (subst x es e0) (subst x es e1) (subst x es e2)
  | Alloc h e => Alloc h (subst x es e)
  | Free e1 e2 => Free (subst x es e1) (subst x es e2)
  | Case e el => Case (subst x es e) (map (subst x es) el)
  | Fork e => Fork (subst x es e)
  | Syscall s e => Syscall s (subst x es e)
  | GatedCall s el => GatedCall s (map (subst x es) el)
  | GatedRet e => GatedRet (subst x es e)
  | MkLocation e0 e1 e2 => MkLocation (subst x es e0) (subst x es e1) (subst x es e2)
  | GetPriv e => GetPriv (subst x es e)
  | GetBlock e => GetBlock (subst x es e)
  | GetOffset e => GetOffset (subst x es e)
  end.

Definition subst' (mx : binder) (es : expr) : expr → expr :=
  match mx with BNamed x => subst x es | BAnon => id end.
Fixpoint subst_l (xl : list binder) (esl : list expr) (e : expr) : option expr :=
  match xl, esl with
  | [], [] => Some e
  | x::xl, es::esl => subst' x es <$> subst_l xl esl e
  | _, _ => None
  end.
Arguments subst_l _%binder _ _%E.

Lemma subst_l_Some xl esl e e':
  subst_l xl esl e = Some e' → length xl = length esl.
Proof.
  elim: xl esl e'; first by case.
  move => ?? IH [] //=??? /fmap_Some[? [? _]]. f_equal. by apply: IH.
Qed.

Fixpoint subst_exprs (l : list (binder * expr)) (e : expr) : expr :=
  match l with
  | [] => e
  | (x, es)::ls => subst' x es $ subst_exprs ls e
  end.
Arguments subst_exprs _ _%E.

Definition subst_v (xl : list binder) (vsl : vec val (length xl))
                   (e : expr) : expr :=
  Vector.fold_right2 (λ b, subst' b ∘ of_val) e _ (list_to_vec xl) vsl.
Arguments subst_v _%binder _ _%E.

Lemma subst_v_eq (xl : list binder) (vsl : vec val (length xl)) e :
  Some $ subst_v xl vsl e = subst_l xl (of_val <$> vec_to_list vsl) e.
Proof.
  revert vsl. induction xl=>/= vsl; inv_vec vsl=>//=v vsl. by rewrite -IHxl.
Qed.

Lemma subst_v_eq_subst_exprs ml xl e :
  subst_v ml xl e = subst_exprs (zip ml (of_val <$> vec_to_list xl)) e.
Proof.
  elim: ml xl e. by move => xl; inv_vec xl.
  move => /= m ml IH xl; inv_vec xl => x xl ? /=; unfold subst_v in * => /=.
  f_equal. by apply: IH.
Qed.

(** Thread state: expression and privilege bit  *)
Record tstate : Type := mkTState { tstate_expr :> expr; tstate_priv: priv }.
Record tstate_val : Type := mkTStateVal { tstatev_val :> val; tstatev_priv: priv }.
Bind Scope expr_scope with tstate.
Bind Scope val_scope with tstate_val.

Definition tstate_of_val (v : tstate_val) : tstate := mkTState (of_val v) (v.(tstatev_priv)).
Definition tstate_to_val (t : tstate) : option tstate_val :=
  flip mkTStateVal (t.(tstate_priv)) <$> (to_val t).
Definition tstate_fill_item (Ki : ectx_item) (e : tstate) : tstate :=
  mkTState (fill_item Ki e) e.(tstate_priv).
Arguments tstate_fill_item /.

(** The stepping relation *)
(* Be careful to make sure that poison is always stuck when used for anything
   except for reading from or writing to memory! *)
Class syscallG : Type := SyscallG {
 sysaxioms : list observation → Prop
}.

Definition Z_of_bool (b : bool) : Z :=
  if b then 1 else 0.

Definition lit_of_bool (b : bool) : base_lit :=
  LitInt $ Z_of_bool b.

Definition shift_loc (l : loc) (z : Z) : loc := (l.1, l.2 + z).

Notation "l +ₗ z" := (shift_loc l%L z%Z)
  (at level 50, left associativity) : loc_scope.

Fixpoint init_mem (l:loc) (n:nat) (σ:state) : state :=
  match n with
  | O => σ
  | S n => heap_fupd (<[l:=(LitV LitPoison)]>) (init_mem (l +ₗ 1) n σ)
  end.

Fixpoint free_mem (l:loc) (n:nat) (σ:state) : state :=
  match n with
  | O => σ
  | S n => heap_fupd (delete l) (free_mem (l +ₗ 1) n σ)
  end.


Inductive lit_eq (σ : state) : base_lit → base_lit → Prop :=
(* No refl case for poison *)
| IntRefl z : lit_eq σ (LitInt z) (LitInt z)
| LocRefl l : lit_eq σ (LitLoc l) (LitLoc l)
(* see comment in LambdaRust for explaination of these cases *)
| LocUnallocL l1 l2 :
    σ.(heap) !! l1 = None →
    lit_eq σ (LitLoc l1) (LitLoc l2)
| LocUnallocR l1 l2 :
    σ.(heap) !! l2 = None →
    lit_eq σ (LitLoc l1) (LitLoc l2).

Inductive lit_neq : base_lit → base_lit → Prop :=
| IntNeq z1 z2 :
    z1 ≠ z2 → lit_neq (LitInt z1) (LitInt z2)
| LocNeq l1 l2 :
    l1 ≠ l2 → lit_neq (LitLoc l1) (LitLoc l2)
| LocNeqNullR l :
    lit_neq (LitLoc l) (LitInt 0)
| LocNeqNullL l :
    lit_neq (LitInt 0) (LitLoc l).

Inductive bin_op_eval (σ : state) : bin_op → base_lit → base_lit → base_lit → Prop :=
| BinOpPlus z1 z2 :
    bin_op_eval σ PlusOp (LitInt z1) (LitInt z2) (LitInt (z1 + z2))
| BinOpMinus z1 z2 :
    bin_op_eval σ MinusOp (LitInt z1) (LitInt z2) (LitInt (z1 - z2))
| BinOpLe z1 z2 :
    bin_op_eval σ LeOp (LitInt z1) (LitInt z2) (lit_of_bool $ bool_decide (z1 ≤ z2))
| BinOpEqTrue l1 l2 :
    lit_eq σ l1 l2 → bin_op_eval σ EqOp l1 l2 (lit_of_bool true)
| BinOpEqFalse l1 l2 :
    lit_neq l1 l2 → bin_op_eval σ EqOp l1 l2 (lit_of_bool false)
| BinOpOffset l z :
    bin_op_eval σ OffsetOp (LitLoc l) (LitInt z) (LitLoc $ l +ₗ z).

Inductive head_step `{!syscallG} : tstate → state → list observation → tstate → state → list tstate → Prop :=
| BinOpS op l1 l2 l' σ p :
    bin_op_eval σ op l1 l2 l' →
    head_step (mkTState (BinOp op (Lit l1) (Lit l2)) p) σ [] (mkTState (Lit l') p) σ []
| BetaS f xl e e' el σ p:
    Forall (λ ei, is_Some (to_val ei)) el →
    Closed (f :b: xl +b+ []) e →
    subst_l (f::xl) (Rec f xl e :: el) e = Some e' →
    head_step (mkTState (App (Rec f xl e) el) p) σ [] (mkTState e' p) σ []
| ReadS l v σ p: (* Reads are always allowed (no confidentiality) *)
    σ.(heap) !! l = Some v →
    head_step (mkTState (Read (Lit $ LitLoc l)) p) σ [] (mkTState (of_val v) p) σ []
| WriteS l e v v' σ p:
    to_val e = Some v →
    σ.(heap) !! l = Some v' →
    l.1.1 ⊑ p →
    head_step (mkTState (Write (Lit $ LitLoc l) e) p) σ
              []
              (mkTState (Lit LitPoison) p) (heap_fupd (<[l:=v]>) σ)
              []
| CasFailS l e1 lit1 e2 lit2 litl σ p :
    to_val e1 = Some $ LitV lit1 → to_val e2 = Some $ LitV lit2 →
    σ.(heap) !! l = Some (LitV litl) →
    lit_neq lit1 litl →
    l.1.1 ⊑ p →
    head_step (mkTState (CAS (Lit $ LitLoc l) e1 e2) p) σ [] (mkTState (Lit $ lit_of_bool false) p) σ  []
| CasSucS l e1 lit1 e2 lit2 litl σ p:
    to_val e1 = Some $ LitV lit1 → to_val e2 = Some $ LitV lit2 →
    σ.(heap) !! l = Some (LitV litl) →
    lit_eq σ lit1 litl →
    l.1.1 ⊑ p →
    head_step (mkTState (CAS (Lit $ LitLoc l) e1 e2) p) σ
              []
              (mkTState (Lit $ lit_of_bool true) p) (heap_fupd (insert l (LitV lit2)) σ)
              []
| AllocS n h l σ p :
    0 < n →
    l.1.1 = h →
    (∀ m, σ.(heap) !! (l +ₗ m) = None) →
    h ⊑ p →
    head_step (mkTState (Alloc h $ Lit $ LitInt n) p) σ
              []
              (mkTState (Lit $ LitLoc l) p) (init_mem l (Z.to_nat n) σ)
              []
| FreeS n l σ p:
    0 < n →
    (∀ m, is_Some (σ.(heap) !! (l +ₗ m)) ↔ 0 ≤ m < n) →
    l.1.1 ⊑ p →
    head_step (mkTState (Free (Lit $ LitInt n) (Lit $ LitLoc l)) p) σ
              []
              (mkTState (Lit LitPoison) p) (free_mem l (Z.to_nat n) σ)
              []
| CaseS i el e σ p:
    0 ≤ i →
    el !! (Z.to_nat i) = Some e →
    head_step (mkTState (Case (Lit $ LitInt i) el) p) σ [] (mkTState e p) σ []
| ForkS e σ p:
    head_step (mkTState (Fork e) p) σ [] (mkTState (Lit LitPoison) p) σ [(mkTState e p)]
| SyscallS s v r σ: (* only allowed at High *)
    sysaxioms ((add_observation (s, v, r) σ).(trace)) →
   head_step (mkTState (Syscall s (Lit v)) High) σ [(s, v, r)] (mkTState (Lit r) High) (add_observation (s, v, r) σ) []
| GatedCallS σ s p el v:
    Forall (λ ei, is_Some (to_val ei)) el →
    σ.(interface) (other_priv p) !! s = Some v →
    head_step (mkTState (GatedCall s el) p) σ [] (mkTState (GatedRet $ App (of_val v) el) (other_priv p)) σ []
| GatedRetS σ p e v:
    to_val e = Some v →
    head_step (mkTState (GatedRet e) p) σ [] (mkTState e (other_priv p)) σ []
| MkLocationS n b o σ p :
    0 ≤ b →
    head_step (mkTState (MkLocation (Lit $ LitInt n) (Lit $ LitInt b) (Lit $ LitInt o)) p)
              σ [] (mkTState (Lit $ LitLoc (Z_to_priv n, Z.to_pos b, o)) p) σ []
| GetPrivS l σ p :
    head_step (mkTState (GetPriv (Lit $ LitLoc l)) p)
              σ [] (mkTState (Lit $ LitInt $ priv_to_Z l.1.1) p) σ []
| GetBlockS l σ p :
    head_step (mkTState (GetBlock (Lit $ LitLoc l)) p)
              σ [] (mkTState (Lit $ LitInt $ Z.pos l.1.2) p) σ []
| GetOffsetS l σ p :
    head_step (mkTState (GetOffset (Lit $ LitLoc l)) p)
              σ [] (mkTState (Lit $ LitInt l.2) p) σ []
.

(** Basic properties about the language *)
Lemma to_of_val v : to_val (of_val v) = Some v.
Proof.
  by induction v; simplify_option_eq; repeat f_equal; try apply (proof_irrel _).
Qed.

Lemma of_to_val e v : to_val e = Some v → of_val v = e.
Proof.
  revert v; induction e; intros v ?; simplify_option_eq; auto with f_equal.
Qed.

Instance of_val_inj : Inj (=) (=) of_val.
Proof. by intros ?? Hv; apply (inj Some); rewrite -!to_of_val Hv. Qed.

Instance fill_item_inj Ki : Inj (=) (=) (fill_item Ki).
Proof. destruct Ki; intros ???; simplify_eq/=; auto with f_equal. Qed.

Lemma fill_item_val Ki e :
  is_Some (to_val (fill_item Ki e)) → is_Some (to_val e).
Proof. intros [v ?]. destruct Ki; simplify_option_eq; eauto. Qed.

Lemma list_expr_val_eq_inv vl1 vl2 e1 e2 el1 el2 :
  to_val e1 = None → to_val e2 = None →
  map of_val vl1 ++ e1 :: el1 = map of_val vl2 ++ e2 :: el2 →
  vl1 = vl2 ∧ el1 = el2.
Proof.
  revert vl2; induction vl1; destruct vl2; intros H1 H2; inversion 1.
  - done.
  - subst. by rewrite to_of_val in H1.
  - subst. by rewrite to_of_val in H2.
  - destruct (IHvl1 vl2); auto. split; f_equal; auto. by apply (inj of_val).
Qed.

Lemma fill_item_no_val_inj Ki1 Ki2 e1 e2 :
  to_val e1 = None → to_val e2 = None →
  fill_item Ki1 e1 = fill_item Ki2 e2 → Ki1 = Ki2.
Proof.
  destruct Ki1 as [| | |v1 vl1 el1| | | | | | | | | | | | s1 vl1 el1 | | | | | | |],
           Ki2 as [| | |v2 vl2 el2| | | | | | | | | | | | s2 vl2 el2 | | | | | | |];
  intros He1 He2 EQ; try discriminate; simplify_eq/=;
    repeat match goal with
    | H : to_val (of_val _) = None |- _ => by rewrite to_of_val in H
    end; auto;
  destruct (list_expr_val_eq_inv vl1 vl2 e1 e2 el1 el2); auto; congruence.
Qed.

Class IntoVal (e : expr) (v : val) := into_val : of_val v = e.
Class AsVal (e : expr) := as_val : ∃ v, of_val v = e.
Global Instance as_vals_of_val vs : TCForall AsVal (of_val <$> vs).
Proof. apply TCForall_Forall, Forall_fmap, Forall_true=> v. rewrite /AsVal /=; eauto. Qed.
Global Instance into_vals_of_val vs : TCForall2 IntoVal (of_val <$> vs) vs.
Proof. elim: vs => /=. by constructor. move => ???. by constructor. Qed.

Lemma into_vals_inv n xl vl:
  TCForall2 IntoVal xl vl →
  length xl = n →
  ∃ (vl2 : vec val n), xl = of_val <$> vl ∧ vl = vec_to_list vl2.
Proof.
  move => Hval <-.
  have ?: (xl = of_val <$> vl) by elim: Hval => //= ???? -> _ ->.
  subst. rewrite fmap_length. exists (list_to_vec vl). split => //.
    by rewrite vec_to_list_of_list.
Qed.


(** basic properties about tstate *)
Lemma tstate_to_of_val v : tstate_to_val (tstate_of_val v) = Some v.
Proof. rewrite /tstate_of_val /tstate_to_val. move: v => [] * /=. by rewrite to_of_val /=. Qed.

Lemma tstate_of_to_val e v : tstate_to_val e = Some v → tstate_of_val v = e.
Proof.
  move: e v => [e ?] [? ?]. cbv -[to_val of_val].
  destruct (to_val e) eqn:He => // [[<- ->]]. f_equal. exact: of_to_val.
Qed.

Lemma to_expr_val e v:
  tstate_to_val e = Some v → to_val e = Some v.(tstatev_val).
Proof. destruct e, v. cbv -[to_val]. case_match; naive_solver. Qed.

Lemma to_expr_val_inv e v p:
  to_val e = Some v → tstate_to_val (mkTState e p) = Some (mkTStateVal v p).
Proof. by cbv -[to_val] => ->. Qed.

Lemma of_expr_val v p: mkTState (of_val v) p = tstate_of_val (mkTStateVal v p).
Proof. done. Qed.

Lemma to_expr_val_is_Some e:
  is_Some (to_val e.(tstate_expr)) → is_Some (tstate_to_val e).
Proof. move: e => [] /=. cbv [tstate_to_val] => /= *. by apply fmap_is_Some. Qed.

Instance tstate_of_val_inj : Inj (=) (=) tstate_of_val.
Proof. by intros ?? Hv; apply (inj Some); rewrite -!tstate_to_of_val Hv. Qed.

Instance tstate_fill_item_inj Ki : Inj (=) (=) (tstate_fill_item Ki).
Proof.
  intros [][]. cbv -[fill_item]. move => [? <-]. f_equal.
    by eapply fill_item_inj.
Qed.

Lemma tstate_fill_item_val Ki e :
  is_Some (tstate_to_val (tstate_fill_item Ki e)) → is_Some (tstate_to_val e).
Proof. intros [v ?]. destruct Ki; simplify_option_eq; eauto. Qed.

Lemma tstate_val_stuck `{!syscallG} e1 σ1 κ e2 σ2 ef :
  head_step e1 σ1 κ e2 σ2 ef → tstate_to_val e1 = None.
Proof. destruct 1; naive_solver. Qed.

Lemma tstate_head_ctx_step_val `{!syscallG} Ki e σ1 κ e2 σ2 ef :
  head_step (tstate_fill_item Ki e) σ1 κ e2 σ2 ef → is_Some (tstate_to_val e).
Proof.
  inversion 1; subst; apply fmap_is_Some;
  destruct Ki; decompose_Forall_hyps;
    simplify_option_eq; destruct e; simpl in *; subst; eauto => /=.
  case_decide => //. by apply: mk_is_Some.
Qed.

Lemma tstate_fill_item_no_val_inj Ki1 Ki2 e1 e2 :
  tstate_to_val e1 = None → tstate_to_val e2 = None →
  tstate_fill_item Ki1 e1 = tstate_fill_item Ki2 e2 → Ki1 = Ki2.
Proof. move => /fmap_None H1 /fmap_None H2 [] H3 ?. exact: fill_item_no_val_inj H1 H2 H3. Qed.

Lemma shift_loc_assoc l n n' : l +ₗ n +ₗ n' = l +ₗ (n + n').
Proof. rewrite /shift_loc /=. f_equal. lia. Qed.
Lemma shift_loc_0 l : l +ₗ 0 = l.
Proof. destruct l as [b o]. rewrite /shift_loc /=. f_equal. lia. Qed.

Lemma shift_loc_assoc_nat l (n n' : nat) : l +ₗ n +ₗ n' = l +ₗ (n + n')%nat.
Proof. rewrite /shift_loc /=. f_equal. lia. Qed.
Lemma shift_loc_0_nat l : l +ₗ 0%nat = l.
Proof. destruct l as [b o]. rewrite /shift_loc /=. f_equal. lia. Qed.

Instance shift_loc_inj l : Inj (=) (=) (shift_loc l).
Proof. destruct l as [b o]; intros n n' [=?]; lia. Qed.

Lemma shift_loc_block l n : (l +ₗ n).1 = l.1.
Proof. done. Qed.

Lemma lookup_init_mem σ (l l' : loc) (n : nat) :
  l.1 = l'.1 → l.2 ≤ l'.2 < l.2 + n →
  (init_mem l n σ).(heap) !! l' = Some (LitV LitPoison).
Proof.
  intros ?. destruct l' as [? l']; simplify_eq/=.
  revert l. induction n as [|n IH]=> /= l Hl; [lia|].
  assert (l' = l.2 ∨ l.2 + 1 ≤ l') as [->|?] by lia.
  { by rewrite -surjective_pairing lookup_insert. }
  rewrite lookup_insert_ne; last by destruct l; intros ?; simplify_eq/=; lia.
  rewrite -(shift_loc_block l 1) IH /=; last lia. done.
Qed.

Lemma lookup_init_mem_ne σ (l l' : loc) (n : nat) :
  l.1 ≠ l'.1 ∨ l'.2 < l.2 ∨ l.2 + n ≤ l'.2 →
  (init_mem l n σ).(heap) !! l' = σ.(heap) !! l'.
Proof.
  revert l. induction n as [|n IH]=> /= l Hl; auto.
  rewrite -(IH (l +ₗ 1)); last (simpl; intuition lia).
  apply lookup_insert_ne. intros ->; intuition lia.
Qed.

Definition fresh_block (σ : state) : block :=
  let loclst : list loc := elements (dom _ σ.(heap) : gset loc) in
  let blockset : gset block := foldr (λ l, ({[l.1.2]} ∪.)) ∅ loclst in
  fresh blockset.

Lemma is_fresh_block σ h i : σ.(heap) !! (h, fresh_block σ,i) = None.
Proof.
  assert (∀ (l : loc) ls (X : gset block),
    l ∈ ls → l.1.2 ∈ foldr (λ l, ({[l.1.2]} ∪.)) X ls) as help.
  { induction 1; set_solver. }
  rewrite /fresh_block /shift_loc /= -(not_elem_of_dom (D := gset loc)) -elem_of_elements.
  move=> /(help _ _ ∅) /=. apply is_fresh.
Qed.

Lemma alloc_fresh `{!syscallG} n σ h p:
  let l := (h, fresh_block σ, 0) in
  let init := repeat (LitV $ LitInt 0) (Z.to_nat n) in
  0 < n →
  h ⊑ p →
  head_step (mkTState (Alloc h $ Lit $ LitInt n) p) σ []
            (mkTState (Lit $ LitLoc l) p) (init_mem l (Z.to_nat n) σ) [].
Proof.
  intros l init Hn. apply AllocS => //.
  - intros i. apply (is_fresh_block _ h i).
Qed.

Lemma lookup_free_mem_ne σ l l' n : l.1 ≠ l'.1 → (free_mem l n σ).(heap) !! l' = σ.(heap) !! l'.
Proof.
  revert l. induction n as [|n IH]=> l ? //=.
  rewrite lookup_delete_ne; last congruence.
  apply IH. by rewrite shift_loc_block.
Qed.

Lemma free_mem_S σ l n :
  free_mem l (S n) σ = heap_fupd (delete l) (free_mem (l +ₗ 1) n σ).
Proof. done. Qed.

Lemma delete_free_mem σ l l' n :
  heap_fupd (delete l) (free_mem l' n σ) = free_mem l' n (heap_fupd (delete l) σ).
Proof.
  revert l'. induction n as [|n IH]=> l' //=. rewrite -IH.
  rewrite /heap_fupd. f_equiv => /=.
  by apply delete_commute.
Qed.

(* Operations on literals *)
Definition lit_to_Z (l : base_lit) : option Z :=
  match l with
  | LitInt n => Some n
  | _ => None
  end.
Definition lit_to_loc (l : base_lit) : option loc :=
  match l with
  | LitLoc n => Some n
  | _ => None
  end.
Definition lit_to_poison (l : base_lit) : option () :=
  match l with
  | LitPoison => Some ()
  | _ => None
  end.

Lemma lit_eq_state σ1 σ2 l1 l2 :
  (∀ l, σ1.(heap) !! l = None ↔ σ2.(heap) !! l = None) →
  lit_eq σ1 l1 l2 → lit_eq σ2 l1 l2.
Proof. intros Heq. inversion 1; econstructor; eauto; eapply Heq; done. Qed.

Lemma bin_op_eval_state σ1 σ2 op l1 l2 l' :
  (∀ l, σ1.(heap) !! l = None ↔ σ2.(heap) !! l = None) →
  bin_op_eval σ1 op l1 l2 l' → bin_op_eval σ2 op l1 l2 l'.
Proof.
  intros Heq. inversion 1; econstructor; eauto using lit_eq_state.
Qed.

Lemma Z_of_bool_1 P `{Decision P} :
  LitV (LitInt (Z_of_bool (bool_decide P))) = LitV (LitInt 1) ↔ P.
Proof. by case_bool_decide. Qed.

(** High and low locations *)
Definition high (l : loc) : Prop := l.1.1 = High.
Definition low (l : loc) : Prop := l.1.1 = Low.
Lemma high_shift l i : high l ↔ high (l +ₗ i).
Proof. done. Qed.
Lemma low_shift l i : low l ↔ low (l +ₗ i).
Proof. done. Qed.
Lemma low_high_excl l : low l → high l → False.
Proof. by rewrite /low /high => ->. Qed.
Lemma low_or_high l : {low l} + {high l}.
Proof. move: l => [[[]]]; by [right | left]. Qed.
Lemma priv_leb_low l : l.1.1 ⊑ Low → low l.
Proof. by move: l => [[[]]]. Qed.
Lemma priv_leb_high l : High ⊑ l.1.1 → high l.
Proof. by move: l => [[[]]]. Qed.

Lemma recv_f_equal f xl e1 e2 H:
  e1 = e2 → ∀ H2 : Closed (f :b: xl +b+ []) e1,
    @RecV f xl e1 H2 = @RecV f xl e2 H.
Proof. move => -> ?. f_equal. by apply proof_irrel. Qed.

(** Closed expressions *)
Lemma is_closed_weaken X Y e : is_closed X e → X ⊆ Y → is_closed Y e.
Proof.
  elim: e X Y; try naive_solver.
  - naive_solver set_solver.
  - move => /= ??? /Forall_forall ???. rewrite !andb_True => [[?/forallb_True/Forall_forall?]] ?.
    split; eauto. apply/forallb_True/Forall_forall; eauto.
  - move => /= ??? /Forall_forall ???. rewrite !andb_True => [[?/forallb_True/Forall_forall?]] ?.
    split; eauto. apply/forallb_True/Forall_forall; eauto.
  - move => /= ?? /Forall_forall ??? /forallb_True/Forall_forall? ?.
     apply/forallb_True/Forall_forall; eauto.
Qed.

Global Instance Closed_proper_perm : (Proper (Permutation ==> eq ==> iff) Closed).
Proof. move => a b HP ? ? ->. by split => ?; apply: is_closed_weaken => //; rewrite HP. Qed.
Lemma is_closed_weaken_nil X e : is_closed [] e → is_closed X e.
Proof. intros. by apply is_closed_weaken with [], list_subseteq_nil. Qed.
Lemma is_closed_subst X e x es : is_closed X e → x ∉ X → subst x es e = e.
Proof.
  revert X.
  elim: e; intros until X => /=; rewrite ?bool_decide_spec ?andb_True => He ?;
    repeat case_bool_decide; simplify_eq/=; f_equal;
    try by [intuition eauto with set_solver];
    try (move: He=> [_ He]);
    try (move: He=> /forallb_True/List.Forall_forall He);
    rewrite -{2}(map_id el); apply map_ext_in => ? ?.
  - move: H0 => /List.Forall_forall. apply; eauto.
  - move: H0 => /List.Forall_forall. apply; eauto.
  - move: H => /List.Forall_forall. apply; eauto.
Qed.
Lemma is_closed_nil_subst e x es : is_closed [] e → subst x es e = e.
Proof. intros. apply is_closed_subst with []; set_solver. Qed.
Lemma is_closed_of_val X v : is_closed X (of_val v).
Proof. apply is_closed_weaken_nil. induction v; simpl; auto. Qed.
Lemma is_closed_to_val X e v : to_val e = Some v → is_closed X e.
Proof. intros <-%of_to_val. apply is_closed_of_val. Qed.
Lemma is_closed_subst' X e x es : is_closed (X +b+ []) e → x ∉ X → subst' x es e = e.
Proof. move: x => [] => //= ???. eapply is_closed_subst; set_solver. Qed.
Lemma is_closed_nil_subst' e x es : is_closed ([]) e → subst' x es e = e.
Proof.  move: x => [] => //=?. by eapply is_closed_nil_subst. Qed.
Lemma subst_is_closed X x es e :
  is_closed X es → is_closed (x::X) e → is_closed X (subst x es e).
Proof.
  revert e X. fix FIX 1; destruct e=>X //=; repeat (case_bool_decide=>//=);
    try naive_solver; rewrite ?andb_True; intros.
  - set_solver.
  - eauto using is_closed_weaken with set_solver.
  - eapply is_closed_weaken; first done.
    destruct (decide (BNamed x = f)), (decide (BNamed x ∈ xl)); set_solver.
  - split; first naive_solver. induction el; naive_solver.
  - split; first naive_solver. induction el; naive_solver.
  - induction el; naive_solver.
Qed.
Lemma subst_is_closed_rev X x es e :
  is_closed X es → is_closed X (subst x es e) → is_closed (x::X) e.
Proof.
  revert e X. fix FIX 1; destruct e=>X //=; repeat (case_bool_decide=>//=);
                                       try naive_solver; rewrite ?andb_True; intros.
  - set_solver.
  - set_solver.
  - have Hc := (FIX _ _ _ H1).
    eauto using is_closed_weaken with set_solver.
  - eapply is_closed_weaken; first done.
    destruct (decide (BNamed x = f)), (decide (BNamed x ∈ xl)); set_solver.
  - split; first naive_solver. induction el; naive_solver.
  - split; first naive_solver. induction el; naive_solver.
  - induction el; naive_solver.
Qed.

Lemma subst'_is_closed X b es e :
  is_closed X es → is_closed (b:b:X) e → is_closed X (subst' b es e).
Proof. destruct b; first done. apply subst_is_closed. Qed.
Lemma subst'_is_closed_rev X b es e :
  is_closed X es → is_closed X (subst' b es e) → is_closed (b:b:X) e.
Proof. destruct b; first done. apply subst_is_closed_rev. Qed.

Lemma subst_exprs_app xl1 xl2 e :
  subst_exprs (xl1 ++ xl2) e = subst_exprs xl1 (subst_exprs xl2 e).
Proof. by move: xl1 xl2 e; elim => //= [[??]] /= ? IH2 ? ?; rewrite IH2. Qed.

Lemma subst_exprs_is_closed xl l e: is_closed (xl.*1 +b+ l) e -> Forall (is_closed l) xl.*2 -> is_closed l (subst_exprs xl e).
Proof.
  elim: xl l => //= [[x ex]] xl IH l /= Hc /Forall_cons [? Hf].
  rewrite -/(fmap) in Hc.
  apply: subst'_is_closed => //.
  apply: IH. by apply: is_closed_weaken; first by [apply: Hc]; set_solver.
  apply: Forall_impl => //= *. apply: is_closed_weaken. done. set_solver.
Qed.

Lemma subst_exprs_is_closed_rev xl l e: Forall (is_closed l) xl.*2 -> is_closed l (subst_exprs xl e) -> is_closed (xl.*1 +b+ l) e.
Proof.
  rewrite -(rev_involutive xl).
  elim: (rev xl) l e => //= {xl} [[x ex]] xl IH l e /=.
  rewrite subst_exprs_app 2!fmap_app /= => /Forall_app[HF /Forall_cons[Hf _]] HC.
  have {IH}IH:= IH _ _ HF HC.
  apply: is_closed_weaken. {
    apply: subst'_is_closed_rev _ _ _ _ _ IH.
    apply: is_closed_weaken => //. set_solver.
  }
  elim: (rev xl).*1 => //=. set_solver.
Qed.

Lemma subst_v_is_closed xl vl l e: is_closed (xl +b+ l) e -> is_closed l (subst_v xl vl e).
Proof.
  move => Hc.
  rewrite subst_v_eq_subst_exprs.
  apply: subst_exprs_is_closed. by rewrite fst_zip // fmap_length vec_to_list_length.
  rewrite snd_zip; last by rewrite fmap_length vec_to_list_length.
  elim: (vec_to_list vl); first by constructor.
  move => /= *.
  constructor => //.
  by apply: is_closed_of_val.
Qed.

Lemma is_closed_subst_exprs X e xl : is_closed X e → (xl.*1 +b+ []) ## X  → subst_exprs xl e = e.
Proof.
  elim: xl => //; csimpl.
  move => [mx es] /= xl IH Hc Hdj.
  rewrite IH => //; last by set_solver.
  destruct mx => //=.
  rewrite (is_closed_subst X) => //. by set_solver.
Qed.

Lemma is_closed_subst_v X e xl vl : is_closed X e → (xl +b+ []) ## X  → subst_v xl vl e = e.
Proof.
  move => ? Hd.
  rewrite subst_v_eq_subst_exprs (is_closed_subst_exprs X) //.
  by rewrite fst_zip // fmap_length vec_to_list_length.
Qed.

Lemma subst_comm mx1 es1 mx2 es2 e:
  Closed [] es1 -> Closed [] es2 ->
  mx1 ≠ mx2 ->
  subst mx1 es1 (subst mx2 es2 e) = subst mx2 es2 (subst mx1 es1 e).
Proof.
  rewrite /Closed.
  move => HC1 HC2 Hneq. move: e es1 es2 mx1 mx2 HC1 HC2 Hneq. fix FIX 1.
  case => //=; intros; try by [ f_equal; apply: FIX ].
  - destruct (bool_decide (x = mx2)) eqn:Hmx2; destruct (bool_decide (x = mx1)) eqn:Hmx1;
      rewrite ?(is_closed_nil_subst es1); rewrite ?(is_closed_nil_subst es2) => //;
        (try move /Is_true_eq_left /bool_decide_spec in Hmx1);
        (try move /Is_true_eq_left /bool_decide_spec in Hmx2); subst => //=.
    + rewrite bool_decide_true => //.
    + by rewrite /subst /= -/(subst) bool_decide_true.
    + rewrite /subst /= -/(subst) ! bool_decide_false //.
      { move /not_true_iff_false in Hmx2. contradict Hmx2.
          by apply /Is_true_eq_true /bool_decide_spec. }
      { move /not_true_iff_false in Hmx1. contradict Hmx1.
          by apply /Is_true_eq_true /bool_decide_spec. }
  - destruct (bool_decide (BNamed mx1 ≠ f ∧ BNamed mx1 ∉ xl)) eqn:Hmx1;
        destruct (bool_decide (BNamed mx2 ≠ f ∧ BNamed mx2 ∉ xl)) eqn:Hmx2 => //.
    f_equal. by apply: FIX.
  - f_equal; try by apply: FIX. elim: el => //= ? ? ->. f_equal. by apply: FIX.
  - f_equal; try by apply: FIX. elim: el => //= ? ? ->. f_equal. by apply: FIX.
  - f_equal; try by apply: FIX. elim: el => //= ? ? ->. f_equal. by apply: FIX.
Qed.

Lemma subst'_comm mx1 es1 mx2 es2 e:
  Closed [] es1 -> Closed [] es2 ->
  mx1 ≠ mx2 ->
  subst' mx1 es1 (subst' mx2 es2 e) = subst' mx2 es2 (subst' mx1 es1 e).
Proof. case mx1 => //. case mx2 => //???? Hneq. apply: subst_comm. naive_solver. Qed.

Lemma subst_exprs_swap mx es (xl : list (binder * expr)) e:
  Closed [] es ->
  Forall (Closed []) xl.*2 ->
  BNamed mx ∉ xl.*1 ->
  subst mx es (subst_exprs xl e) = subst_exprs xl (subst mx es e).
Proof.
  move => Hc.
  elim: xl e => //=[[x1 x2]] xl IH e /Forall_cons [??] Hneq.
  rewrite -IH => //; last by set_solver.
  destruct x1 => //=.
  setoid_rewrite subst_comm at 1 => //. by set_solver.
Qed.

Lemma subst_v_swap x xl v (vl : vec _ (length xl)) e:
  x ∉ xl ->
  subst_v (x :: xl) (v ::: vl) e = subst_v xl vl (subst' x (of_val v) e).
Proof.
  elim: xl vl e. { move => vl e. by inv_vec vl. }
  move => /= x2 xl IH vl e Hneq.
  inv_vec vl => v2 vl.
  unfold subst_v in *.
  rewrite /= -IH /=; last by set_solver.
  setoid_rewrite subst'_comm at 1 => //; last by set_solver.
  by destruct v.
  by destruct v2.
Qed.

Lemma subst_compose e mx es1 es2:
  subst mx es1 (subst mx es2 e) = subst mx (subst mx es1 es2) e.
Proof.
  revert e. fix FIX 1; case => //=; intros; try by [ f_equal; apply: FIX ];
                                 repeat (case_bool_decide => //=); f_equal; try by [ apply: FIX ].
  - elim: el => //= ? ? ->; f_equal; apply: FIX.
  - elim: el => //= ? ? ->; f_equal; apply: FIX.
  - elim: el => //= ? ? ->; f_equal; apply: FIX.
Qed.

Lemma subst'_compose e mx es1 es2:
  subst' mx es1 (subst' mx es2 e) = subst' mx (subst' mx es1 es2) e.
Proof. move: mx => [] //= ?. by apply subst_compose. Qed.

(** Equality and other typeclass stuff *)
Instance base_lit_dec_eq : EqDecision base_lit.
Proof. solve_decision. Defined.
Instance bin_op_dec_eq : EqDecision bin_op.
Proof. solve_decision. Defined.

Fixpoint expr_beq (e : expr) (e' : expr) : bool :=
  let fix expr_list_beq el el' :=
    match el, el' with
    | [], [] => true
    | eh::eq, eh'::eq' => expr_beq eh eh' && expr_list_beq eq eq'
    | _, _ => false
    end
  in
  match e, e' with
  | Var x, Var x' => bool_decide (x = x')
  | Lit l, Lit l' => bool_decide (l = l')
  | Rec f xl e, Rec f' xl' e' =>
    bool_decide (f = f') && bool_decide (xl = xl') && expr_beq e e'
  | BinOp op e1 e2, BinOp op' e1' e2' =>
    bool_decide (op = op') && expr_beq e1 e1' && expr_beq e2 e2'
  | App e el, App e' el' | Case e el, Case e' el' =>
    expr_beq e e' && expr_list_beq el el'
  | Read e, Read e' => expr_beq e e'
  | Write e1 e2, Write e1' e2' =>
    expr_beq e1 e1' && expr_beq e2 e2'
  | CAS e0 e1 e2, CAS e0' e1' e2' =>
    expr_beq e0 e0' && expr_beq e1 e1' && expr_beq e2 e2'
  | Alloc h e, Alloc h' e' => bool_decide (h = h') && expr_beq e e'
  | Fork e, Fork e' => expr_beq e e'
  | Free e1 e2, Free e1' e2' => expr_beq e1 e1' && expr_beq e2 e2'
  | Syscall s e, Syscall s' e' => bool_decide (s = s') && expr_beq e e'
  | GatedCall s el, GatedCall s' el' => bool_decide (s = s') && expr_list_beq el el'
  | GatedRet e, GatedRet e' => expr_beq e e'
  | MkLocation e0 e1 e2, MkLocation e0' e1' e2' =>
    expr_beq e0 e0' && expr_beq e1 e1' && expr_beq e2 e2'
  | GetPriv e, GetPriv e' => expr_beq e e'
  | GetBlock e, GetBlock e' => expr_beq e e'
  | GetOffset e, GetOffset e' => expr_beq e e'
  | _, _ => false
  end.
Lemma expr_beq_correct (e1 e2 : expr) : expr_beq e1 e2 ↔ e1 = e2.
Proof.
  revert e1 e2; fix FIX 1.
    destruct e1 as [| | | |? el1| | | | | |? el1| | |? el1| | | | |],
             e2 as [| | | |? el2| | | | | |? el2| | |? el2| | | | |]; simpl; try done;
  rewrite ?andb_True ?bool_decide_spec ?FIX;
  try (split; intro; [destruct_and?|split_and?]; congruence).
  - match goal with |- context [?F el1 el2] => assert (F el1 el2 ↔ el1 = el2) end.
    { revert el2. induction el1 as [|el1h el1q]; destruct el2; try done.
      specialize (FIX el1h). naive_solver. }
    clear FIX. naive_solver.
  - match goal with |- context [?F el1 el2] => assert (F el1 el2 ↔ el1 = el2) end.
    { revert el2. induction el1 as [|el1h el1q]; destruct el2; try done.
      specialize (FIX el1h). naive_solver. }
    clear FIX. naive_solver.
  - match goal with |- context [?F el1 el2] => assert (F el1 el2 ↔ el1 = el2) end.
    { revert el2. induction el1 as [|el1h el1q]; destruct el2; try done.
      specialize (FIX el1h). naive_solver. }
    clear FIX. naive_solver.
Qed.
Instance expr_dec_eq : EqDecision expr.
Proof.
 refine (λ e1 e2, cast_if (decide (expr_beq e1 e2))); by rewrite -expr_beq_correct.
Defined.
Instance val_dec_eq : EqDecision val.
Proof.
 refine (λ v1 v2, cast_if (decide (of_val v1 = of_val v2))); abstract naive_solver.
Defined.

Instance expr_inhabited : Inhabited expr := populate (Lit LitPoison).
Instance val_inhabited : Inhabited val := populate (LitV LitPoison).
Instance tstate_inhabited : Inhabited tstate := populate (mkTState (Lit LitPoison) High).
Instance tstate_val_inhabited : Inhabited tstate_val := populate (mkTStateVal (LitV LitPoison) High).
Instance state_inhabited : Inhabited state := populate (empty_state (λ _, ∅)).

Canonical Structure stateO := leibnizO state.
Canonical Structure valO := leibnizO val.
Canonical Structure exprO := leibnizO expr.
Canonical Structure obsO := leibnizO observation.
Canonical Structure interfaceO := leibnizO interface_map.

(** Language *)
Lemma sandbox_lang_mixin `{!syscallG} : EctxiLanguageMixin tstate_of_val tstate_to_val tstate_fill_item head_step.
Proof.
  split; apply _ || eauto using tstate_to_of_val, tstate_of_to_val,
    tstate_val_stuck, tstate_fill_item_val, tstate_fill_item_no_val_inj, tstate_head_ctx_step_val.
Qed.
Canonical Structure sandbox_ectxi_lang `{!syscallG} := EctxiLanguage sandbox_lang_mixin.
Canonical Structure sandbox_ectx_lang `{!syscallG} := EctxLanguageOfEctxi sandbox_ectxi_lang.
Canonical Structure sandbox_lang `{!syscallG} := LanguageOfEctx sandbox_ectx_lang.

(* Define some derived forms *)
Notation Lam xl e := (Rec BAnon xl e) (only parsing).
Notation Let x e1 e2 := (App (Lam [x] e2) [e1]) (only parsing).
Notation Seq e1 e2 := (Let BAnon e1 e2) (only parsing).
Notation LamV xl e := (RecV BAnon xl e) (only parsing).
Notation LetCtx x e2 := (AppRCtx (LamV [x] e2) [] []).
Notation SeqCtx e2 := (LetCtx BAnon e2).
Notation Skip := (Seq (Lit LitPoison) (Lit LitPoison)).
Coercion lit_of_bool : bool >-> base_lit.
Notation If e0 e1 e2 := (Case e0 (@cons expr e2 (@cons expr e1 (@nil expr)))) (only parsing).
