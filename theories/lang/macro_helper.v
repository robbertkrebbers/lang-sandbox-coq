From stdpp Require Export pretty.
From sandbox.lang Require Import lang notation proofmode tactics substitution type.

Local Hint Resolve is_closed_of_val.

Section helper.
  Context `{sandboxG Σ}.

  Fixpoint gen_binders (n : nat) : list string :=
    match n with
    | 0%nat => []
    | S n' => (pretty n') :: gen_binders n'
    end.

  Lemma gen_binders_length n :
    length (gen_binders n) = n.
  Proof. by elim: n => //= n ->. Qed.
  Local Hint Resolve gen_binders_length.

  Lemma gen_binders_length_vec n (vl : vec val n):
    length (gen_binders n) = length vl.
  Proof. by rewrite gen_binders_length vec_to_list_length. Qed.
  Local Hint Resolve gen_binders_length_vec.

  Global Instance gen_binders_in n s: SetUnfoldElemOf
    s (gen_binders n) (∃ m, s = pretty m ∧ (m < n)%nat).
  Proof. constructor.
    elim: n s => /=; set_unfold; first by split => // [[?]]; lia.
    move => n IH s. rewrite IH.
    split; first by naive_solver.
    move => [m [-> Hm]].
    have [->|?] := decide (n = m); first by left.
    right. exists m. split => //. lia.
  Qed.

  Lemma gen_binders_NoDup n:
    NoDup (gen_binders n).
  Proof.
    elim: n => /=. by constructor.
    move => n Hnd. constructor => //.
    set_unfold. move => [? [/pretty_nat_inj->]]. lia.
  Qed.
  Local Hint Resolve gen_binders_NoDup.

  Lemma gen_binders_map_pretty n (xl: vec val n):
    subst_map (gen_binders n) xl !! BNamed (pretty n) = None.
  Proof.
    apply/subst_map_not_in => //. set_unfold.
    move => [? [/pretty_nat_inj ->]]. lia.
  Qed.

  Lemma gen_binders_map_v n (xl: vec val n):
    subst_map (gen_binders n) xl !! BNamed "v" = None.
  Proof.
    apply/subst_map_not_in => //. set_unfold.
    move => [? [??]]. by eapply pretty_nat_neq.
  Qed.

  Definition gen_calls (vars: list string) (fns : list val) (rest : expr)
    : expr := foldr (λ b r, (let: BNamed b.1 :=
                    App (of_val b.2) [ Var $ b.1 ] in r)%E)
                    rest (zip vars fns).

  Lemma gen_calls_surface_expr vars fns rest:
    surface_expr rest → (∀ v, v ∈ fns → surface_val v) → surface_expr (gen_calls vars fns rest).
  Proof.
    move => ?. elim: fns vars => [|???] [] //= _ ??.
    by split_and? => //; eauto with set_solver.
  Qed.

 Lemma gen_calls_closed vars fns l rest :
    Closed (vars ++ l) rest →
    Closed (vars ++ l) (gen_calls vars fns rest).
  Proof.
    have : (vars ⊆ vars) by [].
    move: {1 5}(vars) => vs Hvs Hc.
    elim: fns vs Hvs. by case.
    move => f fs IH [] // v vs ?. rewrite /Closed /= -/(gen_calls _ _ _).
    split_and? => //; eauto with set_solver.
    apply: is_closed_weaken; first by apply: IH; set_solver. set_solver.
  Qed.


  Local Hint Resolve gen_binders_length.
  Local Hint Resolve gen_binders_length_vec.
  Local Hint Resolve gen_binders_NoDup.
  Lemma gen_calls_type {n} vars fns rest atys rtys (args : vec _ n) ty (γ : env) p :
    length vars = n →
    length fns = n →
    NoDup vars →
    (big_sepL3 (λ _ v a r, v ◁ fn High [# a] r) fns atys rtys)  -∗
    [∗] zip_with ty_own atys args -∗
        (∀ (xl : vec _ n),
            [∗] (zip_with ty_own rtys xl) -∗
            typed_expr2 High p ty True
             ((subst_map vars xl ∪ γ) rest)) -∗
     typed_expr2 High p ty True
       ((subst_map vars args ∪ γ) (gen_calls vars fns rest)) .
  Proof.
    elim: n vars fns atys rtys args γ.
    - move => []//[]//atys rtys args γ _ _ _.
      iIntros "Hv _ Hrest". iApply "Hrest". by destruct rtys, atys.
    - move=>n IH [|v vs]//[|f fs]//atys rtys args γ[?][?]/NoDup_cons[??]. iIntros "Hv".
      iDestruct (big_sepL3_cons_inv_l with "Hv") as (aty atys' rty rtys' ??) "[Hf ?]". subst.
      inv_vec args => arg args /=.
      iIntros "[Harg Hargs] Hrest".
      rewrite /gen_calls/= -/(gen_calls _ _ _).
      rewrite subst_env_let (subst_env_expr (App _ _)) (subst_env_closed (of_val _)) /= -!insert_union_l  subst_subst_env /= /subst /= delete_insert_delete.
      case_bool_decide; last done. rewrite (subst_env_closed (of_val _)).
      iApply (type_let with "[Hf Harg]") => //. {
        iApply (type_fn_call with "Hf") => //.  by iFrame.
      }
      iIntros (r) "Hr _".
      rewrite -subst_subst_env_l; last by apply lookup_delete.
      rewrite insert_delete insert_union_r; last by apply/subst_map_not_in.
      iApply (IH with "[$] Hargs") => //.
      iIntros (xl) "Hxl".
      iSpecialize ("Hrest" $! (vcons r xl) with "[Hr Hxl]") => /=.
      by iFrame.
      rewrite -insert_union_r ?insert_union_l //.
      by apply/subst_map_not_in.
  Qed.

  Definition gen_seq : list expr → expr → expr :=
    flip $ foldr (λ a b, (a;; b)%E).

  Lemma gen_seq_closed X es e:
    Closed X e → (∀ el, el ∈ es → Closed X el) → Closed X (gen_seq es e).
  Proof.
    move => ?. elim: es => //=?? IH ?. rewrite /Closed /=.
    split_and? => //; set_solver.
  Qed.

  Lemma gen_seq_subst_env es e (γ : env):
    γ (gen_seq es e) = gen_seq (γ <$> es) (γ e).
  Proof. elim: es => //; csimpl => ? ? <-. by apply subst_env_seq. Qed.

  Lemma gen_seq_surface_expr e el:
    surface_expr e → (∀ e', e' ∈ el → surface_expr e') → surface_expr (gen_seq el e).
  Proof.
    move => ?. elim: el => //= ?? IH Hin.
    by split_and? => //; eauto with set_solver.
  Qed.

  Lemma type_gen_seq P es e p ty:
    P 0%nat -∗
    ([∗ list] i↦x ∈ es, P i -∗ typed_expr p any (P (S i)) x) -∗
    (P (length es) -∗ typed_expr p ty True e) -∗
    typed_expr p ty True (gen_seq es e).
  Proof.
    iIntros "H0". iInduction es as [|x es] "IH" forall (P) => /=.
    { iIntros "_ He". by iApply "He". }
    iIntros "[Hx Hes] He".
    iApply (type_seq with "[H0 Hx]"). by iApply "Hx". iIntros "H1".
    iApply ("IH" $! (P ∘ S) with "H1 Hes"). by iApply "He".
  Qed.

End helper.
