From iris.program_logic Require Export weakestpre.
From iris.program_logic Require Import ectx_lifting.
From sandbox.lang Require Export lang heap.
From sandbox.lang Require Import tactics notation.
From iris.proofmode Require Import tactics.
Set Default Proof Using "Type".
Import uPred.

Class sandboxG Σ := SandboxG {
  sandboxG_invG : invG Σ;
  sandboxG_gen_heapG :> heapG Σ
}.

Instance sandboxG_irisG `{!sandboxG Σ} : irisG sandbox_lang Σ := {
  iris_invG := sandboxG_invG;
  state_interp σ κs _ := heap_ctx σ;
  fork_post _ := True%I;
}.
Global Opaque iris_invG.

Ltac inv_lit :=
  repeat match goal with
  | H : lit_eq _ ?x ?y |- _ => inversion H; clear H; simplify_map_eq/=
  | H : lit_neq ?x ?y |- _ => inversion H; clear H; simplify_map_eq/=
  end.
Ltac inv_bin_op_eval :=
  repeat match goal with
  | H : bin_op_eval _ ?c _ _ _ |- _ => is_constructor c; inversion H; clear H; simplify_eq/=
  end.

Local Hint Resolve to_of_val.

Lemma tac_solve_sub_redexes_are_values_app `{!syscallG} e el p:
  AsVal e → TCForall AsVal el →
  sub_redexes_are_values (e el at p)%E.
Proof.
  move => [? <-] /TCForall_Forall Hel.
  apply ectxi_language_sub_redexes_are_values.
  move => Ki [??] Heq.
  destruct Ki; simpl in * => //;
  move: Heq => [*]; subst; apply to_expr_val_is_Some => /=; eauto.
  elim: vl Hel => /=; first by move /Forall_cons => [[? <-] _]; eauto.
  by move => ? ? IH /Forall_cons [_].
Qed.

Lemma tac_solve_sub_redexes_are_values_privcall `{!syscallG} s el p:
  TCForall AsVal el →
  sub_redexes_are_values (GatedCall s el at p)%E.
Proof.
  move => /TCForall_Forall Hel.
  apply ectxi_language_sub_redexes_are_values.
  move => Ki [??] Heq.
  destruct Ki; simpl in * => //;
  move: Heq => [*]; subst; apply to_expr_val_is_Some => /=; eauto.
  elim: vl Hel => /=; first by move /Forall_cons => [[? <-] _]; eauto.
  by move => ? ? IH /Forall_cons [_].
Qed.

Tactic Notation "solve_sub_redexes_are_values" :=
  let Ki := fresh "Ki" in let Heq := fresh "Heq" in
  try by [apply tac_solve_sub_redexes_are_values_app; apply _];
  try by [apply tac_solve_sub_redexes_are_values_privcall; apply _];
  apply ectxi_language_sub_redexes_are_values;
  move => Ki [??] Heq;
  destruct Ki; simpl in * => //;
  move: Heq => [*]; subst;
  apply to_expr_val_is_Some => /=; try case_decide => //;
   by eauto using to_of_val.
Tactic Notation "solve_stuck" :=
  iApply wp_lift_pure_head_stuck; [
    cbn => //; by case_decide
  | by solve_sub_redexes_are_values
  | move => ?; split; [by cbn => //; case_decide|] => // ?*?; inv_head_step; inv_bin_op_eval; by eauto ].


Local Hint Extern 0 (atomic _) => solve_atomic.
Local Hint Extern 0 (sub_redexes_are_values _) => solve_sub_redexes_are_values.
Local Hint Extern 0 (priv_leb _ _) => solve_priv_leb.
Local Hint Extern 0 (@sqsubseteq priv priv_le _ _) => solve_priv_leb.
Local Hint Extern 0 (head_reducible _ _) => eexists _, _, _, _; simpl.

Local Hint Constructors head_step bin_op_eval lit_neq lit_eq.
Local Hint Resolve alloc_fresh.

Class AsRec (e : expr) (f : binder) (xl : list binder) (erec : expr) :=
  as_rec : e = Rec f xl erec.
Instance AsRec_rec f xl e : AsRec (Rec f xl e) f xl e := eq_refl.
Instance AsRec_rec_locked_val v f xl e :
  AsRec (of_val v) f xl e → AsRec (of_val (locked v)) f xl e.
Proof. by unlock. Qed.

Class DoSubst (x : binder) (es : expr) (e er : expr) :=
  do_subst : subst' x es e = er.
Hint Extern 0 (DoSubst _ _ _ _) =>
  rewrite /DoSubst; simpl_subst; reflexivity : typeclass_instances.

Class DoSubstL (xl : list binder) (esl : list expr) (e er : expr) :=
  do_subst_l : subst_l xl esl e = Some er.
Instance do_subst_l_nil e : DoSubstL [] [] e e.
Proof. done. Qed.
Instance do_subst_l_cons x xl es esl e er er' :
  DoSubstL xl esl e er' → DoSubst x es er' er →
  DoSubstL (x :: xl) (es :: esl) e er.
Proof. rewrite /DoSubstL /DoSubst /= => -> <- //. Qed.
Instance do_subst_vec xl (vsl : vec val (length xl)) e :
  DoSubstL xl (of_val <$> vec_to_list vsl) e (subst_v xl vsl e).
Proof. by rewrite /DoSubstL subst_v_eq. Qed.

Class AsRecV (v : val) (f : binder) (xl : list binder) (erec : expr) (HC : Closed _ _) :=
  as_recv : v = @RecV f xl erec HC.
(* for mysterious reasons ssreflect apply workds while normal apply changes the expression *)
Hint Extern 0 (AsRecV _ _ _ _ _) => (apply: eq_refl) : typeclass_instances.


Section lifting.
Context `{sandboxG Σ}.
Implicit Types P Q : iProp Σ.
Implicit Types e : expr.
Implicit Types ef : option expr.

Lemma sandbox_head_step_maybestuck {E Φ} (e1 : tstate) :
  tstate_to_val e1 = None → sub_redexes_are_values e1 →
  (∀ σ1 κ, heap_ctx σ1 ={E,∅}=∗
    ▷ ∀ (e2 : tstate) σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ ={∅,E}=∗
      heap_ctx σ2 ∗
      WP e2 @ E ?{{ Φ }} ∗
      [∗ list] ef ∈ efs, WP ef @ ⊤ ?{{ fork_post }})
  ⊢ WP e1 @ E ?{{ Φ }}.
Proof.
  iIntros (??) "Hn". iApply wp_lift_head_step_maybestuck => //=. iIntros (????) "Hctx".
  iMod ("Hn" with "Hctx"). iModIntro. iSplitR; first by iRight.
  done.
Qed.

Lemma sandbox_atomic_head_step_maybestuck {E Φ} (e1 : tstate):
  Atomic StronglyAtomic e1 →
  tstate_to_val e1 = None → sub_redexes_are_values e1 →
  ▷ (∀ (e2 : tstate) σ1 σ2 κ efs,
   ⌜head_step e1 σ1 κ e2 σ2 efs⌝ -∗ heap_ctx σ1 ={E}=∗ heap_ctx σ2 ∗
     (from_option Φ False (tstate_to_val e2)) ∗
     [∗ list] ef ∈ efs, WP ef @ ⊤ ?{{ _, True }})
           -∗ WP e1 @ E ?{{ Φ }}.
Proof.
  iIntros (Ha ? ?) "Hn".
  iApply wp_lift_atomic_head_step_maybestuck => //=. iIntros (????) "Hctx".
  iModIntro. iSplitR; first by iRight.
  iNext. iIntros (e ?? Hstep).
  by iMod ("Hn" $! _ _ _ _ _ Hstep with "Hctx") as "[$ $]".
Qed.

(** Base axioms for core primitives of the language: Stateless reductions *)
Lemma wp_fork E e s p :
  {{{ ▷ WP e at p @ s; ⊤ {{ _, True }} }}} Fork e at p @ s; E {{{ RET (LitV LitPoison at p); True }}}.
Proof.
  iIntros (?) "?HΦ". iApply wp_lift_atomic_head_step; [done|].
  iIntros (σ1 κ κs n) "Hσ !>"; iSplit; first by eauto.
  iNext; iIntros (v2 σ2 efs Hstep); inv_head_step. iFrame.
  iModIntro. by iApply "HΦ".
Qed.

(** Pure reductions *)
Class PureExec (Φ : Prop) (n : nat) (e1 : expr) (p1 : priv) (e2 : expr) (p2 : priv) :=
  pure_exec : language.PureExec Φ n (e1 at p1) (e2 at p2).

Local Ltac solve_exec_safe :=
  intros; destruct_and?; subst; do 3 eexists; econstructor; simpl; eauto with omega.
Local Ltac solve_exec_puredet :=
  simpl; intros; destruct_and?; inv_head_step; inv_bin_op_eval; inv_lit; done.
Local Ltac solve_pure_exec :=
  intros ?; apply nsteps_once, pure_head_step_pure_step;
    constructor; [solve_exec_safe | solve_exec_puredet].

Global Instance pure_rec e f xl erec erec' el p :
  AsRec e f xl erec →
  TCForall AsVal el →
  Closed (f :b: xl +b+ []) erec →
  DoSubstL (f :: xl) (e :: el) erec erec' →
  PureExec True 1 (App e el) p (erec') p.
Proof.
  rewrite /AsRec /DoSubstL=> -> /TCForall_Forall Hel ??. solve_pure_exec.
  eapply Forall_impl; [exact Hel|]. intros e' [v <-]. rewrite to_of_val; eauto.
Qed.

Global Instance pure_le n1 n2 p:
  PureExec True 1 (BinOp LeOp (Lit (LitInt n1)) (Lit (LitInt n2))) p
                  (Lit (bool_decide (n1 ≤ n2))) p.
Proof. solve_pure_exec. Qed.

Global Instance pure_eq_int n1 n2 p:
  PureExec True 1 (BinOp EqOp (Lit (LitInt n1)) (Lit (LitInt n2))) p (Lit (bool_decide (n1 = n2))) p.
Proof. case_bool_decide; solve_pure_exec. Qed.

Global Instance pure_eq_loc_0_r l p:
  PureExec True 1 (BinOp EqOp (Lit (LitLoc l)) (Lit (LitInt 0))) p (Lit false) p.
Proof. solve_pure_exec. Qed.

Global Instance pure_eq_loc_0_l l p:
  PureExec True 1 (BinOp EqOp (Lit (LitInt 0)) (Lit (LitLoc l))) p (Lit false) p.
Proof. solve_pure_exec. Qed.

Global Instance pure_plus z1 z2 p:
  PureExec True 1 (BinOp PlusOp (Lit $ LitInt z1) (Lit $ LitInt z2)) p (Lit $ LitInt $ z1 + z2) p.
Proof. solve_pure_exec. Qed.

Global Instance pure_minus z1 z2 p:
  PureExec True 1 (BinOp MinusOp (Lit $ LitInt z1) (Lit $ LitInt z2)) p (Lit $ LitInt $ z1 - z2) p.
Proof. solve_pure_exec. Qed.

Global Instance pure_offset l z p :
  PureExec True 1 (BinOp OffsetOp (Lit $ LitLoc l) (Lit $ LitInt z)) p (Lit $ LitLoc $ l +ₗ z) p.
Proof. solve_pure_exec. Qed.

Global Instance pure_case i e el p:
  PureExec (0 ≤ i ∧ el !! (Z.to_nat i) = Some e) 1 (Case (Lit $ LitInt i) el) p e p | 10.
Proof. solve_pure_exec. Qed.

Global Instance pure_if b e1 e2 p:
  PureExec True 1 (If (Lit (lit_of_bool b)) e1 e2) p (if b then (e1)%E else (e2)%E) p | 1.
Proof. destruct b; solve_pure_exec. Qed.

Global Instance pure_priv_ret e v p:
  IntoVal e v →
  PureExec True 1 (GatedRet e) p v (other_priv p).
Proof. move => <-. solve_pure_exec. Qed.

Global Instance pure_mk_location n b o p:
  PureExec (0 ≤ b) 1 (MkLocation (Lit $ LitInt n) (Lit $ LitInt b) (Lit $ LitInt o)) p (Lit $ LitLoc (Z_to_priv n, Z.to_pos b, o)) p.
Proof. solve_pure_exec. Qed.

Global Instance pure_get_priv l p:
  PureExec True 1 (GetPriv (Lit $ LitLoc l)) p (Lit $ LitInt (priv_to_Z l.1.1)) p.
Proof. solve_pure_exec. Qed.

Global Instance pure_get_block l p:
  PureExec True 1 (GetBlock (Lit $ LitLoc l)) p (Lit $ LitInt (Z.pos l.1.2)) p.
Proof. solve_pure_exec. Qed.

Global Instance pure_get_offset l p:
  PureExec True 1 (GetOffset (Lit $ LitLoc l)) p (Lit $ LitInt l.2) p.
Proof. solve_pure_exec. Qed.

(** High Heap *)
Lemma wp_alloc_high E (n : Z):
  {{{ True }}} Alloc High (Lit $ LitInt n) at High @ E ?
  {{{ l (sz: nat), RET LitV $ LitLoc l at High; ⌜n = sz⌝ ∗ †l…sz ∗ l ↦∗ replicate sz (LitV LitPoison) }}}.
Proof.
  iIntros (Φ) "_ HΦ".
  iApply sandbox_atomic_head_step_maybestuck => //.
  iNext. iIntros (??????) "Hσ"; inv_head_step. rewrite right_id.
  iMod (heap_alloc_high with "Hσ") as "[Hσ Hl]"; [done..|].
  iFrame. iModIntro.
  iApply ("HΦ" $! _ (Z.to_nat n)). iFrame. iPureIntro. rewrite Z2Nat.id; lia.
Qed.

Lemma wp_free_high E (n:Z) l vl s:
  n = length vl →
  {{{ ▷ l ↦∗ vl ∗ ▷ †l…(length vl) }}}
    Free (Lit $ LitInt n) (Lit $ LitLoc l) at High @ s; E
  {{{ RET LitV LitPoison at High; True }}}.
Proof.
  iIntros (? Φ) "[>Hmt >Hf] HΦ".
  iApply wp_lift_atomic_head_step => //=. iIntros (????) "Hσ".
  iMod (heap_free_high _ _ _ n with "Hσ Hmt Hf") as "(% & % & Hσ)" => //.
  iSplitR; auto. do 2 iModIntro. iIntros (????); inv_head_step. rewrite right_id.
  iModIntro. iFrame. by iApply "HΦ".
Qed.

Lemma wp_read_high E l q v s p:
  {{{ ▷ l ↦{q} v }}} Read (Lit $ LitLoc l) at p @ s; E
  {{{ RET v at p; l ↦{q} v }}}.
Proof.
  iIntros (?) ">Hv HΦ".
  iApply wp_lift_atomic_head_step => //=. iIntros (????) "Hσ".
  iDestruct (heap_read_high with "Hσ Hv") as %?.
  iSplitR; eauto. do 2 iModIntro. iIntros (????); inv_head_step. rewrite right_id.
  rewrite (to_expr_val_inv v v) //=. iFrame.
  iModIntro. by iApply "HΦ".
Qed.

Lemma wp_write_high E l e v v' s:
  IntoVal e v → surface_val v →
  {{{ ▷ l ↦ v' }}}
    Write (Lit $ LitLoc l) e at High @ s; E
  {{{ RET LitV LitPoison at High; l ↦ v }}}.
Proof.
  iIntros (<- ? Φ) ">Hv HΦ".
  iApply wp_lift_atomic_head_step => //=. iIntros (????) "Hσ".
  iDestruct (heap_read_high with "Hσ Hv") as %?.
  iSplitR; eauto. do 2 iModIntro. iIntros (????); inv_head_step. rewrite right_id.
  iMod (heap_write_high _ _ _  v with "Hσ Hv") as "[Hctx Hv]" => //.
  iModIntro. iFrame. by iApply "HΦ".
Qed.

Lemma wp_cas_int_fail_high E l q z1 e2 lit2 zl s:
  IntoVal e2 (LitV lit2) → z1 ≠ zl →
  {{{ ▷ l ↦{q} LitV (LitInt zl) }}}
    CAS (Lit $ LitLoc l) (Lit $ LitInt z1) e2 at High @ s; E
  {{{ RET LitV $ LitInt 0 at High; l ↦{q} LitV (LitInt zl) }}}.
Proof.
  iIntros (<- ? Φ) ">Hv HΦ".
  iApply wp_lift_atomic_head_step => //=. iIntros (????) "Hσ".
  iDestruct (heap_read_high with "Hσ Hv") as %?.
  iSplitR; eauto. do 2 iModIntro. iIntros (????); inv_head_step; inv_lit. rewrite right_id.
  iModIntro. iFrame. by iApply "HΦ".
Qed.

Lemma wp_cas_suc_high E l lit1 e2 lit2 s:
  IntoVal e2 (LitV lit2) → lit1 ≠ LitPoison →
  {{{ ▷ l ↦ LitV lit1 }}}
    CAS (Lit $ LitLoc l) (Lit lit1) e2 at High @ s; E
  {{{ RET LitV (LitInt 1) at High; l ↦ LitV lit2 }}}.
Proof.
  iIntros (<- ? Φ) ">Hv HΦ".
  iApply wp_lift_atomic_head_step => //=. iIntros (????) "Hσ".
  iDestruct (heap_read_high with "Hσ Hv") as %?.
  iSplitR; first by destruct lit1; eauto.
  do 2 iModIntro. iIntros (????); inv_head_step; [inv_lit|]. rewrite right_id.
  iMod (heap_write_high with "Hσ Hv") as "[Hσ Hv]"; [|iFrame] => //.
  iModIntro. by iApply "HΦ".
Qed.

Lemma wp_cas_int_suc_high E l z1 e2 lit2 s:
  IntoVal e2 (LitV lit2) →
  {{{ ▷ l ↦ LitV (LitInt z1) }}}
    CAS (Lit $ LitLoc l) (Lit $ LitInt z1) e2 at High @ s; E
  {{{ RET LitV (LitInt 1) at High; l ↦ LitV lit2 }}}.
Proof. intros ?. by apply wp_cas_suc_high. Qed.

Lemma wp_cas_loc_suc_high E l l1 e2 lit2 s:
  IntoVal e2 (LitV lit2) →
  {{{ ▷ l ↦ LitV (LitLoc l1) }}}
    CAS (Lit $ LitLoc l) (Lit $ LitLoc l1) e2 at High @ s; E
  {{{ RET LitV (LitInt 1) at High; l ↦ LitV lit2 }}}.
Proof. intros ?. by apply wp_cas_suc_high. Qed.

Lemma wp_cas_loc_fail_high E l q q' q1 l1 v1' e2 lit2 l' vl' s:
  IntoVal e2 (LitV lit2) → l1 ≠ l' →
  {{{ ▷ l ↦{q} LitV (LitLoc l') ∗ ▷ l' ↦{q'} vl' ∗ ▷ l1 ↦{q1} v1' }}}
    CAS (Lit $ LitLoc l) (Lit $ LitLoc l1) e2 at High @ s; E
  {{{ RET LitV (LitInt 0) at High;
      l ↦{q} LitV (LitLoc l') ∗ l' ↦{q'} vl' ∗ l1 ↦{q1} v1' }}}.
Proof.
  iIntros (<- ? Φ) "[>Hl [>Hl' >Hl1]] HΦ".
  iApply wp_lift_atomic_head_step => //=. iIntros (????) "Hσ".
  iDestruct (heap_read_high with "Hσ Hl") as %?.
  iDestruct (heap_read_high with "Hσ Hl'") as %?.
  iDestruct (heap_read_high with "Hσ Hl1") as %?.
  iSplitR; eauto. do 2 iModIntro. iIntros (????); inv_head_step; inv_lit. rewrite right_id.
  iModIntro. iFrame. iApply "HΦ". by iFrame.
Qed.

Lemma wp_cas_loc_nondet_high E l l1 e2 l2 ll s:
  IntoVal e2 (LitV $ LitLoc l2) →
  {{{ ▷ l ↦ LitV (LitLoc ll) }}}
    CAS (Lit $ LitLoc l) (Lit $ LitLoc l1) e2 at High @ s; E
  {{{ b, RET LitV (lit_of_bool b) at High;
      if b is true then l ↦ LitV (LitLoc l2)
      else ⌜l1 ≠ ll⌝ ∗ l ↦ LitV (LitLoc ll) }}}.
Proof.
  iIntros (<- Φ) ">Hv HΦ".
  iApply wp_lift_atomic_head_step => //=. iIntros (????) "Hσ".
  iDestruct (heap_read_high with "Hσ Hv") as %?.
  iSplitR; first (destruct (decide (ll = l1)) as [->|]; by eauto).
  do 2 iModIntro. iIntros (????); inv_head_step; rewrite right_id.
  - inv_lit. iModIntro. iFrame. iApply "HΦ". by iFrame.
  - iMod (heap_write_high with "Hσ Hv") as "[Hσ Hv]"; [|iFrame] => //.
    iModIntro. by iApply "HΦ".
Qed.

Lemma wp_eq_loc E (l1 : loc) (l2: loc) q1 q2 v1 v2 P Φ s p:
  (P -∗ ▷ l1 ↦{q1} v1) →
  (P -∗ ▷ l2 ↦{q2} v2) →
  (P -∗ ▷ Φ (LitV (bool_decide (l1 = l2)) at p)%V) →
  P -∗ WP BinOp EqOp (Lit (LitLoc l1)) (Lit (LitLoc l2)) at p @ s; E {{ Φ }}.
Proof.
  iIntros (Hl1 Hl2 Hpost) "HP".
  destruct (bool_decide_reflect (l1 = l2)) as [->|].
  - iApply wp_lift_pure_det_head_step_no_fork';
      [done|solve_exec_safe|solve_exec_puredet|].
    iApply wp_value. by iApply Hpost.
  - iApply wp_lift_atomic_head_step_no_fork; subst=>//.
    iIntros (σ1 ???) "Hσ1". iModIntro. inv_head_step.
    iSplitR.
    { iPureIntro. repeat eexists. econstructor. eapply BinOpEqFalse. by auto. }
    (* We need to do a little gymnastics here to apply Hne now and strip away a
       ▷ but also have the ↦s. *)
    iAssert ((▷ ∃ q v, l1 ↦{q} v) ∧ (▷ ∃ q v, l2 ↦{q} v) ∧ ▷ Φ (LitV false at p)%V)%I with "[HP]" as "HP".
    { iSplit; last iSplit.
      + iExists _, _. by iApply Hl1.
      + iExists _, _. by iApply Hl2.
      + by iApply Hpost. }
    clear Hl1 Hl2. iNext. iIntros (e2 σ2 efs Hs) "!>".
    inv_head_step. iSplitR=>//. inv_bin_op_eval; inv_lit.
    + iExFalso. iDestruct "HP" as "[Hl1 _]".
      iDestruct "Hl1" as (??) "Hl1".
      iDestruct (heap_read_high σ2 with "Hσ1 Hl1") as %?; simplify_eq.
    + iExFalso. iDestruct "HP" as "[_ [Hl2 _]]".
      iDestruct "Hl2" as (??) "Hl2".
      iDestruct (heap_read_high σ2 with "Hσ1 Hl2") as %?; simplify_eq.
    + iDestruct "HP" as "[_ [_ $]]". done.
Qed.

(** Low Heap *)
Lemma wp_alloc_low E (n : Z) p:
  {{{ True }}} Alloc Low (Lit $ LitInt n) at p @ E ?
  {{{ l, RET LitV $ LitLoc l at p; ⌜low l⌝ }}}.
Proof.
  iIntros (Φ) "_ HΦ".
  iApply sandbox_atomic_head_step_maybestuck => //.
  iNext. iIntros (??????) "Hσ"; inv_head_step. rewrite right_id.
  iMod (heap_alloc_low with "Hσ") as "Hσ"; [done..|].
  iFrame. iModIntro. by iApply "HΦ".
Qed.

(* the low l is needed because p might be High *)
Lemma wp_free_low E (n:Z) l p:
  low l →
  {{{ True }}}
    Free (Lit $ LitInt n) (Lit $ LitLoc l) at p @ E ?
  {{{ RET LitV LitPoison at p; True }}}.
Proof.
  iIntros (? Φ) "_ HΦ".
  iApply sandbox_atomic_head_step_maybestuck => //.
  iNext. iIntros (??????) "Hσ"; inv_head_step. rewrite right_id.
  iMod (heap_free_low with "Hσ") => //. iFrame.
  by iApply "HΦ".
Qed.

Lemma wp_read_low E l p:
  {{{ True }}} Read (Lit $ LitLoc l) at p @ E ?
  {{{ v, RET v at p; ⌜surface_val v⌝ }}}.
Proof.
  iIntros (Φ) "_ HΦ".
  iApply sandbox_atomic_head_step_maybestuck => //.
  iNext. iIntros (??????) "Hσ"; inv_head_step. rewrite right_id.
  rewrite (to_expr_val_inv v v) //=.
  iDestruct (heap_read_surface with "Hσ []") as %? => //.
  iFrame. iModIntro. by iApply "HΦ".
Qed.

Lemma wp_write_low E l e v p:
  IntoVal e v → low l → surface_val v →
  {{{ True }}} Write (Lit $ LitLoc l) e at p @ E ?
  {{{ RET LitV LitPoison at p; True }}}.
Proof.
  iIntros (<- ?? Φ) "_ HΦ".
  iApply sandbox_atomic_head_step_maybestuck => //.
  iNext. iIntros (??????) "Hσ"; inv_head_step. rewrite right_id.
  iMod (heap_write_low _ l v with "Hσ") as "$" => //.
  iModIntro. by iApply "HΦ".
Qed.

(** Proof rules for working on the n-ary argument list. *)
Lemma wp_app_ind E f (el : list expr) (Ql : vec (val → iProp Σ) (length el)) vs Φ s p:
  AsVal f →
  ([∗ list] eQ ∈ zip el Ql, WP eQ.1 at p @ s; E {{ v, eQ.2 v.(tstatev_val) ∗ ⌜v.(tstatev_priv) = p⌝  }}) -∗
    (∀ vl : vec val (length el), ([∗ list] vQ ∈ zip vl Ql, vQ.2 $ vQ.1) -∗
                    WP App f (of_val <$> vs ++ vl) at p @ s; E {{ Φ }}) -∗
    WP App f ((of_val <$> vs) ++ el) at p @ s; E {{ Φ }}.
Proof.
  intros [vf <-]. revert vs Ql.
  induction el as [|e el IH]=>/= vs Ql; inv_vec Ql; simpl.
  - iIntros "_ H". iSpecialize ("H" $! [#]). rewrite !app_nil_r /=. by iApply "H".
  - iIntros (Q Ql) "[He Hl] HΦ".
    change (App (of_val vf) ((of_val <$> vs) ++ e :: el) at p)%E with (tstate_fill_item (AppRCtx vf vs el) (e at p)).
    iApply wp_bind. iApply (wp_wand with "He"). iIntros (v) "/= [HQ ->]".
    rewrite cons_middle (assoc app) -(fmap_app _ _ [tstatev_val v]).
    iApply (IH _ _ with "Hl"). iIntros "* Hvl". rewrite -assoc.
    iApply ("HΦ" $! (v.(tstatev_val):::vl)). iFrame.
Qed.

Lemma wp_app_vec E f el (Ql : vec (val → iProp Σ) (length el)) Φ s p:
  AsVal f →
  ([∗ list] eQ ∈ zip el Ql, WP eQ.1 at p @ s; E {{ v, eQ.2 v.(tstatev_val) ∗ ⌜v.(tstatev_priv) = p⌝ }}) -∗
    (∀ vl : vec val (length el), ([∗ list] vQ ∈ zip vl Ql, vQ.2 $ vQ.1) -∗
                    WP App f (of_val <$> (vl : list val)) at p@ s; E {{ Φ }}) -∗
    WP App f el at p @ s; E {{ Φ }}.
Proof. iIntros (Hf). by iApply (wp_app_ind _ _ _ _ []). Qed.

Lemma wp_app (Ql : list (val → iProp Σ)) E f el Φ s p:
  length Ql = length el → AsVal f →
  ([∗ list] eQ ∈ zip el Ql, WP eQ.1 at p @ s; E {{ v, eQ.2 v.(tstatev_val) ∗ ⌜v.(tstatev_priv) = p⌝ }}) -∗
    (∀ vl : list val, ⌜length vl = length el⌝ -∗
            ([∗ list] k ↦ vQ ∈ zip vl Ql, vQ.2 $ vQ.1) -∗
             WP App f (of_val <$> (vl : list val)) at p @ s; E {{ Φ }}) -∗
    WP App f el at p@ s; E {{ Φ }}.
Proof.
  iIntros (Hlen Hf) "Hel HΦ". rewrite -(vec_to_list_of_list Ql).
  generalize (list_to_vec Ql). rewrite Hlen. clear Hlen Ql=>Ql.
  iApply (wp_app_vec with "Hel"). iIntros (vl) "Hvl".
  iApply ("HΦ" with "[%] Hvl"). by rewrite vec_to_list_length.
Qed.

Lemma app_argument_length_stuck f xl e vl Φ `{Closed (f :b: xl +b+ []) e} p :
  (⌜length xl = length vl⌝ -∗ WP (RecV f xl e) (of_val <$> vl) at p ?{{ Φ }})
    -∗ WP (RecV f xl e) (of_val <$> vl) at p ?{{ Φ }}.
Proof.
  iIntros "HWp".
  have [Hxl|?] := decide (length xl = length vl); first by iApply "HWp".
  iApply wp_lift_pure_head_stuck => //.
  move => ?; split => // ?*.
  move Happ: (App _ _ at _)%E => app Hs.
  generalize dependent xl.
  move: Hs => [] //=?????????/fmap_Some[?[/subst_l_Some Hsubst _]] xl _ Hn [*]; subst.
  apply: Hn. by rewrite Hsubst fmap_length.
Qed.

Lemma wp_syscall subSP `{inSP Σ subSP} s v E (P: base_lit → iProp Σ):
  {{{ (∀ r st (i : nat) π, subSP.(spg_prop) (insp_name subSP heap_spg_name) st -∗
       ∃ idxs, ([∗ list] i∈idxs, ∃ o, in_trace i o) ∗
            (⌜i > max_list_Z idxs⌝ -∗ in_trace i (s, v, r) -∗
              full_trace π -∗ ⌜subSP.(spg_axioms) π⌝
           ==∗
        ∃ st', ⌜run_sp_obs subSP (s, v, r) st = Some st'⌝ ∗
                subSP.(spg_prop) (insp_name subSP heap_spg_name) st' ∗
                full_trace π ∗
                  P r))
  }}} Syscall s #v at High @ E ?
  {{{ r, RET #r at High; P r }}}.
Proof.
  iIntros (Φ) "Hstep HΦ".
  iApply sandbox_atomic_head_step_maybestuck => //.
  iNext. iIntros (??????) "Hσ"; inv_head_step. rewrite right_id.
  iMod (heap_syscall with "Hstep Hσ") as "[$ HP]" => //.
  iModIntro. by iApply "HΦ".
Qed.

End lifting.
