From iris.program_logic Require Export weakestpre.
From iris.program_logic Require Import ectx_lifting.
From sandbox.lang Require Export lang lifting proofmode notation substitution.
From sandbox.lang.lib Require Export memcpy.

Definition global_to_loc (name : namespace) := (High, encode name, 0).

Definition global_to_val (n : namespace) : val :=
  #(LitLoc $ global_to_loc n).

Section interface_ctx.
  Context `{!sandboxG Σ}.

  Definition interface_ctx_rec (self : iProp Σ) : iProp Σ :=
    (▷ (∃ (I : interface_map), high_interface I ∗
         [∗map] n↦v∈I,
         (* we cannot use a hoare triple here because it bakes in a later *)
         □ (∀ vs Φ, self -∗ ([∗ list] v ∈ vs, ⌜surface_val v⌝)
             -∗ (∀ v : val, ⌜surface_val v⌝ -∗ Φ (v at High)%V)
             -∗ WP (of_val v) (of_val <$> vs) at High ?{{ v, Φ v }})
                 ))%I.

  Instance interface_ctx_rec_contractive : Contractive interface_ctx_rec.
  Proof. solve_contractive. Qed.

  Definition interface_ctx : iProp Σ := fixpoint interface_ctx_rec.

  Global Instance interface_ctx_persistent : Persistent interface_ctx.
  Proof.
    rewrite /interface_ctx fixpoint_unfold. by apply _.
  Qed.
End interface_ctx.

Record type `{!sandboxG Σ} :=
  { ty_own : val → iProp Σ;
    ty_surface : ∀ v, ty_own v -∗ ⌜surface_val v⌝
  }.
(* Arguments ty_own {_ _} !_ _ : simpl nomatch. *)
Arguments ty_own {_ _} _ _ : simpl never.
Notation "p ◁ ty" := (ty.(ty_own) p) (at level 70).

Class Copy `{sandboxG Σ} (t : type) :=
  copy_persistent v :> Persistent (v ◁ t).

Section any.
  Context `{sandboxG Σ}.
  Program Definition any : type :=
    {| ty_own v := ⌜surface_val v⌝%I |}.
  Next Obligation. done. Qed.

  Global Instance any_copy : Copy any.
  Proof. intros ?. apply _. Qed.

  Lemma any_subtype v ty:
    v ◁ ty -∗ v ◁ any.
  Proof. iIntros "Hv". by iApply ty_surface. Qed.
End any.

Section programs.
  Context `{!sandboxG Σ}.

  (* we cannot use hoare triples because of the persistence modality
     no later modality because of type_val
   *)
  Definition typed_expr2 (pstart : priv) (pend : priv) (ty : type) (T : iProp Σ) (e : expr) : iProp Σ :=
    (interface_ctx -∗ ∀ Φ, (∀ v, v ◁ ty -∗ T -∗ Φ (v at pend)%V) -∗ WP e at pstart ? {{ Φ }})%I.
  Notation typed_expr p ty T e := (typed_expr2 p p ty T e).
  Arguments typed_expr2 _ _ _ _%I _%E.

  Lemma typed_expr_mono ty1 T1 p ty2 T2 e:
    (∀ v, v ◁ ty1 -∗ v ◁ ty2) -∗
    (T1 -∗ T2) -∗
    typed_expr p ty1 T1 e -∗
    typed_expr p ty2 T2 e.
  Proof.
    iIntros "Ht HT He #?" (?) "HΦ". iApply "He" => //.
    iIntros (v) "Hty1 HT1". iApply ("HΦ" with "[Hty1 Ht] [HT1 HT]").
    by iApply "Ht". by iApply "HT".
  Qed.

  (** useful for going back to typing mode *)
  Lemma type_type p1 p2 ty T e:
    typed_expr2 p1 p2 ty T e -∗ typed_expr2 p1 p2 ty T e.
  Proof. done. Qed.

  Lemma type_val ty p e v:
    IntoVal e v → v ◁ ty -∗ typed_expr p ty True e.
  Proof.
    move => <-. iIntros "Hv #?" (Φ) "HΦ". wp_value_head.
    by iApply ("HΦ" with "[Hv]").
  Qed.

  (** Attention: this might get stuck! (But is not a big problem
  because we would probably get stuck later anyways if the
  subexpression is not closed)

     It is a lot of work to always manually carry around a proof, that
   the current expression is closed (especially in generic wrappers).
   Thus the requirement Closed (xb :b: []) e' was dropped. This means
   that the execution gets stuck when this precondition is not
   fulfilled. Another possibility would be to put the proof of
   closedness into typed_expr. *)
  Lemma type_let ty2 T2 p1 p2 p3 ty T xb e e':
    typed_expr2 p1 p2 ty2 T2 e -∗
    (∀ v, v ◁ ty2 -∗ T2 -∗ typed_expr2 p2 p3 ty T (subst' xb v e')) -∗
    typed_expr2 p1 p3 ty T (let: xb := e in e').
  Proof.
    iIntros "He He' #?" (Φ) "HΦ".
    have [?|?] := decide (Closed (xb :b: []) e'); last by wp_bind (Rec _ _ _); solve_stuck.
    wp_apply "He" => //. iIntros (v) "Hv ?".
    wp_let. by iApply ("He'" with "[$] [$] [//]").
  Qed.

  Lemma type_seq ty2 T2 p1 p2 p3 ty T e e':
    typed_expr2 p1 p2 ty2 T2 e -∗
    (T2 -∗ typed_expr2 p2 p3 ty T e') -∗
    typed_expr2 p1 p3 ty T (e;; e').
  Proof.
    iIntros "He He'". iApply (type_let with "He") => /=.
    by iIntros (?) "?".
  Qed.

  Lemma type_fork p1 p2 ty T e:
    typed_expr2 p1 p2 ty T e -∗
    typed_expr p1 any True (Fork e).
  Proof.
    iIntros "He #?" (?) "HΦ".
    wp_apply (wp_fork with "[He]"). {
      iNext. iApply "He" => //. by iIntros (?) "? ?".
    }
    iIntros "_". by iApply "HΦ".
  Qed.

  (* must be a notation for solve_closed to work *)
  Notation stuck_expr := (#1 [ #1 ])%E.
  Lemma type_stuck p1 p2 ty T:
    typed_expr2 p1 p2 ty T stuck_expr.
  Proof. iIntros "?" (?) "?". solve_stuck. Qed.

  Global Instance stuck_expr_closed : Closed [] stuck_expr.
  Proof. done. Qed.

End programs.
Notation typed_expr p ty T e := (typed_expr2 p p ty T e).
Notation stuck_expr := (#1 [ #1 ])%E.

Section const_lit.
  Context `{sandboxG Σ}.

  Program Definition const_lit (l : base_lit) : type :=
    {| ty_own v := match v return _ with
                   | LitV l' => ⌜l = l'⌝
                   | _ => False
                   end%I |}.
  Next Obligation. move => ? [[]|] * //; by iIntros "?". Qed.

  Lemma const_lit_inv v l: v ◁ const_lit l -∗ ⌜v = #l⌝.
  Proof. iIntros "Hc". case v => // ?. iRevert "Hc". iIntros (?). by subst. Qed.
End const_lit.

Section int.
  Context `{!sandboxG Σ}.
  Program Definition int: type :=
    {| ty_own v := match v return _ with
                   | LitV (LitInt z) => True
                   | _ => False
                   end%I |}.
  Next Obligation. move => [[]|] * //; by iIntros "?". Qed.

  Global Instance int_copy : Copy int.
  Proof.
    case; last by apply _. by case; apply _.
  Qed.

  Lemma int_inv v : v ◁ int -∗ ⌜∃ (z : Z), v = #z⌝.
  Proof. iIntros "?". case v => // [[| |?]] //. eauto. Qed.

  Lemma int_to_const_lit v: v ◁ int -∗ ∃ n, v ◁ const_lit (LitInt n).
  Proof.
    iIntros "Hv". iDestruct (int_inv with "Hv") as %[n ->]. by eauto.
  Qed.

  Lemma type_plus p e1 e2 n1 n2 :
    IntoVal e1 n1 → IntoVal e2 n2 →
    n1 ◁ any -∗ n2 ◁ any -∗ typed_expr p int True (e1 + e2).
  Proof. move => <- <-.
    iIntros "Hn1 Hn2 #?" (Φ) "HΦ".
    move: n1 => [|*]; last by solve_stuck.
    move => [|?|n]; [solve_stuck..|].
    move: n2 => [|*]; last by solve_stuck.
    move => [|?|?]; [solve_stuck..|].
    wp_op. by iApply "HΦ".
  Qed.

End int.

Section const_int.
  Context `{sandboxG Σ}.

  Program Definition const_int (n : Z) : type :=
    {| ty_own v := match v return _ with
                   | LitV (LitInt n') => ⌜n = n'⌝
                   | _ => False
                   end%I |}.
  Next Obligation. move => ? [[]|] * //; by iIntros "?". Qed.

  Global Instance const_int_copy n : Copy (const_int n).
  Proof.
    case; last by apply _. by case; apply _.
  Qed.

  Lemma int_to_const_int v: v ◁ int -∗ ∃ n, v ◁ const_int n.
  Proof.
    iIntros "Hv". iDestruct (int_inv with "Hv") as %[n ->]. by eauto.
  Qed.

  Lemma const_int_inv v n: v ◁ const_int n -∗ ⌜v = #n⌝.
  Proof. iIntros "Hc". case v => // [[| |?]] //. iRevert "Hc". iIntros (?). by subst. Qed.

  Lemma type_eq p e1 e2 v1 v2 n1 n2 :
    IntoVal e1 v1 → IntoVal e2 v2 →
    v1 ◁ const_int n1 -∗ v2 ◁ const_int n2 -∗
       typed_expr p (const_int (Z_of_bool $ bool_decide (n1 = n2)))
       True (e1 = e2).
  Proof. move => <- <-.
    iIntros "Hv1 Hv2 #?" (Φ) "HΦ".
    iDestruct (const_int_inv with "Hv1") as %->.
    iDestruct (const_int_inv with "Hv2") as %->.
    wp_op.
    by iApply "HΦ".
  Qed.

  Lemma type_leq p e1 e2 v1 v2 n1 n2 :
    IntoVal e1 v1 → IntoVal e2 v2 →
    v1 ◁ const_int n1 -∗ v2 ◁ const_int n2 -∗
       typed_expr p (const_int (Z_of_bool $ bool_decide (n1 ≤ n2)))
       True (e1 ≤ e2).
  Proof. move => <- <-.
    iIntros "Hv1 Hv2 #?" (Φ) "HΦ".
    iDestruct (const_int_inv with "Hv1") as %->.
    iDestruct (const_int_inv with "Hv2") as %->.
    wp_op.
    by iApply "HΦ".
  Qed.

  Lemma type_const_plus p e1 e2 v1 v2 n1 n2 :
    IntoVal e1 v1 → IntoVal e2 v2 →
    v1 ◁ const_int n1 -∗ v2 ◁ const_int n2 -∗ typed_expr p (const_int (n1 + n2)) True (e1 + e2).
  Proof. move => <- <-.
    iIntros "Hn1 Hn2 #?" (Φ) "HΦ".
    iDestruct (const_int_inv with "Hn1") as %->.
    iDestruct (const_int_inv with "Hn2") as %->.
    wp_op.
    by iApply "HΦ".
  Qed.
End const_int.

Section fn.
  Context `{sandboxG Σ}.

  (* An alternative formulation would be that the value applied to the
  arguments is a typed_expr. This did not work when still using unt,
  but now it could work. tc_opaque because otherwise iNext tries to
  remove the later inside and unfolds the definition *)
  Program Definition fn {n : nat} (p : priv) (tys : vec type n) (retty : type) : type :=
    {| ty_own v := tc_opaque (∃ f xb e H, ⌜v = @RecV f xb e H⌝ ∗ ⌜length xb = n⌝ ∗ ⌜surface_expr e⌝ ∗
                     ▷ ∀ (xl : vec val (length xb)),
                       □ (([∗] zip_with ty_own tys xl) -∗
                          typed_expr p retty True
                          (subst_v (f::xb) ((RecV f xb e):::xl) e)))%I
    |}.
  Next Obligation. move => /= ?????. by iDestruct 1 as (????->??) "?". Qed.

  Lemma type_fn argsb n p (tys : vec type n) retty ef e HC :
    AsRecV ef <> argsb e HC →
    n = length argsb →
    surface_expr e →
    ▷ □ (∀ (xl : vec val (length argsb)),
          ([∗ list] p ∈ zip_with ty_own tys xl, p ) -∗
          typed_expr p retty True (subst_v argsb xl e)) -∗
       ef ◁ fn p tys retty.
  Proof.
    iIntros (->??) "#Hfn"; subst. iExists _, _, _, _.
    iSplit => //. do 2 iSplit => //. iNext.
    iIntros (xl) "!# Htys". by iApply "Hfn".
  Qed.

  Lemma type_fn_call n p (tys : vec type n) xl vl retty e v:
    IntoVal e v →
    length xl = n →
    TCForall2 IntoVal xl vl →
    v ◁ fn p tys retty -∗
    ([∗] zip_with ty_own tys vl) -∗
    typed_expr p retty True (e xl).
  Proof.
    iIntros (<- Hlen Hvals) "He Hxl #?". iIntros(Φ)"HΦ".
    iDestruct "He"as(f xb e' ?? Hlen2 ?) "Hfn".
    move: Hvals => /(into_vals_inv (length xb))[|? [??]]. by subst.
    subst. wp_rec.
    by iApply ("Hfn" with "[$]").
  Qed.

  Global Instance fn_copy {n} p (tys : vec _ n) retty:
    Copy (fn p tys retty).
  Proof. move => ?. unfold fn, tc_opaque. by apply _. Qed.

  Lemma type_rec argsb n p (tys:vec type n) retty ef fb e `{Closed _ e} :
    ef = (rec: fb argsb := e)%V →
    n = length argsb →
    surface_expr e →
    ▷ □ (∀ (f : val) (xl : vec val (length argsb)),
          f ◁ fn p tys retty -∗
          ([∗ list] p ∈ zip_with ty_own tys xl, p ) -∗
          typed_expr p retty True (subst_v (fb :: argsb) (f::: xl) e)) -∗
       ef ◁ fn p tys retty.
  Proof.
    iIntros (???) "#Hfn"; subst.
    iLöb as "IH".
    iExists _, _, _, _. iSplit => //. do 2 iSplit => //. iNext.
    iIntros (xl) "!# Htys". by iApply "Hfn".
  Qed.

End fn.

Section fn_once.
  Context `{sandboxG Σ}.

  (* same as fn but body does not need to be persistent and thus
  fn_once is not persistent *)
  Program Definition fn_once {n : nat} (p : priv) (tys : vec type n) (retty : type) : type :=
    {| ty_own v := tc_opaque (∃ f xb e H, ⌜v = @RecV f xb e H⌝ ∗ ⌜length xb = n⌝ ∗ ⌜surface_expr e⌝ ∗
                     ▷ ∀ (xl : vec val (length xb)),
                      (([∗] zip_with ty_own tys xl) -∗
                          typed_expr p retty True
                          (subst_v (f::xb) ((RecV f xb e):::xl) e)))%I
    |}.
  Next Obligation. move => /= ?????. by iDestruct 1 as (????->??) "?". Qed.

  Lemma type_fn_once argsb n p (tys : vec type n) retty ef e `{Closed _ e} :
    ef = (λ: argsb, e)%V →
    n = length argsb →
    surface_expr e →
    ▷ (∀ (xl : vec val (length argsb)),
          ([∗ list] p ∈ zip_with ty_own tys xl, p ) -∗
          typed_expr p retty True (subst_v argsb xl e)) -∗
       ef ◁ fn_once p tys retty.
  Proof.
    iIntros (???) "Hfn"; subst. iExists _, _, _, _. by eauto.
  Qed.

  Lemma type_call_once n p (tys : vec type n) xl vl retty e v :
    IntoVal e v →
    length xl = n →
    TCForall2 IntoVal xl vl →
    v ◁ fn_once p tys retty -∗
    ([∗] zip_with ty_own tys vl) -∗
    typed_expr p retty True (e xl).
  Proof.
    iIntros (<- Hlen Hvals) "He Hxl #?". iIntros(Φ)"HΦ".
    iDestruct "He"as(f xb e' ?? Hlen2 ?) "Hfn".
    move: Hvals => /(into_vals_inv (length xb))[|? [??]]. by subst.
    subst. wp_rec.
    by iApply ("Hfn" with "[$]").
  Qed.

End fn_once.

Section low_ptr.
  Context `{sandboxG Σ}.

  Program Definition lowptr : type :=
    {| ty_own v := match v return _ with
                     | LitV (LitLoc l) => ⌜low l⌝
                     | _ => False
                   end%I |}.
  Next Obligation. move => [[]|] * //; by iIntros "?". Qed.

  Global Instance lowptr_copy : Copy lowptr.
  Proof. case; last by apply _. by case; apply _. Qed.

  Lemma lowptr_inv v: v ◁ lowptr -∗ ⌜∃ (l : loc), v = #l ∧ low l⌝.
  Proof.
    iIntros "Hv". case v => // [[|?|]] //.
    iRevert "Hv". iIntros (?). eauto.
  Qed.

  Lemma type_new_low (n : Z) p:
    typed_expr p lowptr True%I (Alloc Low #n).
  Proof.
    iIntros "#?" (Φ) "HΦ". wp_apply wp_alloc_low => //.
    iIntros (l ?). by iApply "HΦ".
  Qed.

  Lemma type_delete_low (n : Z) p v:
    v ◁ lowptr -∗
    typed_expr p any True (Free #n v).
  Proof.
    iIntros "Hv #?" (Φ) "HΦ".
    iDestruct (lowptr_inv with "Hv") as %[l [-> ?]].
    wp_apply wp_free_low => //. by iApply "HΦ".
  Qed.

  Lemma type_read_low v n p:
    v ◁ lowptr -∗
    n ◁ int -∗
    typed_expr p any True (!(v +ₗ n)).
  Proof.
    iIntros "Hv Hn #?" (?) "HΦ".
    iDestruct (lowptr_inv with "Hv") as %[l [-> ?]].
    iDestruct (int_inv with "Hn") as %[m ->].
    wp_op. wp_read_low v => ?. wp_value_head. by iApply "HΦ".
  Qed.

  Lemma type_write_low n v p pr:
    v ◁ any -∗
    p ◁ lowptr -∗
    n ◁ int -∗
    typed_expr pr any True ((p +ₗ n) <- v).
  Proof.
    iIntros "Hv Hp Hn #?" (?) "HΦ".
    iDestruct (lowptr_inv with "Hp") as %[l [-> ?]].
    iDestruct (int_inv with "Hn") as %[m ->].
    iDestruct (ty_surface with "Hv") as %?.
    wp_op. wp_write_low. by iApply "HΦ".
  Qed.

End low_ptr.


Section programs2.
  Context `{sandboxG Σ}.

  Lemma type_case ty1 T1 p1 p2 p3 ty T e (es : list expr):
    typed_expr2 p1 p2 ty1 T1 e -∗
    (∀ (i : nat) er v, ⌜es !! i = Some er⌝ -∗
       v ◁ ty1 -∗ v ◁ const_int i -∗ T1 -∗ typed_expr2 p2 p3 ty T er) -∗
    typed_expr2 p1 p3 ty T (case: e of es).
  Proof.
    iIntros "He Hes #?" (?) "HΦ".
    wp_bind e. iApply "He" => //=. iIntros (v) "Hv1 HT".
    move: v => [l|*]; last by solve_stuck.
    move: l => [|?|n]; [solve_stuck.. | ].
    have [?|?] := decide (0 ≤ n); last by solve_stuck.
    have [[? ?]|?] := decide (is_Some (es !! Z.to_nat n)); last by solve_stuck.
    wp_case.
    iApply ("Hes" with "[//] Hv1 [] HT [//]") => //.
    rewrite /ty_own /=. iPureIntro. by apply Z2Nat.id.
  Qed.

  Lemma type_syscall subSP `{inSP Σ subSP} ty T s v l:
    v ◁ const_lit l -∗
    (∀ r st (i : nat) π, subSP.(spg_prop) (insp_name subSP heap_spg_name) st -∗
       ∃ idxs, ([∗ list] i∈idxs, ∃ o, in_trace i o) ∗
            (⌜i > max_list_Z idxs⌝ -∗ in_trace i (s, l, r) -∗
              full_trace π -∗ ⌜subSP.(spg_axioms) π⌝
           ==∗
        ∃ st', ⌜run_sp_obs subSP (s, l, r) st = Some st'⌝ ∗
                subSP.(spg_prop) (insp_name subSP heap_spg_name) st' ∗
                full_trace π ∗
                  #r ◁ ty ∗ T)) -∗
    typed_expr High ty T (Syscall s v).
  Proof.
    iIntros "Hv Hstep #?" (?) "HΦ".
    iDestruct (const_lit_inv with "Hv") as %->.
    iApply (wp_syscall subSP with "[$]") => //.
    iNext. iIntros (r) "[? ?]".
    by iApply ("HΦ" with "[$]").
  Qed.

End programs2.

Section constloc.
  Context `{sandboxG Σ}.

  Program Definition const_loc (l : loc) : type :=
    {| ty_own v := match v return _ with
                   | LitV (LitLoc l') => ⌜l = l'⌝
                   | _ => False
                   end%I |}.
  Next Obligation. move => ? [[]|] * //; by iIntros "?". Qed.

  Global Instance const_loc_copy l : Copy (const_loc l).
  Proof.
    case; last by apply _. by case; apply _.
  Qed.

  Lemma const_loc_inv v l: v ◁ const_loc l -∗ ⌜v = #l⌝.
  Proof. iIntros "Hc". case v => // [[|?|]] //. iRevert "Hc". iIntros (?). by subst. Qed.

  Definition assert_const_loc : val :=
    (λ: ["x"], "x" +ₗ #0;; #☠).

  Lemma type_assert_const_loc p v:
   typed_expr p any (∃ l, v ◁ const_loc l)%I (assert_const_loc [v]%E).
  Proof.
    iIntros "#?" (?) "HΦ". wp_rec. wp_bind (BinOp _ _ _).
    move: v => [l|*]; [|solve_stuck].
    move: l => [|l|?]; [solve_stuck| |solve_stuck].
    wp_op => /=. wp_seq. iApply "HΦ" => //. by iExists _.
  Qed.

End constloc.

Section getpriv.
  Context `{sandboxG Σ}.

  Program Definition getpriv_ty (l : loc) : type :=
    {| ty_own v := v ◁ const_int (priv_to_Z l.1.1) |}.
  Next Obligation. move => /=??. by apply ty_surface. Qed.

  (* this should also work for arbitrary values
     - general thought: each type could take an additional refinement type predicate for easier creation of refinement types
*)
  Lemma type_getpriv v l p:
    v ◁ (const_loc l) -∗
    typed_expr p (getpriv_ty l) True (GetPriv v).
  Proof.
    iIntros "Hv #?" (?) "HΦ".
    iDestruct (const_loc_inv with "Hv") as %->.
    wp_pure (GetPriv _). by iApply "HΦ".
  Qed.
End getpriv.

Section product.
  Context `{sandboxG Σ}.

  Program Definition product (tys : list type) : type :=
    {| ty_own v := match v return _ with
                   | LitV (LitLoc l) =>
                     l ↦∗: (λ vl, ⌜length tys = length vl⌝ ∗
                                [∗] zip_with ty_own tys vl)
                       ∗ († l … (length tys))
                   | _ => False
                   end%I |}.
  Next Obligation. move => ? [[]|] * //; by iIntros "?". Qed.

  Lemma product_inv v tys: v ◁ product tys -∗ ⌜∃ (l : loc), v = #l⌝.
  Proof. iIntros "?". case v => // [[|?|]] //. eauto. Qed.

  Lemma type_new (n : Z):
    let n' := Z.to_nat n in
    typed_expr High (product (replicate n' any)) True%I (Alloc High #n).
  Proof.
    iIntros "#?" (?) "HΦ".
    iApply wp_alloc_high => //. iIntros "!#" (l sz) "[-> [Hl ?]]".
    iApply ("HΦ" with "[-]") => //=.
    rewrite /ty_own /= replicate_length Nat2Z.id. iFrame.
    iExists _. iFrame.
    iSplit; first by auto using replicate_length.
    iInduction sz as [|m] "IH" => //=. by iSplit.
  Qed.

  Lemma type_delete (n : Z) v tys:
    n = length tys →
    v ◁ (product tys) -∗
    typed_expr High any True (Free #n v).
  Proof.
    move => ->. iIntros "Hv #?" (?) "HΦ".
    iDestruct (product_inv with "Hv") as %[l ->].
    iDestruct "Hv" as "[Hown Hf]".
    iDestruct "Hown" as (vl) "(Hl & -> & _)".
    iApply (wp_free_high with "[$Hl Hf] [HΦ]") => //.
    by iApply "HΦ".
  Qed.

  Lemma type_write ty (n : Z) p e v tys:
    IntoVal e v →
    0 ≤ n →
    n < length tys →
    p ◁ (product tys) -∗
    v ◁ ty -∗
    typed_expr High any (p ◁ (product (<[Z.to_nat n:=ty]> tys))) (p +ₗ #n <- e).
  Proof.
    move => <- ??. iIntros "Hp Hv #?" (?) "HΦ".
    iDestruct (product_inv with "Hp") as %[l ->].
    wp_op.
    iDestruct "Hp" as "[Hp Hf]".
    iDestruct "Hp" as (vl) "(Hp & Heq & Htys)". iDestruct "Heq" as %Heq.
    have [|v1 [v2 [Heq2 Hlen]]]:= (@nth_split _ (Z.to_nat n) vl #1); first by rewrite -Heq -(Nat2Z.id (length _)) -Z2Nat.inj_lt //; lia.
    have {Hlen} Hlen : (Z.of_nat (length v1) = n) by rewrite Hlen Z2Nat.id.
    move: Heq. rewrite Heq2. set vm := nth _ _ _ => Heq {Heq2}.
    rewrite heap_mapsto_vec_app heap_mapsto_vec_cons Hlen.
    iDestruct "Hp" as "(? & ? & ?)".
    iDestruct (ty_surface with "Hv") as %?.

    wp_write_high => //.
    iApply ("HΦ" with "[]") => //=.
    iSplitR "Hf"; rewrite insert_length //.
    iExists (v1 ++ v :: v2).
    rewrite heap_mapsto_vec_app heap_mapsto_vec_cons Hlen Heq !app_length !cons_length.
    iFrame. iSplitR => //.
    have ->: ((v1 ++ v :: v2) = <[Z.to_nat n := v]>(v1 ++ vm :: v2)) by rewrite -Hlen Nat2Z.id (plus_n_O (length _)) insert_app_r.
    rewrite -insert_zip_with.
    iApply (big_sepL_insert' with "[$] [Hv]") => //=.
    rewrite zip_with_length Heq -Hlen Nat2Z.id. lia.
  Qed.

  Lemma type_read (n : Z) p ty tys:
    0 ≤ n →
    tys !! Z.to_nat n = Some ty →
    p ◁ (product tys) -∗
    typed_expr High ty (p ◁ (product (<[Z.to_nat n:=any]> tys))) (!(p +ₗ #n)).
  Proof.
    move => ? Hin. iIntros "Hp #?" (?) "HΦ".
    iDestruct (product_inv with "Hp") as %[l ->].
    wp_op.
    iDestruct "Hp" as "[Hp Hf]".
    iDestruct "Hp" as (vl) "(Hp & Heq & Htys)". iDestruct "Heq" as %Heq.
    have [x Hx] : (is_Some (vl !! Z.to_nat n)) by rewrite lookup_lt_is_Some -Heq -lookup_lt_is_Some; econstructor.

    have <- := take_drop_middle _ _ _ Hx. set v1 := take _ _. set v2 := drop _ _.
    have Hlenv : (n = length v1) by rewrite take_length_le ?Z2Nat.id //; have := lookup_lt_Some _ _ _ Hx; lia.
    have <- := take_drop_middle _ _ _ Hin. set t1 := take _ _. set t2 := drop _ _.
    have Hlent : (n = length t1) by rewrite take_length_le ?Z2Nat.id //; have := lookup_lt_Some _ _ _ Hin; lia.

    rewrite heap_mapsto_vec_app heap_mapsto_vec_cons -Hlenv.
    iDestruct "Hp" as "(? & ? & ?)".
    wp_read_high.
    rewrite zip_with_app /= ?big_sepL_app /=; last by rewrite !take_length; lia.
    iDestruct "Htys" as "(? & Hx & ?)".
    iDestruct (ty_surface with "Hx") as %?.
    iApply ("HΦ" with "[$]") => /=.
    iSplitR "Hf"; rewrite insert_length //.
    iExists (v1 ++ x :: v2).
    rewrite heap_mapsto_vec_app heap_mapsto_vec_cons -Hlenv !app_length !cons_length.
    iFrame. iSplitR.
    { iPureIntro. do 2 f_equal; rewrite ?take_length ?drop_length; lia. }
    rewrite insert_app_r_alt Hlent Nat2Z.id // -minus_n_n /= zip_with_app; last by rewrite !take_length; lia.
    rewrite /= ?big_sepL_app /=. by iFrame.
  Qed.

  Lemma type_read_copy (n : Z) p ty tys `{!Copy ty} :
    0 ≤ n →
    tys !! Z.to_nat n = Some ty →
    p ◁ (product tys) -∗
    typed_expr High ty (p ◁ (product tys)) (!(p +ₗ #n)).
  Proof.
    move => ? Hin. iIntros "Hp #?" (?) "HΦ".
    iDestruct (product_inv with "Hp") as %[l ->].
    wp_op.
    iDestruct "Hp" as "[Hp Hf]".
    iDestruct "Hp" as (vl) "(Hp & Heq & Htys)". iDestruct "Heq" as %Heq.
    have [x Hx] : (is_Some (vl !! Z.to_nat n)) by rewrite lookup_lt_is_Some -Heq -lookup_lt_is_Some; econstructor.

    have <- := take_drop_middle _ _ _ Hx. set v1 := take _ _. set v2 := drop _ _.
    have Hlenv : (n = length v1) by rewrite take_length_le ?Z2Nat.id //; have := lookup_lt_Some _ _ _ Hx; lia.
    have <- := take_drop_middle _ _ _ Hin. set t1 := take _ _. set t2 := drop _ _.
    have Hlent : (n = length t1) by rewrite take_length_le ?Z2Nat.id //; have := lookup_lt_Some _ _ _ Hin; lia.

    rewrite heap_mapsto_vec_app heap_mapsto_vec_cons -Hlenv.
    iDestruct "Hp" as "(? & ? & ?)".
    wp_read_high.
    rewrite zip_with_app /= ?big_sepL_app /=; last by rewrite !take_length; lia.
    iDestruct "Htys" as "(? & #Hx & ?)".
    iDestruct (ty_surface with "Hx") as %?.
    iApply ("HΦ" with "[$]") => /=.
    iSplitR "Hf" => //.
    iExists (v1 ++ x :: v2).
    rewrite heap_mapsto_vec_app heap_mapsto_vec_cons -Hlenv !app_length !cons_length.
    iFrame. iSplitR.
    { iPureIntro. do 2 f_equal; rewrite ?take_length ?drop_length; lia. }
    rewrite zip_with_app; last by rewrite !take_length; lia.
    rewrite /= ?big_sepL_app /=. by iFrame.
  Qed.

  Lemma type_memcpy (n : Z) len src dst tys1 tys2 lenv srcv dstv:
    IntoVal len lenv →
    IntoVal src srcv →
    IntoVal dst dstv →
    0 ≤ n →
    n ≤ length tys1 →
    n ≤ length tys2 →
    lenv ◁ const_int n -∗
    srcv ◁ product tys1 -∗
    dstv ◁ product tys2 -∗
    typed_expr High any
    (srcv ◁ (product (replicate (Z.to_nat n) any ++ drop (Z.to_nat n) tys1)) ∗
     dstv ◁ (product (take (Z.to_nat n) tys1 ++ drop (Z.to_nat n) tys2)))
    (memcpy [dst; len; src]).
  Proof.
    move => <- <- <- ???. iIntros "Hlen Hsrc Hdst #?" (Φ) "HΦ".
    iDestruct (const_int_inv with "Hlen") as %->.
    iDestruct (product_inv with "Hsrc") as %[ls ->].
    iDestruct (product_inv with "Hdst") as %[ld ->] => /=.
    iDestruct "Hsrc" as "[Hsrc Hfs]". iDestruct "Hsrc" as (vls) "(Hvls & Hl1 & Hts)".
    iDestruct "Hdst" as "[Hdst Hfd]". iDestruct "Hdst" as (vld) "(Hvld & Hl2 & Htd)".
    iDestruct "Hl1" as %Hl1. iDestruct "Hl2" as %Hl2.
    rewrite -(take_drop (Z.to_nat n) vls) -(take_drop (Z.to_nat n) vld) 2!heap_mapsto_vec_app.
    set vls1 := take _ vls. set vls2 := drop _ vls.
    set vld1 := take _ vld. set vld2 := drop _ vld.
    rewrite 2!zip_with_app_r 2!big_sepL_app.
    iDestruct "Hts" as "[Hts1 Hts2]". iDestruct "Htd" as "[Htd1 Htd2]".
    iDestruct "Hvls" as "[Hvls1 Hvls2]". iDestruct "Hvld" as "[Hvld1 Hvld2]".
    iApply (wp_memcpy with "[$Hvld1 $Hvls1 //]") => //;
      try by rewrite take_length_le ?Z2Nat.id // Nat2Z.inj_le Z2Nat.id // -?Hl1 -?Hl2.
    iIntros "!# [Hvld1 Hvls1]". iApply "HΦ" => //.
    iDestruct (heap_mapsto_vec_surface with "Hvld1") as %Hsurface.
    iSplitL "Hvls1 Hvls2 Hts2 Hfs";
      [ iSplitR "Hfs"; first iExists (vls1 ++ vls2) | iSplitR "Hfd"; first iExists (vls1 ++ vld2) ];
      rewrite app_length ?zip_with_app ?big_sepL_app ?heap_mapsto_vec_app
              ?app_length ?replicate_length ?take_length_le ?drop_length
              ?le_plus_minus_r ?Nat2Z.inj_le ?Z2Nat.id -?Hl1 -?Hl2 // ?Hl1 ?Hl2; iFrame; eauto.
    iSplit => // {vls2 Hl1}. move: Hsurface. rewrite {}/vls1 => Hsurface.
    iInduction (Z.to_nat n) as [|] "IH" forall (vls Hsurface) => //=.
    destruct vls => //=. move: Hsurface => /Forall_cons[??]. iSplitR => //.
    by iApply "IH".
  Qed.
End product.
