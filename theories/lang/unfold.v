From sandbox.lang Require Import lang notation type interface wrapper.

Local Transparent wrap_import wrap_export import_const_int import_int export_lowptr export_int import_any export_product import_product export_any.

Ltac unfold_expr n e t :=
  match n with
  | O => t e
  | S ?n =>
    let e:= eval unfold untrusted_main, wrap_export, wrap_import,write_export, export_fn, export_int, import_fn, export_product, import_product, export_any in e in
        let e := eval cbn -[pretty.pretty] in e in
            unfold_expr n e t
  end.

Ltac solve_unfold n :=
  let n:= eval cbn -[pretty.pretty] in n in match n with
   | @RecV ?f ?xl ?e _ => unfold_expr 10%nat e ltac:(fun e =>
                                                    (* idtac e; *)
          let C := fresh "C" in evar (C : Closed (f :b: xl +b+ []) e); refine (@RecV f xl e C))
  end.

Section logic.
  Context `{!sandboxG Σ}.

  Definition untrusted_main_val : val.
  Proof using.
    let t:= eval unfold untrusted_main in untrusted_main in solve_unfold t.
    Unshelve. apply _.
  Defined.

  Lemma untrusted_main_val_eq : untrusted_main_val = untrusted_main.
  Proof. by apply recv_f_equal. Qed.
End logic.
