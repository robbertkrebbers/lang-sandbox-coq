From sandbox.lang Require Import lang notation type proofmode.

Section examples.
  Context `{sandboxG Σ}.

  Definition add_test : val :=
    λ: ["x"], let: "x2" := "x" + "x" in
              let: "ret" := "x2" + #1 in "ret".

  Lemma add_test_type p : add_test ◁ fn p [# int] int.
  Proof.
    iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    iDestruct (int_inv with "Hx") as %[? ->].
    iApply (type_let). { by iApply type_plus. }
    iIntros (v) "Hv _" => /=.
    iDestruct (int_inv with "Hv") as %[? ->].
    iApply (type_let with "[Hv]"). { by iApply (type_plus). }
    iIntros (?) "Hv _" => /=.
      by iApply type_val.
  Qed.

  Definition int_prod_test : val :=
    λ: ["v1"], let: "p" := Alloc High #2 in
               ("p" +ₗ #0) <- "v1";;
               ("p" +ₗ #1) <- #2;;
               let: "v2r" := !("p" +ₗ #1) in
               Free #2 "p";;
               let: "ret" := add_test [ "v2r" ] in "ret".

  Lemma int_prod_type : int_prod_test ◁ fn High [# int] int.
  Proof.
    iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    iApply (type_let). { by iApply type_new. }
    iIntros (v) "Hv _"; simpl_subst.
    iApply (type_seq with "[Hv]"). { by iApply (type_write int with "[$]"). }
    iIntros "Hv" => /=.
    iApply (type_seq with "[Hv]"). { by iApply (type_write int _ _ #2 with "[$]"). }
    iIntros "Hv" => /=.
    iApply (type_let with "[Hv]"). { by iApply type_read => //. }
    iIntros (v2r) "Hv2r Hv"; simpl_subst.
    iApply (type_seq with "[Hv]"). { by iApply (type_delete with "[$]"). }
    iIntros "_" => /=.
    iApply (type_let with "[Hv2r]"). { iApply type_fn_call => //. by iApply add_test_type. by iFrame. }
    iIntros (v2) "Hv _"; simpl_subst.
      by iApply type_val.
  Qed.

End examples.
