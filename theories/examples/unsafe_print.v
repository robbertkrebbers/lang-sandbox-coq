From stdpp Require Import gmap.
From sandbox.lang Require Import lang notation proofmode type wrapper adversary robust_safety interface unfold.
From sandbox.lang.lib Require Import assert product.

Definition printSP : syscall_policy := {|
  sp_state := ();
  sp_initial_state := ();
  sp_transitions :=
    <["unsafe_print" := λ v' r s, v ← lit_to_Z v';
          if bool_decide (v ≤ 999) then Some () else None ]>
    ∅
|}.

Section print.
  Context `{sandboxG Σ} `{inSP Σ (liftSPG Σ printSP)}.
  Program Definition less_than_1000 : type :=
    {| ty_own v := match v return _ with
                   | LitV (LitInt z) => ⌜z ≤ 999⌝
                   | _ => False
                   end%I |}.
  Next Obligation. move => [[]|] * //; by iIntros "?". Qed.

  Global Instance less_than_1000_copy : Copy less_than_1000.
  Proof.
    case; last by apply _. by case; apply _.
  Qed.

  Global Instance import_less_than_1000 : Import less_than_1000 :=
    {| import_ctx := True%I;
       import_fn := (λ: ["x"], let: "v" := import_fn int ["x"] in
                               if: "v" ≤ #999 then "v" else stuck_expr
                               ) |}.
  Proof.
    iIntros "_". iApply type_fn => //. do 2 iModIntro.
    iIntros (xl). inv_vec xl => x /=. iIntros "[#Hx _]". simpl_subst.
    iApply (type_let).
    { iApply type_fn_call => //.  by iApply import_type. by iSplit. }
    iIntros (v) "#Hv _". simpl_subst.
    iDestruct (int_to_const_int with "Hv") as (m) "Hm".
    iApply type_case. by iApply type_leq.
    iIntros (i er ? Heq) "H1 H2 _".
    move: i Heq => [|[|?]] /=; rewrite ?lookup_nil // => [[<-]].
    - by iApply type_stuck.
    - iApply type_val.
      iDestruct (const_int_inv with "H1") as %->.
      iDestruct (const_int_inv with "H2") as %Heq.
      iDestruct (const_int_inv with "Hm") as %->.
      move: Heq. by case_bool_decide.
  Defined.
  Global Opaque import_less_than_1000.

  Lemma less_than_1000_inv v : v ◁ less_than_1000 -∗ ⌜∃ (z : Z), v = #z ∧ z ≤ 999⌝.
  Proof.
    iIntros "He". case v => // [[| |?]] //.
    iRevert "He". iIntros (?). eauto.
  Qed.

  Lemma type_unsafe_print v :
    v ◁ less_than_1000 -∗
      typed_expr High any True (Syscall "unsafe_print" v).
  Proof.
    iIntros "Hv".
    iDestruct (less_than_1000_inv with "Hv") as %[z [-> ?]].
    iApply (type_syscall _ _ True%I) => //.
    iIntros (? ? ? ?) "_". iExists []. iSplit => //.
    iIntros (?) "_ ? _". iExists (). iModIntro. iFrame. iSplit => //.
    rewrite /run_sp_obs /=. simpl_map => /=.
      by case_bool_decide.
  Qed.

  Definition print : val :=
    λ: ["v"], Syscall "unsafe_print" "v".

  Lemma type_print :
    print ◁ fn High [# less_than_1000] any.
  Proof.
    iApply type_fn => //. do 2 iModIntro. iIntros (xl).
    inv_vec xl => v /=. iIntros "[Hv _]". simpl_subst.
    by iApply type_unsafe_print.
  Qed.

  Definition main : val :=
    λ: [ ], Fork (GatedCall (nroot.@"untrusted_main") [] );;
            let: "l" := Alloc High #1 in
            ("l" +ₗ #0) <- #42;;
            let: "r" := !("l" +ₗ #0) in
            Syscall "unsafe_print" "r".

  Lemma type_main :
    main ◁ fn High [# ] any.
  Proof.
    iApply type_fn => //. do 2 iModIntro. iIntros (xl).
    inv_vec xl => /=. iIntros "_". simpl_subst.
    iApply type_seq. {
      iApply type_fork. by iApply (type_gated_call _ []).
    }
    iIntros "_".
    iApply type_let. {
      iApply type_new.
    } simpl.
    iIntros (l) "Hl _".
    iApply (type_seq with "[Hl]"). {
      by iApply (type_write less_than_1000 with "Hl").
    }
    simpl. iIntros "Hl".
    iApply (type_let with "[Hl]"). {
      by iApply (type_read with "Hl").
    }
    simpl. iIntros (r) "Hr _".
    by iApply type_unsafe_print.
  Qed.

  Local Transparent wrap_import wrap_export import_const_int import_int export_lowptr export_int import_any export_product import_product export_any import_less_than_1000.

  Definition print_export : val := wrap_export print [less_than_1000] any.
  Definition print_export_val : val.
  Proof using.
    let t:= eval unfold print_export, wrap_export in print_export in solve_unfold t.
    Unshelve. apply _.
  Defined.

  Lemma print_export_val_eq : print_export_val = print_export.
  Proof. by apply: recv_f_equal. Qed.

  Definition main_val : val.
  Proof using.
    let t:= eval unfold main, wrap_export in main in solve_unfold t.
    Unshelve. apply _.
  Defined.

  Lemma main_val_eq : main_val = main.
  Proof. by apply: recv_f_equal. Qed.

End print.

Definition interface (u : interface_map) := λ p, match p with
                | Low => u
                | High =>
                  <[ nroot.@ "1" := print_export_val]> $
                  ∅
                end.

Lemma print_safe u t2 σ2 :
  let _ := SyscallG (λ _, True) in
  map_Forall (λ _ v, surface_val v) u →
  rtc erased_step ([(main_val [] at High)%E], initial_state ∅ (interface u)) (t2, σ2) →
  is_good printSP σ2.
Proof.
  set Σ : gFunctors := #[sandboxΣ]. move=>???.
  eapply (robust_safety Σ _ printSP ∅) => //.
  move => ?. exists (liftSPG Σ printSP). split => //. split => //? HSP HPre.
  have Hf : (inSP Σ (liftSPG Σ printSP)) by rewrite -HSP; apply insp_reflexive.
  rewrite main_val_eq print_export_val_eq.
  iIntros "_ !#". iSplit; last by iApply type_main.
  rewrite !big_sepM_insert => //.
  do ! iSplit => //.
  iExists _. iApply (type_wrap_export _ [less_than_1000] _ _ type_print) => //=.
Qed.

Print Assumptions print_safe.
